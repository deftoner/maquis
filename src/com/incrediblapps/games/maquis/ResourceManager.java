package com.incrediblapps.games.maquis;

import java.io.IOException;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import com.incrediblapps.games.maquis.layers.GameOverLayer;
import com.incrediblapps.games.maquis.utils.constants.Constants;
import com.incrediblapps.games.maquis.utils.constants.ConstantsLevel1;
import com.incrediblapps.games.maquis.utils.constants.ConstantsLevel6;

public class ResourceManager {

	private static ResourceManager INSTANCE;

	private static final String TAG = "ResourceManager";

	// Player

	public static TiledTextureRegion mPlayerTextureRegion;

	public static TiledTextureRegion mPlayerConfusedTextureRegion;

	public static ITiledTextureRegion mPlayerClimbingTextureRegion;

	// Shared

	public static ITextureRegion mLibraryTextureRegion;

	public static ITiledTextureRegion mDoor2TextureRegion;

	public static ITiledTextureRegion mDoor3TextureRegion;

	public static ITiledTextureRegion mSoldier1TextureRegion;

	public static ITextureRegion mSkipButtonRegion;

	// Level1

	public static ITextureRegion mBackgroundTextureRegionLvl1_1;

	public static ITextureRegion mBackgroundTextureRegionLvl1_2;

	public static ITiledTextureRegion mFlagTextureRegion;

	public static ITextureRegion mColumnTextureRegion;

	public static ITiledTextureRegion mDoor11102TextureRegion;

	public static ITiledTextureRegion mDoor11101TextureRegion;

	public static ITiledTextureRegion mDoor11100TextureRegion;

	public static ITiledTextureRegion mDoor11110TextureRegion;

	public static ITiledTextureRegion mDoor5TextureRegion;

	public static ITiledTextureRegion mClosetTextureRegion;

	public static ITiledTextureRegion mTablePeopleTextureRegion;

	public static ITextureRegion mShelves1TextureRegion;

	public static ITextureRegion mPicture11100TextureRegion;

	// Left House
	public static ITextureRegion mDoor11000TextureRegion;

	public static ITiledTextureRegion mDoor11001TextureRegion;

	public static ITiledTextureRegion mDoor11002TextureRegion;

	public static ITiledTextureRegion mDoor11010TextureRegion;

	public static ITextureRegion mShelves2TextureRegion;

	public static ITextureRegion mFlagRepTextureRegion;

	public static ITextureRegion mFlagCatTextureRegion;

	public static ITextureRegion mPicture12000TextureRegion;

	public static ITextureRegion mCarTextureRegion;

	public static TiledTextureRegion mClockTextureRegion;

	public static ITextureRegion mMetalicClosetTextureRegion;

	public static ITiledTextureRegion mTableTextureRegion;

	public static ITiledTextureRegion mHUDPapersTextureRegion;

	// Enemies

	public static ITiledTextureRegion mMaquiTextureRegion;

	// Level6

	public static ITextureRegion mBackgroundTextureRegionLvl6_1;

	public static ITextureRegion mBackgroundTextureRegionLvl6_2;

	public static ITiledTextureRegion mDoor61030TextureRegion;

	public static ITextureRegion mPillar1TextureRegion;

	public static ITextureRegion mPillar2TextureRegion;

	public static ITextureRegion mRailing630TextureRegion;

	public static ITextureRegion mRailing631TextureRegion;

	public static ITiledTextureRegion mStreetLight61TextureRegion;

	public static ITiledTextureRegion mStreetLight62TextureRegion;

	public static ITiledTextureRegion mStreetLight63TextureRegion;

	public static ITiledTextureRegion mDoor610401TextureRegion;

	public static ITiledTextureRegion mDoor61042TextureRegion;

	public static ITiledTextureRegion mDoor610434TextureRegion;

	public static ITiledTextureRegion mDoor61010TextureRegion;

	public static ITiledTextureRegion mDoor61011TextureRegion;

	public static ITiledTextureRegion mDoor6105012TextureRegion;

	public static ITextureRegion mCloset62050TextureRegion;

	public static ITextureRegion mPoster6040TextureRegion;

	public static ITextureRegion mPoster6041TextureRegion;

	public static ITextureRegion mCouchTextureRegion;

	public static ITextureRegion mViktoriaTextureRegion;

	public static ITextureRegion mPicture6010TextureRegion;

	public static ITextureRegion mPicture6011TextureRegion;

	public static ITextureRegion mWCTextureRegion;

	public static ITextureRegion mWCWallTextureRegion;

	public static ITextureRegion mShelves61TextureRegion;

	public static ITiledTextureRegion mDoor6WCTextureRegion;

	public static ITiledTextureRegion mCloset6040TextureRegion;

	// Level2

	public static ITextureRegion mBackgroundTextureRegionLvl2;

	// Level3

	public static ITextureRegion mBackgroundTextureRegionLvl3;

	// Menu

	public static ITextureRegion mGameBackgroundTextureRegion;

	public static ITextureRegion mMenuBackgroundTextureRegion;

	public static ITiledTextureRegion mLevelButtonTextureRegion;

	public static Sound mSound;

	public static ITiledTextureRegion mMapLevelSelectorTextureRegion;

	// Splash

	private static BuildableBitmapTextureAtlas mLogosTextureAtlas;

	public static ITextureRegion mStudioLogoTextureRegion;

	public static ITextureRegion mMaquisLogoTextureRegion;

	// Fonts

	public static Font mFontWhite;

	public static Font mFontBlack;

	public static Font mFontLCSmithWhite;

	public static Font mFontLCSmithBlack;

	public static Font mFontPixelBlack;

	public static Font mFontPixelWhite;

	// Utils

	public static Engine engine;

	public static Context context;

	public static MaquisMainActivity activity;

	public static float cameraWidth;

	public static float cameraHeight;

	public static float cameraWidthHalf;

	public static float cameraHeightHalf;

	public static float cameraScaleFactorX;

	public static float cameraScaleFactorY;

	ResourceManager() {
		// The constructor is of no use to us
	}

	public synchronized static ResourceManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ResourceManager();
		}
		return INSTANCE;
	}

	// ====================================================
	// PUBLIC METHODS
	// ====================================================
	// Setup the ResourceManager
	public static void setup(MaquisMainActivity pActivity, Engine pEngine, Context pContext, float pCameraWidth, float pCameraHeight, float pCameraScaleX,
			float pCameraScaleY) {
		ResourceManager.activity = pActivity;
		ResourceManager.engine = pEngine;
		ResourceManager.context = pContext;
		ResourceManager.cameraWidth = pCameraWidth;
		ResourceManager.cameraHeight = pCameraHeight;
		ResourceManager.cameraWidthHalf = pCameraWidth * 0.5f;
		ResourceManager.cameraHeightHalf = pCameraHeight * 0.5f;
		ResourceManager.cameraScaleFactorX = pCameraScaleX;
		ResourceManager.cameraScaleFactorY = pCameraScaleY;
	}

	// Loads all game resources.
	public static void loadGameResources() {
		Log.d(TAG, "loadGameResources()");
		// getInstance().loadGameTextures();
		// getInstance().loadSharedResources();
		GameOverLayer.getInstance().onLoadLayer();
		// getInstance().loadSharedResources();

		// LevelPauseLayer.getInstance().onLoadLayer();
		// OptionsLayer.getInstance().onLoadLayer();
	}

	public static void loadSplashResources() {

		Log.d(TAG, "loadSplashResources()");

		// Set the base path
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/splash/");

		mLogosTextureAtlas = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(), Constants.Shared.SPLASH_ATLAS_WIDTH,
				Constants.Shared.SPLASH_ATLAS_HEIGHT);

		mStudioLogoTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mLogosTextureAtlas,
				ResourceManager.getActivity().getAssets(),
				"studio_logo.png");

		mMaquisLogoTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mLogosTextureAtlas,
				ResourceManager.getActivity().getAssets(),
				"maquis_logo.png");

		try {
			mLogosTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			mLogosTextureAtlas.load();
		} catch (TextureAtlasBuilderException e) {
			e.printStackTrace();
		}
	}

	public static void unloadSplashResources() {
		mLogosTextureAtlas.unload();
	}

	// Loads all menu resources
	public static void loadMenuResources() {
		Log.d(TAG, "loadMenuResources()");
		getInstance().loadMenuTextures();
		getInstance().loadLevelSelectorTextures();
		getInstance().loadSharedResources();
	}

	private void loadLevelSelectorTextures() {
		// Set the base path
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");

		BuildableBitmapTextureAtlas mMapTextureAtlas = new BuildableBitmapTextureAtlas(engine.getTextureManager(), Constants.LevelSelector.ATLAS_WIDTH,
				Constants.LevelSelector.ATLAS_HEIGHT);

		mMapLevelSelectorTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAssetDirectory(mMapTextureAtlas,
				ResourceManager.activity.getAssets(),
				"map");

		/* Create the flag texture region */
		mLevelButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(mMapTextureAtlas,
				ResourceManager.activity.getAssets(),
				"flag_level_button.png",
				8,
				1);

		/* Build and load the background left texture atlas */
		try {
			mMapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			mMapTextureAtlas.load();
		} catch (TextureAtlasBuilderException e) {
			e.printStackTrace();
		}

	}

	/*
	 * Each scene within a game should have a loadTextures method as well as an
	 * accompanying unloadTextures method. This way, we can display a loading
	 * image during scene swapping, unload the first scene's textures then load
	 * the next scenes textures.
	 */
	public synchronized void loadGameTextures() {
		// Set our game assets folder in "assets/gfx/game/"
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/game/");

		BuildableBitmapTextureAtlas mBitmapTextureAtlas = new BuildableBitmapTextureAtlas(engine.getTextureManager(), 800, 480);

		mGameBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mBitmapTextureAtlas, context, "game_background.png");

		try {
			mBitmapTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			mBitmapTextureAtlas.load();
		} catch (TextureAtlasBuilderException e) {
			Debug.e(e);
		}

	}

	/*
	 * All textures should have a method call for unloading once they're no
	 * longer needed; ie. a level transition.
	 */
	public synchronized void unloadGameTextures() {
		// call unload to remove the corresponding texture atlas from memory
		BuildableBitmapTextureAtlas mBitmapTextureAtlas = (BuildableBitmapTextureAtlas) mGameBackgroundTextureRegion.getTexture();
		mBitmapTextureAtlas.unload();

		// ... Continue to unload all textures related to the 'Game' scene

		// Once all textures have been unloaded, attempt to invoke the Garbage
		// Collector
		System.gc();
	}

	/*
	 * Similar to the loadGameTextures(...) method, except this method will be
	 * used to load a different scene's textures
	 */
	public synchronized void loadMenuTextures() {

		// Set our menu assets folder in "assets/gfx/menu/"
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");

		BuildableBitmapTextureAtlas mMenuBGTextureAtlas = new BuildableBitmapTextureAtlas(engine.getTextureManager(), Constants.Resolution.DEFAULT_WIDTH,
				Constants.Resolution.DEFAULT_HEIGHT);

		mMenuBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mMenuBGTextureAtlas, context, "MainMenuBG.png");

		try {
			mMenuBGTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			mMenuBGTextureAtlas.load();

		} catch (TextureAtlasBuilderException e) {
			Debug.e(e);
		}

	}

	// Once again, this method is similar to the 'Game' scene's for unloading
	public synchronized void unloadMenuTextures() {
		// call unload to remove the corresponding texture atlas from memory
		BuildableBitmapTextureAtlas mBitmapTextureAtlas = (BuildableBitmapTextureAtlas) mMenuBackgroundTextureRegion.getTexture();
		mBitmapTextureAtlas.unload();

		// ... Continue to unload all textures related to the 'Game' scene

		// Once all textures have been unloaded, attempt to invoke the Garbage
		// Collector
		System.gc();
	}

	/*
	 * As with textures, we can create methods to load sound/music objects for
	 * different scene's within our games.
	 */
	public synchronized void loadSounds(Engine pEngine, Context pContext) {
		// Set the SoundFactory's base path
		SoundFactory.setAssetBasePath("sounds/");
		try {
			// Create mSound object via SoundFactory class
			mSound = SoundFactory.createSoundFromAsset(pEngine.getSoundManager(), pContext, "sound.mp3");
		} catch (final IOException e) {
			Log.v("Sounds Load", "Exception:" + e.getMessage());
		}
	}

	/*
	 * In some cases, we may only load one set of sounds throughout our entire
	 * game's life-cycle. If that's the case, we may not need to include an
	 * unloadSounds() method. Of course, this all depends on how much variance
	 * we have in terms of sound
	 */
	public synchronized void unloadSounds() {
		// we call the release() method on sounds to remove them from memory
		if (!mSound.isReleased())
			mSound.release();
	}

	/*
	 * Lastly, we've got the loadFonts method which, once again, tends to only
	 * need to be loaded once as Font's are generally used across an entire
	 * game, from menu to shop to game-play.
	 */
	public synchronized void loadFonts() {

		Log.d(TAG, "loadFonts()");

		FontFactory.setAssetBasePath("fonts/");

		// Create mFont object via FontFactory class
		mFontWhite = FontFactory.create(engine.getFontManager(),
				engine.getTextureManager(),
				256,
				256,
				Typeface.create(Typeface.DEFAULT, Typeface.NORMAL),
				32f,
				true,
				Color.WHITE_ABGR_PACKED_INT);

		mFontWhite.load();

		mFontBlack = FontFactory.create(engine.getFontManager(),
				engine.getTextureManager(),
				256,
				256,
				Typeface.create(Typeface.DEFAULT, Typeface.NORMAL),
				32f,
				true,
				Color.BLACK_ABGR_PACKED_INT);

		mFontBlack.load();

		mFontLCSmithBlack = FontFactory.create(engine.getFontManager(),
				engine.getTextureManager(),
				256,
				256,
				Typeface.createFromAsset(activity.getAssets(), "fonts/L.C.Smith5.ttf"),
				32f,
				true,
				Color.BLACK_ABGR_PACKED_INT);

		mFontLCSmithBlack.load();

		mFontLCSmithWhite = FontFactory.create(engine.getFontManager(),
				engine.getTextureManager(),
				256,
				256,
				Typeface.createFromAsset(activity.getAssets(), "fonts/L.C.Smith5.ttf"),
				32f,
				true,
				Color.WHITE_ABGR_PACKED_INT);

		mFontLCSmithWhite.load();

		mFontPixelBlack = FontFactory.create(engine.getFontManager(),
				engine.getTextureManager(),
				256,
				256,
				Typeface.createFromAsset(activity.getAssets(), "fonts/PressStart2P.ttf"),
				24f,
				true,
				Color.BLACK_ABGR_PACKED_INT);

		mFontPixelBlack.load();

		mFontPixelWhite = FontFactory.create(engine.getFontManager(),
				engine.getTextureManager(),
				256,
				256,
				Typeface.createFromAsset(activity.getAssets(), "fonts/PressStart2P.ttf"),
				24f,
				true,
				Color.WHITE_ABGR_PACKED_INT);

		mFontPixelWhite.load();

	}

	/*
	 * If an unloadFonts() method is necessary, we can provide one
	 */
	public synchronized void unloadFonts() {
		// Similar to textures, we can call unload() to destroy font resources
		mFontWhite.unload();
		mFontBlack.unload();
	}

	// ====================================================
	// PRIVATE METHODS
	// ====================================================
	// Loads resources used by both the game scenes and menu scenes
	private void loadSharedResources() {
		Log.d(TAG, "loadSharedResources()");
		// loadSharedTextures();
		loadFonts();

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/common/");

		BuildableBitmapTextureAtlas playerTextures = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(),
				Constants.Player.PLAYER_ATLAS_WIDTH, Constants.Player.PLAYER_ATLAS_HEIGHT, TextureOptions.BILINEAR);

		mPlayerTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(playerTextures,
				ResourceManager.activity.getAssets(),
				"player.png",
				Constants.Player.PLAYER_TILES_COLUMNS,
				Constants.Player.PLAYER_TILES_ROWS);

		mPlayerConfusedTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(playerTextures,
				ResourceManager.activity.getAssets(),
				"confused.png",
				Constants.Player.CONFUSED_TILES_COLUMNS,
				Constants.Player.CONFUSED_TILES_ROWS);

		mPlayerClimbingTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(playerTextures,
				ResourceManager.activity.getAssets(),
				"climbing.png",
				Constants.Player.CLIMBING_TILES_COLUMNS,
				Constants.Player.CLIMBING_TILES_ROWS);

		BuildableBitmapTextureAtlas sharedTextureAtlas = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(),
				Constants.Shared.SHARED_ATLAS_WIDTH, Constants.Shared.SHARED_ATLAS_HEIGHT, TextureOptions.BILINEAR);

		mDoor2TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(sharedTextureAtlas,
				ResourceManager.activity.getAssets(),
				"door2.png",
				2,
				1);

		mDoor3TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(sharedTextureAtlas,
				ResourceManager.activity.getAssets(),
				"door3.png",
				2,
				1);

		mLibraryTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sharedTextureAtlas, ResourceManager.activity.getAssets(), "library.png");

		mSkipButtonRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sharedTextureAtlas,
				ResourceManager.activity.getAssets(),
				"skip_temporal.png");

		BuildableBitmapTextureAtlas enemiesTextureAtlas = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(),
				Constants.Shared.ENEMIES_ATLAS_WIDTH, Constants.Shared.ENEMIES_ATLAS_HEIGHT, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		mSoldier1TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(enemiesTextureAtlas,
				ResourceManager.activity.getAssets(),
				"soldier1.png",
				Constants.Enemies.Soldier1.TILES_COLUMNS,
				Constants.Enemies.Soldier1.TILES_ROWS);

		try {
			playerTextures.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			playerTextures.load();
			sharedTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			sharedTextureAtlas.load();
			enemiesTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			enemiesTextureAtlas.load();
		} catch (TextureAtlasBuilderException e) {
			e.printStackTrace();
		}

	}

	public static synchronized void loadLevel1Resources() {

		Log.d(TAG, "loadLevel1Resources()");

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

		BuildableBitmapTextureAtlas backgroundTexture1 = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(), ConstantsLevel1.WIDTH_1,
				ConstantsLevel1.HEIGHT, TextureOptions.BILINEAR);

		BuildableBitmapTextureAtlas backgroundTexture2 = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(), ConstantsLevel1.WIDTH_2,
				ConstantsLevel1.HEIGHT, TextureOptions.BILINEAR);

		mBackgroundTextureRegionLvl1_1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(backgroundTexture1,
				ResourceManager.activity.getAssets(),
				"level1/background_lvl1_1.png");

		mBackgroundTextureRegionLvl1_2 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(backgroundTexture2,
				ResourceManager.activity.getAssets(),
				"level1/background_lvl1_2.png");

		BuildableBitmapTextureAtlas objectsTextureAtlas = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(),
				ConstantsLevel1.OBJECTS_ATLAS_WIDTH, ConstantsLevel1.OBJECTS_ATLAS_HEIGHT, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		mDoor11100TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_1_00.png",
				1,
				2);
		mDoor11101TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_1_01.png",
				1,
				2);
		mDoor11102TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_1_02.png",
				1,
				2);
		mDoor11110TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_1_10.png",
				1,
				2);

		mDoor11000TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_0_00.png");

		mDoor11001TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_0_01.png",
				1,
				2);
		mDoor11002TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_0_02.png",
				1,
				2);

		mDoor11010TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_0_10.png",
				1,
				2);

		mClosetTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/closet.png",
				2,
				1);

		mColumnTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/column.png");

		mShelves1TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/shelves1.png");

		mShelves2TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/shelves2.png");

		mPicture11100TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/picture1.png");

		BuildableBitmapTextureAtlas tablePeopleTextures = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(),
				ConstantsLevel1.TABLEPEOPLE_ATLAS_WIDTH, ConstantsLevel1.TABLEPEOPLE_ATLAS_HEIGHT, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		mTablePeopleTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(tablePeopleTextures,
				ResourceManager.activity.getAssets(),
				"level1/table_with_people.png",
				ConstantsLevel1.TABLEPEOPLE_TILES_COLUMNS,
				ConstantsLevel1.TABLEPEOPLE_TILES_ROWS);

		mDoor11101TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_1_01.png",
				1,
				2);
		mDoor11102TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_1_02.png",
				1,
				2);
		mDoor11110TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/door1_1_10.png",
				1,
				2);

		mFlagRepTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/flag_rep.png");

		mFlagCatTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/flag_cat.png");

		mPicture12000TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/picture2.png");

		mCarTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas, ResourceManager.activity.getAssets(), "level1/car.png");

		mClockTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/clock.png",
				4,
				1);

		mMetalicClosetTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/metalic_closet.png");

		mTableTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/table.png",
				2,
				1);

		mHUDPapersTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/HUD/papers_HUD.png",
				2,
				1);

		BuildableBitmapTextureAtlas enemiesTextureAtlas = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(),
				ConstantsLevel1.ENEMIES_ATLAS_WIDTH, ConstantsLevel1.ENEMIES_ATLAS_HEIGHT, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		mMaquiTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(enemiesTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level1/maqui.png",
				Constants.Enemies.Maqui.TILES_COLUMNS,
				Constants.Enemies.Maqui.TILES_ROWS);

		try {

			backgroundTexture1.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			backgroundTexture1.load();
			backgroundTexture2.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			backgroundTexture2.load();

			tablePeopleTextures.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			tablePeopleTextures.load();

			objectsTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			objectsTextureAtlas.load();

			enemiesTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			enemiesTextureAtlas.load();

		} catch (TextureAtlasBuilderException e) {
			e.printStackTrace();
		}
	}

	public static synchronized void loadLevel6Resources() {

		Log.d(TAG, "loadLevel1Resources()");

		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

		BuildableBitmapTextureAtlas backgroundTexture1 = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(), ConstantsLevel6.WIDTH_1,
				ConstantsLevel6.HEIGHT, TextureOptions.BILINEAR);

		BuildableBitmapTextureAtlas backgroundTexture2 = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(), ConstantsLevel6.WIDTH_2,
				ConstantsLevel6.HEIGHT, TextureOptions.BILINEAR);

		mBackgroundTextureRegionLvl6_1 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(backgroundTexture1,
				ResourceManager.activity.getAssets(),
				"level6/background_lvl6_1.png");

		mBackgroundTextureRegionLvl6_2 = BitmapTextureAtlasTextureRegionFactory.createFromAsset(backgroundTexture2,
				ResourceManager.activity.getAssets(),
				"level6/background_lvl6_2.png");

		BuildableBitmapTextureAtlas objectsTextureAtlas = new BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(),
				ConstantsLevel6.OBJECTS_ATLAS_WIDTH, ConstantsLevel6.OBJECTS_ATLAS_HEIGHT, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		mDoor61030TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/door1_0_30.png",
				1,
				2);

		mPillar1TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/pillar1.png");

		mPillar2TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/pillar2.png");

		mRailing630TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/railing_left.png");

		mRailing631TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/railing_right.png");

		mStreetLight61TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/streetlight1.png",
				3,
				1);

		mStreetLight62TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/streetlight2.png",
				3,
				1);

		mStreetLight63TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/streetlight3.png",
				3,
				1);

		mDoor610401TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/door1_0_40&1.png",
				1,
				2);

		mDoor61042TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/door1_0_42.png",
				1,
				2);

		mDoor610434TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/door1_0_43&4.png",
				1,
				2);

		mDoor61010TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/door1_0_10.png",
				1,
				2);

		mDoor61011TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/door1_0_11.png",
				1,
				2);

		mDoor6105012TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/door1_0_50&1&2.png",
				1,
				2);

		mCloset62050TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/closet62050.png");

		mPoster6040TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/poster6_040.png");

		mPoster6041TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/poster6_041.png");

		mCouchTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/couch.png");

		mViktoriaTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/viktoria.png");

		mPicture6010TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/picture6_011.png");

		mPicture6011TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/picture6_010.png");

		mShelves61TextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/shelves61.png");

		mWCTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas, ResourceManager.activity.getAssets(), "level6/wc.png");

		mWCWallTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/wcWall.png");

		mDoor6WCTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/door6_0_10.png",
				1,
				2);

		mCloset6040TextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(objectsTextureAtlas,
				ResourceManager.activity.getAssets(),
				"level6/closet.png",
				2,
				1);

		//
		// BuildableBitmapTextureAtlas enemiesTextureAtlas = new
		// BuildableBitmapTextureAtlas(ResourceManager.engine.getTextureManager(),
		// ConstantsLevel1.ENEMIES_ATLAS_WIDTH,
		// ConstantsLevel1.ENEMIES_ATLAS_HEIGHT,
		// TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		//
		// mMaquiTextureRegion =
		// BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(enemiesTextureAtlas,
		// ResourceManager.activity.getAssets(),
		// "level1/maqui.png",
		// Constants.Enemy.MAQUI_TILES_COLUMNS,
		// Constants.Enemy.MAQUI_TILES_ROWS);

		try {

			backgroundTexture1.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			backgroundTexture1.load();
			backgroundTexture2.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
			backgroundTexture2.load();

			objectsTextureAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 1));
			objectsTextureAtlas.load();

			// enemiesTextureAtlas.build(new
			// BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource,
			// BitmapTextureAtlas>(0, 0, 0));
			// enemiesTextureAtlas.load();

		} catch (TextureAtlasBuilderException e) {
			e.printStackTrace();
		}
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		ResourceManager.engine = engine;
	}

	public static MaquisMainActivity getActivity() {
		getInstance();
		return ResourceManager.activity;
	}

}