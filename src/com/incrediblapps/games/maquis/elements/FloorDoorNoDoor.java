package com.incrediblapps.games.maquis.elements;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

import android.os.SystemClock;
import android.util.Log;

import com.incrediblapps.games.maquis.MaquisMainActivity;
import com.incrediblapps.games.maquis.elements.Door.DoorState;
import com.incrediblapps.games.maquis.players.actions.ChangeFloorAction;
import com.incrediblapps.games.maquis.players.actions.HideDoorAction;
import com.incrediblapps.games.maquis.players.actions.HidePlayerAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

public class FloorDoorNoDoor extends Rectangle {

	private static final String TAG = "FloorDoorNoDoorElement";

	public final int DOOR_CENTER = 26;

	public FloorDoorNoDoor mConnectedDoor;

	public int mFloor;

	public int mRoom;

	protected SceneLevel mScene;

	private float gotodoor = 21;

	/* Parameter to control the double-tap */
	// Set the double tap delay in milliseconds
	protected static final long DOUBLE_CLICK_MAX_DELAY = 400L;

	public static long thisTime = 0;

	public static long prevTime = 0;

	public static boolean firstTap = true;

	public FloorDoorNoDoor(SceneLevel pScene, float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager,
			int pFloor, int pRoom) {
		super(pWidth * 0.5f, pHeight * 0.5f, pWidth, pHeight, pVertexBufferObjectManager);
		this.mScene = pScene;
		this.setColor(Color.GREEN);
		this.setDebug(false);
		this.mFloor = pFloor;
		this.mRoom = pRoom;
	}

	public FloorDoorNoDoor getConnectedDoor() {
		return mConnectedDoor;
	}

	public void setConnectedDoor(FloorDoorNoDoor connectedDoor) {
		this.mConnectedDoor = connectedDoor;
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
		mScene.mGestureTouchEvent = pSceneTouchEvent;
		if (pSceneTouchEvent.isActionDown()) {
			if (firstTap) {
				thisTime = SystemClock.uptimeMillis();
				firstTap = false;
				MaquisMainActivity.mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.d(TAG, "handled");
						if (!firstTap) {
							firstTap = !firstTap;
						}
					}
				}, 500);
			} else {
				prevTime = thisTime;
				thisTime = SystemClock.uptimeMillis();
				if (thisTime > prevTime) {
					if ((thisTime - prevTime) <= DOUBLE_CLICK_MAX_DELAY) {
						// We have detected a double tap!
						doubleClickAction();
						firstTap = true;
					} else {
						firstTap = true;
					}
				} else {
					firstTap = true;
				}
			}
		}
		return false;
	}

	public void doubleClickAction() {

		if (!mScene.mPlayer.mIsLocked) {
			if (mScene.mPlayer.mFloor == this.mFloor) {

				if (!mScene.mPlayer.mMovementsQueue.isEmpty()) {
					if (mScene.mPlayer.mMovementsQueue.element() instanceof MovementAction
							&& (((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_LEFT || ((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_RIGHT)) {
						mScene.mPlayer.mHasChanged = true;
						mScene.mPlayer.reset();
					} else {
						return;
					}
				}

				float targetX = this.getX() + DOOR_CENTER;
				if (targetX % 2 == 1) {
					targetX++;
				}

				boolean wasHidden = false;

				if (mScene.mPlayer.mIsHidden) {
					mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mScene.mPlayer.mWhereIsHidden, DoorState.OPEN_DOOR));
					mScene.mPlayer.mMovementsQueue.add(new HidePlayerAction(mScene.mPlayer, false));
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, mScene.mPlayer.mPlayerY - ((Door) mScene.mPlayer.mWhereIsHidden).gotodoor,
							PlayerState.TELEPORTATION));
					mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mScene.mPlayer.mWhereIsHidden, DoorState.CLOSED_DOOR));
					mScene.mPlayer.mWhereIsHidden = null;
					wasHidden = true;
				}

				if (mScene.mPlayer.mPlayerX < targetX) {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_RIGHT));
				} else {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_LEFT));
				}

				float mPlayerAux = mScene.mPlayer.mPlayerY;
				if (wasHidden) {
					mPlayerAux = mScene.mPlayer.mPlayerY - ((Door) mScene.mPlayer.mWhereIsHidden).gotodoor;
				}

				mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, mPlayerAux + this.gotodoor, PlayerState.GO_TO_DOOR));

				mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, this.mConnectedDoor.getY() + 6, PlayerState.TELEPORTATION));

				mScene.mPlayer.mMovementsQueue.add(new ChangeFloorAction(this.mConnectedDoor.mFloor, this.mConnectedDoor.mRoom));
			}
		}
	}

	public void setDebug(boolean pDebug) {
		if (pDebug) {
			this.setAlpha(1);
		} else {
			this.setAlpha(0);
		}
	}

}
