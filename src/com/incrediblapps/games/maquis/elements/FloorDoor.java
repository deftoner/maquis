package com.incrediblapps.games.maquis.elements;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.incrediblapps.games.maquis.players.actions.ChangeFloorAction;
import com.incrediblapps.games.maquis.players.actions.FloorDoorAction;
import com.incrediblapps.games.maquis.players.actions.HideDoorAction;
import com.incrediblapps.games.maquis.players.actions.HidePlayerAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

public class FloorDoor extends Door {

	private static final String TAG = "FloorDoorElement";

	public final int DOOR_CENTER = 32;

	public FloorDoor mConnectedDoor;

	public FloorDoor(SceneLevel pScene, float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			boolean pOpened, int pFloor, int pRoom) {
		super(pScene, pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pOpened, pFloor, pRoom);
	}

	public FloorDoor(SceneLevel pScene, float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			boolean pOpened, int pFloor, int pRoom, int pGotodoor, int pDoorCenter) {
		super(pScene, pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pOpened, pFloor, pRoom);
		this.doorCenter = pDoorCenter;
		this.gotodoor = pGotodoor;
	}

	public FloorDoor getConnectedDoor() {
		return mConnectedDoor;
	}

	public void setConnectedDoor(FloorDoor connectedDoor) {
		this.mConnectedDoor = connectedDoor;
	}

	@Override
	public void doubleClickAction() {

		if (!mScene.mPlayer.mIsLocked) {
			if (mScene.mPlayer.mFloor == this.mFloor) {

				if (!mScene.mPlayer.mMovementsQueue.isEmpty()) {
					if (mScene.mPlayer.mMovementsQueue.element() instanceof MovementAction
							&& (((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_LEFT || ((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_RIGHT)) {
						mScene.mPlayer.mHasChanged = true;
						mScene.mPlayer.reset();
					} else {
						return;
					}
				}

				float targetX = this.getX() + DOOR_CENTER;
				if (targetX % 2 == 1) {
					targetX++;
				}

				boolean wasHidden = false;

				if (mScene.mPlayer.mIsHidden) {
					mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mScene.mPlayer.mWhereIsHidden, DoorState.OPEN_DOOR));
					mScene.mPlayer.mMovementsQueue.add(new HidePlayerAction(mScene.mPlayer, false));
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, mScene.mPlayer.mPlayerY - this.gotodoor, PlayerState.TELEPORTATION));
					mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mScene.mPlayer.mWhereIsHidden, DoorState.CLOSED_DOOR));
					mScene.mPlayer.mWhereIsHidden = null;
					wasHidden = true;
				}

				if (mScene.mPlayer.mPlayerX < targetX) {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_RIGHT));
				} else {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_LEFT));
				}

				mScene.mPlayer.mMovementsQueue.add(new FloorDoorAction(this, DoorState.OPEN_DOOR, false));

				float mPlayerAux = mScene.mPlayer.mPlayerY;
				if (wasHidden) {
					mPlayerAux = mScene.mPlayer.mPlayerY - this.gotodoor;
				}

				mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, mPlayerAux + this.gotodoor, PlayerState.GO_TO_DOOR));

				mScene.mPlayer.mMovementsQueue.add(new FloorDoorAction(this, DoorState.CLOSED_DOOR, false));

				mScene.mPlayer.mMovementsQueue.add(new FloorDoorAction(this.mConnectedDoor, DoorState.OPEN_DOOR, false));

				mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, this.mConnectedDoor.getY() + this.gotodoor, PlayerState.TELEPORTATION));

				mScene.mPlayer.mMovementsQueue.add(new ChangeFloorAction(this.mConnectedDoor.mFloor, this.mConnectedDoor.mRoom));

				mScene.mPlayer.mMovementsQueue.add(new FloorDoorAction(this.mConnectedDoor, DoorState.CLOSED_DOOR, true));

			}
		}
	}

}
