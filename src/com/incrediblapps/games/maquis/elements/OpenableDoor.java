package com.incrediblapps.games.maquis.elements;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.incrediblapps.games.maquis.players.actions.DoorAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

public class OpenableDoor extends Door {

	private static final String TAG = "OpenableDoor";

	public final int DOOR_CENTER = 30;

	public OpenableDoor(SceneLevel pScene, float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			boolean pOpened, int pFloor) {
		super(pScene, pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pOpened, pFloor, 0);
		this.openTile = 0;
		this.closedTile = 1;
		if (pOpened) {
			this.setCurrentTileIndex(openTile);
		} else {
			this.setCurrentTileIndex(closedTile);
		}

	}

	@Override
	public void doubleClickAction() {

		if (!mScene.mPlayer.mIsLocked) {
			if (mScene.mPlayer.mFloor == this.mFloor) {

				if (!mScene.mPlayer.mMovementsQueue.isEmpty()) {
					if (mScene.mPlayer.mMovementsQueue.element() instanceof MovementAction
							&& (((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_LEFT || ((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_RIGHT)) {
						mScene.mPlayer.mHasChanged = true;
						mScene.mPlayer.reset();
					} else {
						return;
					}
				}

				float targetX = this.getX() + DOOR_CENTER;
				if (targetX % 2 == 1) {
					targetX++;
				}

				if (mScene.mPlayer.mPlayerX < targetX) {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_RIGHT));
				} else {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_LEFT));
				}

				if (this.mOpened) {
					mScene.mPlayer.mMovementsQueue.add(new DoorAction((Door) this, DoorState.CLOSED_DOOR));
				} else {
					mScene.mPlayer.mMovementsQueue.add(new DoorAction((Door) this, DoorState.OPEN_DOOR));
				}

				mOpened = !mOpened;
			}
		}
	}

}
