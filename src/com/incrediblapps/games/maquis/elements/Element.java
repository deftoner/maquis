package com.incrediblapps.games.maquis.elements;

import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.os.SystemClock;

import com.incrediblapps.games.maquis.MaquisMainActivity;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

public abstract class Element extends Sprite {

	protected SceneLevel mScene;

	/* Parameter to control the double-tap */
	// Set the double tap delay in milliseconds
	protected static final long DOUBLE_CLICK_MAX_DELAY = 400L;

	public static long thisTime = 0;

	public static long prevTime = 0;

	public static boolean firstTap = true;

	public int mFloor;

	public int mRoom;

	public Element(SceneLevel pScene, float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager, int pFloor,
			int pRoom) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.mScene = pScene;
		this.mFloor = pFloor;
		this.mRoom = pRoom;
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {

		((SceneLevel) mScene).mGestureTouchEvent = pSceneTouchEvent;

		if (pSceneTouchEvent.isActionDown()) {

			if (firstTap) {
				thisTime = SystemClock.uptimeMillis();
				firstTap = false;

				MaquisMainActivity.mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						if (!firstTap) {
							firstTap = !firstTap;
						}
					}
				}, 500);
			} else {
				prevTime = thisTime;
				thisTime = SystemClock.uptimeMillis();

				if (thisTime > prevTime) {
					if ((thisTime - prevTime) <= DOUBLE_CLICK_MAX_DELAY) {

						doubleClickAction();

						firstTap = true;

					} else {
						firstTap = true;
					}
				} else {
					firstTap = true;
				}
			}
		}

		return false;
	}

	protected abstract void doubleClickAction();

}
