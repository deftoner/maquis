package com.incrediblapps.games.maquis.elements;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

import com.incrediblapps.games.maquis.players.main.MainCharacter;

public class Wall extends Rectangle {

	private MainCharacter mPlayer;

	public Wall(MainCharacter pPlayer, float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX + (float) (pWidth * 0.5), pY + (float) (pHeight * 0.5), pWidth, pHeight, pVertexBufferObjectManager);
		this.mPlayer = pPlayer;
		this.setColor(Color.GREEN);
		this.setDebug(false);
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		super.onManagedUpdate(pSecondsElapsed);

		if (mPlayer.mShapeCollision.collidesWith(this)) {
			mPlayer.reset();
			if (mPlayer.mPlayerX < this.getX()) {
				if (mPlayer.mPlayerX % 2 == 1) {
					mPlayer.mPlayerX--;
				} else {
					mPlayer.mPlayerX = mPlayer.mPlayerX - 2;
				}
				mPlayer.mPlayerAnimation.stopAnimation(MainCharacter.RIGHT_TILE);
			} else {
				if (mPlayer.mPlayerX % 2 == 1) {
					mPlayer.mPlayerX++;
				} else {
					mPlayer.mPlayerX = mPlayer.mPlayerX + 2;
				}
				mPlayer.mPlayerAnimation.stopAnimation(MainCharacter.LEFT_TILE);
			}
			mPlayer.movePlayerX();
		}
	}

	public void setDebug(boolean pDebug) {
		if (pDebug) {
			this.setAlpha(1);
		} else {
			this.setAlpha(0);
		}
	}

}
