package com.incrediblapps.games.maquis.elements;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.incrediblapps.games.maquis.intefaces.Hideable;
import com.incrediblapps.games.maquis.players.actions.HideDoorAction;
import com.incrediblapps.games.maquis.players.actions.HidePlayerAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

public class HideDoor extends Door implements Hideable {

	private static final String TAG = "HideDoorElement";

	public HideDoor(SceneLevel pScene, float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			boolean pOpened, int pFloor, int pRoom) {
		super(pScene, pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pOpened, pFloor, pRoom);
	}

	public HideDoor(SceneLevel pScene, float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			boolean pOpened, int pFloor, int pRoom, int pGotodoor, int pDoorCenter) {
		super(pScene, pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pOpened, pFloor, pRoom);
		this.doorCenter = pDoorCenter;
		this.gotodoor = pGotodoor;
	}

	@Override
	public void doubleClickAction() {

		if (!mScene.mPlayer.mIsLocked) {
			if (mScene.mPlayer.mFloor == this.mFloor) {

				if (!mScene.mPlayer.mMovementsQueue.isEmpty()) {
					if (mScene.mPlayer.mMovementsQueue.element() instanceof MovementAction
							&& (((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_LEFT || ((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_RIGHT)) {
						mScene.mPlayer.mHasChanged = true;
						mScene.mPlayer.reset();
					} else {
						return;
					}
				}

				float targetX = this.getX() + doorCenter;
				if (targetX % 2 == 1) {
					targetX++;
				}

				if (mScene.mPlayer.mPlayerX < targetX) {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_RIGHT));
				} else {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_LEFT));
				}

				mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) this, DoorState.OPEN_DOOR));

				if (!mScene.mPlayer.mIsHidden) {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, mScene.mPlayer.mPlayerY + this.gotodoor, PlayerState.GO_TO_DOOR));
					mScene.mPlayer.mMovementsQueue.add(new HidePlayerAction(mScene.mPlayer, true));
					mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) this, DoorState.CLOSED_DOOR));
					mScene.mPlayer.mWhereIsHidden = this;
				} else {					
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, mScene.mPlayer.mPlayerY - this.gotodoor, PlayerState.TELEPORTATION));
					mScene.mPlayer.mMovementsQueue.add(new HidePlayerAction(mScene.mPlayer, false));
					mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) this, DoorState.CLOSED_DOOR));
					mScene.mPlayer.mWhereIsHidden = null;
				}
			}
		}
	}
}
