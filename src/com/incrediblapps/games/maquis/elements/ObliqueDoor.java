package com.incrediblapps.games.maquis.elements;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.util.Log;

import com.incrediblapps.games.maquis.players.actions.DoorAction;
import com.incrediblapps.games.maquis.players.actions.HideDoorAction;
import com.incrediblapps.games.maquis.players.actions.HidePlayerAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.main.MainCharacter;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

public class ObliqueDoor extends Door {

	public static final String TAG = "ObliqueDoorElement";

	private static final int CLOSED_TILE = 0;

	private static final int OPEN_TILE = 1;

	public boolean openable = true;

	public ObliqueDoor(SceneLevel pScene, float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			boolean pOpened, int pFloor) {
		super(pScene, pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pOpened, pFloor, 0);
		this.openTile = OPEN_TILE;
		this.closedTile = CLOSED_TILE;
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		super.onManagedUpdate(pSecondsElapsed);

		if (mScene.mPlayer.mShapeCollision.collidesWith(this)) {

			MovementAction movement = (MovementAction) mScene.mPlayer.mMovementsQueue.element();

			if (movement.mDoubleClicked && movement.mDoorClicked != null && movement.mDoorClicked.equals(this) && openable) {

				this.setIgnoreUpdate(true);

				mScene.mPlayer.reset();

				if (mScene.mPlayer.mPlayerX < this.getX()) {
					mScene.mPlayer.mPlayerAnimation.stopAnimation(MainCharacter.RIGHT_TILE);
				} else {
					mScene.mPlayer.mPlayerAnimation.stopAnimation(MainCharacter.LEFT_TILE);
				}

				mScene.mPlayer.mMovementsQueue.add(new DoorAction(this, DoorState.OPEN_DOOR));

				if (mScene.mPlayer.mPlayerX < this.getX()) {
					float targetX = this.getX() + this.mWidth + (float) (mScene.mPlayer.mShapeCollision.getWidth() * 0.5) + 1;
					if (targetX % 2 == 1) {
						targetX++;
					}
					mScene.mPlayer.mRoom++;
					Log.d(TAG, "player floor=" + mScene.mPlayer.mFloor + " room=" + mScene.mPlayer.mRoom);
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.THROUGH_DOOR_RIGHT));
					Log.d(TAG, "targetX=" + targetX);
				} else {
					float targetX = this.getX() - (float) (mScene.mPlayer.mShapeCollision.getWidth() * 0.5) - 1;
					if (targetX % 2 == 1) {
						targetX--;
					}
					mScene.mPlayer.mRoom--;
					Log.d(TAG, "player floor=" + mScene.mPlayer.mFloor + " room=" + mScene.mPlayer.mRoom);
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.THROUGH_DOOR_LEFT));
					Log.d(TAG, "targetX=" + targetX);
				}

				mScene.mPlayer.mMovementsQueue.add(new DoorAction(this, DoorState.CLOSED_DOOR));

			} else {
				mScene.mPlayer.reset();
				if (mScene.mPlayer.mPlayerX < this.getX()) {
					if (mScene.mPlayer.mPlayerX % 2 == 1) {
						mScene.mPlayer.mPlayerX--;
					} else {
						mScene.mPlayer.mPlayerX = mScene.mPlayer.mPlayerX - 2;
					}
					mScene.mPlayer.mPlayerAnimation.stopAnimation(MainCharacter.RIGHT_TILE);
				} else {
					if (mScene.mPlayer.mPlayerX % 2 == 1) {
						mScene.mPlayer.mPlayerX++;
					} else {
						mScene.mPlayer.mPlayerX = mScene.mPlayer.mPlayerX + 2;
					}
					mScene.mPlayer.mPlayerAnimation.stopAnimation(MainCharacter.LEFT_TILE);
				}
				mScene.mPlayer.movePlayerX();
				Log.d(TAG, "targetX=" + mScene.mPlayer.mPlayerX);
			}
		}
	}

	@Override
	public void doubleClickAction() {

		if (!mScene.mPlayer.mIsLocked) {
			if (mScene.mPlayer.mFloor == this.mFloor) {

				if (!mScene.mPlayer.mMovementsQueue.isEmpty()) {
					if (mScene.mPlayer.mMovementsQueue.element() instanceof MovementAction
							&& (((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_LEFT || ((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_RIGHT)) {
						mScene.mPlayer.mHasChanged = true;
						mScene.mPlayer.reset();
					} else {
						return;
					}
				}

				float targetX = this.getX();

				if (mScene.mPlayer.mIsHidden) {
					mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mScene.mPlayer.mWhereIsHidden, DoorState.OPEN_DOOR));
					mScene.mPlayer.mMovementsQueue.add(new HidePlayerAction(mScene.mPlayer, false));
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, mScene.mPlayer.mPlayerY - ((Door) mScene.mPlayer.mWhereIsHidden).gotodoor,
							PlayerState.TELEPORTATION));
					mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mScene.mPlayer.mWhereIsHidden, DoorState.CLOSED_DOOR));
					mScene.mPlayer.mWhereIsHidden = null;
				}

				if (mScene.mPlayer.mPlayerX < targetX) {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_RIGHT, true, this));
				} else {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_LEFT, true, this));
				}
			}
		}
	}

}
