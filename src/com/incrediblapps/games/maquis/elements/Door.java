package com.incrediblapps.games.maquis.elements;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.incrediblapps.games.maquis.scenes.SceneLevel;

public abstract class Door extends AnimatedElement {

	public enum DoorState {
		OPEN_DOOR, CLOSED_DOOR
	}

	private static final String TAG = "DoorElement";

	public boolean mOpened;

	public DoorState mState;

	public int openTile = 0;

	public int closedTile = 1;

	public int doorCenter = 32;

	public int gotodoor = 21;

	public Door(SceneLevel pScene, float pX, float pY, ITiledTextureRegion pTiledTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager,
			boolean pOpened, int pFloor, int pRoom) {
		super(pScene, pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pFloor, pRoom);
		this.mOpened = pOpened;
	}

}
