package com.incrediblapps.games.maquis.elements;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.incrediblapps.games.maquis.elements.Door.DoorState;
import com.incrediblapps.games.maquis.players.actions.HideDoorAction;
import com.incrediblapps.games.maquis.players.actions.HidePlayerAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

public class UselessAnimatedElement extends AnimatedElement {

	private static final String TAG = "UselessObject";

	private final int halfObject;

	public UselessAnimatedElement(SceneLevel pScene, float pX, float pY, ITiledTextureRegion pTiledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager, int pFloor, int pRoom) {
		super(pScene, pX, pY, pTiledTextureRegion, pVertexBufferObjectManager, pFloor, pRoom);
		this.halfObject = (int) (pTiledTextureRegion.getWidth() * 0.5f);
	}

	public void doubleClickAction() {

		if (!mScene.mPlayer.mIsLocked) {
			if (mScene.mPlayer.mFloor == this.mFloor) {

				if (!mScene.mPlayer.mMovementsQueue.isEmpty()) {
					if (mScene.mPlayer.mMovementsQueue.element() instanceof MovementAction
							&& (((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_LEFT || ((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_RIGHT)) {
						mScene.mPlayer.mHasChanged = true;
						mScene.mPlayer.reset();
					} else {
						return;
					}
				}

				float targetX = this.getX() + halfObject;
				if (targetX % 2 == 1) {
					targetX++;
				}

				if (mScene.mPlayer.mIsHidden) {
					mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mScene.mPlayer.mWhereIsHidden, DoorState.OPEN_DOOR));
					mScene.mPlayer.mMovementsQueue.add(new HidePlayerAction(mScene.mPlayer, false));
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, mScene.mPlayer.mPlayerY - ((Door) mScene.mPlayer.mWhereIsHidden).gotodoor,
							PlayerState.TELEPORTATION));
					mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mScene.mPlayer.mWhereIsHidden, DoorState.CLOSED_DOOR));
					mScene.mPlayer.mWhereIsHidden = null;
				}

				if (mScene.mPlayer.mPlayerX < targetX) {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_RIGHT));
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.CONFUSED_RIGHT));
				} else {
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_LEFT));
					mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.CONFUSED_LEFT));
				}
			}
		}
	}

}
