package com.incrediblapps.games.maquis.elements;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.os.SystemClock;
import android.util.Log;

import com.incrediblapps.games.maquis.MaquisMainActivity;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

public abstract class AnimatedElement extends AnimatedSprite {

	private static final String TAG = "AnimatedElement";

	protected SceneLevel mScene;

	/* Parameter to control the double-tap */
	// Set the double tap delay in milliseconds
	protected static final long DOUBLE_CLICK_MAX_DELAY = 400L;

	public static long thisTime = 0;

	public static long prevTime = 0;

	public static boolean firstTap = true;

	public int mFloor;

	public int mRoom;

	public AnimatedElement(SceneLevel pScene, float pX, float pY, ITiledTextureRegion pTiledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager, int pFloor, int pRoom) {
		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);
		this.mScene = pScene;
		this.mFloor = pFloor;
		this.mRoom = pRoom;
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
		mScene.mGestureTouchEvent = pSceneTouchEvent;

		if (pSceneTouchEvent.isActionDown()) {
			if (firstTap) {
				thisTime = SystemClock.uptimeMillis();
				firstTap = false;
				MaquisMainActivity.mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.d(TAG, "handled");
						if (!firstTap) {
							firstTap = !firstTap;
						}
					}
				}, 500);
			} else {
				prevTime = thisTime;
				thisTime = SystemClock.uptimeMillis();

				if (thisTime > prevTime) {
					if ((thisTime - prevTime) <= DOUBLE_CLICK_MAX_DELAY) {
						// We have detected a double tap!
						Log.d(TAG, "FloorDoor1: We have detected a double tap!");

						doubleClickAction();

						firstTap = true;

					} else {
						firstTap = true;
					}
				} else {
					firstTap = true;
				}
			}
		}
		return false;
	}

	public abstract void doubleClickAction();

}
