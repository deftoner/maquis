package com.incrediblapps.games.maquis.elements;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

import android.os.SystemClock;
import android.util.Log;

import com.incrediblapps.games.maquis.MaquisMainActivity;
import com.incrediblapps.games.maquis.elements.Door.DoorState;
import com.incrediblapps.games.maquis.players.actions.HideDoorAction;
import com.incrediblapps.games.maquis.players.actions.HidePlayerAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.actions.WindowAction;
import com.incrediblapps.games.maquis.players.main.MainCharacter;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;
import com.incrediblapps.games.maquis.scenes.SceneLevel;
import com.incrediblapps.games.maquis.utils.constants.Constants;

public class Window extends Rectangle {

	public enum WindowState {
		RIGHT_WINDOW, LEFT_WINDOW, CLOSED_WINDOW
	}

	private static final String TAG = "Window";

	protected SceneLevel mScene;

	/* Parameter to control the double-tap */
	// Set the double tap delay in milliseconds
	protected static final long DOUBLE_CLICK_MAX_DELAY = 400L;

	public static long thisTime = 0;

	public static long prevTime = 0;

	public static boolean firstTap = true;

	public int mFloor;

	public int mRoom;

	public Window(SceneLevel pScene, float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager, int pFloor,
			int pRoom) {
		super(pX + (float) (pWidth * 0.5), pY + (float) (pHeight * 0.5), pWidth, pHeight, pVertexBufferObjectManager);
		this.mScene = pScene;
		this.setColor(Color.GREEN);
		this.setDebug(false);
		this.mFloor = pFloor;
		this.mRoom = pRoom;
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		super.onManagedUpdate(pSecondsElapsed);

		if (mScene.mPlayer.mShapeCollision.collidesWith(this)) {

			MovementAction movement = (MovementAction) mScene.mPlayer.mMovementsQueue.element();

			if (movement.mDoubleClicked && movement.mDoorClicked == null) {

				mScene.mPlayer.reset();

				if (mScene.mPlayer.mPlayerX < this.getX()) {
					mScene.mPlayer.mPlayerAnimation.stopAnimation(MainCharacter.RIGHT_TILE);
				} else {
					mScene.mPlayer.mPlayerAnimation.stopAnimation(MainCharacter.LEFT_TILE);
				}

				this.setIgnoreUpdate(true);

				if (mScene.mPlayer.mPlayerX < this.getX()) {
					mScene.mPlayer.mMovementsQueue.add(new WindowAction(this, WindowState.RIGHT_WINDOW));
					mScene.mPlayer.mRoom++;
					Log.d(TAG, "player floor=" + mScene.mPlayer.mFloor + " room=" + mScene.mPlayer.mRoom);
				} else {
					mScene.mPlayer.mMovementsQueue.add(new WindowAction(this, WindowState.LEFT_WINDOW));
					mScene.mPlayer.mRoom--;
					Log.d(TAG, "player floor=" + mScene.mPlayer.mFloor + " room=" + mScene.mPlayer.mRoom);
				}
				mScene.mPlayer.mMovementsQueue.add(new WindowAction(this, WindowState.CLOSED_WINDOW));

			} else {
				mScene.mPlayer.reset();
				if (mScene.mPlayer.mPlayerX < this.getX()) {
					mScene.mPlayer.mPlayerX = mScene.mPlayer.mPlayerX - Constants.Player.RUN_VELOCITY;
					mScene.mPlayer.mPlayerAnimation.stopAnimation(MainCharacter.RIGHT_TILE);
				} else {
					mScene.mPlayer.mPlayerX = mScene.mPlayer.mPlayerX + Constants.Player.RUN_VELOCITY;
					mScene.mPlayer.mPlayerAnimation.stopAnimation(MainCharacter.LEFT_TILE);
				}
				mScene.mPlayer.movePlayerX();
			}
		}
	}

	public void setDebug(boolean pDebug) {
		if (pDebug) {
			this.setAlpha(1);
		} else {
			this.setAlpha(0);
		}
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {

		((SceneLevel) mScene).mGestureTouchEvent = pSceneTouchEvent;

		if (pSceneTouchEvent.isActionDown()) {

			if (firstTap) {
				thisTime = SystemClock.uptimeMillis();
				firstTap = false;

				MaquisMainActivity.mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						if (!firstTap) {
							firstTap = !firstTap;
						}
					}
				}, 500);
			} else {
				prevTime = thisTime;
				thisTime = SystemClock.uptimeMillis();

				if (thisTime > prevTime) {
					if ((thisTime - prevTime) <= DOUBLE_CLICK_MAX_DELAY) {

						doubleClickAction();

						firstTap = true;

					} else {
						firstTap = true;
					}
				} else {
					firstTap = true;
				}
			}
		}

		return false;
	}

	protected void doubleClickAction() {

		if (mScene.mPlayer.mFloor == this.mFloor) {

			if (!mScene.mPlayer.mMovementsQueue.isEmpty()) {
				if (mScene.mPlayer.mMovementsQueue.element() instanceof MovementAction
						&& (((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_LEFT || ((MovementAction) mScene.mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_RIGHT)) {
					mScene.mPlayer.mHasChanged = true;
					mScene.mPlayer.reset();
				} else {
					return;
				}
			}

			float targetX = this.getX();

			if (mScene.mPlayer.mIsHidden) {
				mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mScene.mPlayer.mWhereIsHidden, DoorState.OPEN_DOOR));
				mScene.mPlayer.mMovementsQueue.add(new HidePlayerAction(mScene.mPlayer, false));
				mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, mScene.mPlayer.mPlayerY - ((Door) mScene.mPlayer.mWhereIsHidden).gotodoor,
						PlayerState.TELEPORTATION));
				mScene.mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mScene.mPlayer.mWhereIsHidden, DoorState.CLOSED_DOOR));
				mScene.mPlayer.mWhereIsHidden = null;
			}

			if (mScene.mPlayer.mPlayerX < targetX) {
				mScene.mPlayer.mPlayerClimbing.setVisible(false);
				mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_RIGHT, true));
			} else {
				mScene.mPlayer.mMovementsQueue.add(new MovementAction(targetX, 0, PlayerState.RUNNING_LEFT, true));
			}
		}
	}

}
