package com.incrediblapps.games.maquis.players.actions;

import com.incrediblapps.games.maquis.elements.Door;
import com.incrediblapps.games.maquis.elements.Door.DoorState;

public class HideDoorAction extends DoorAction {

	public HideDoorAction() {
		super();
	}

	public HideDoorAction(Door mDoor, DoorState mState) {
		super(mDoor, mState);
	}

}