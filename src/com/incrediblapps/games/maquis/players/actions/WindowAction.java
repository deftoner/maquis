package com.incrediblapps.games.maquis.players.actions;

import com.incrediblapps.games.maquis.elements.Window;
import com.incrediblapps.games.maquis.elements.Window.WindowState;

public class WindowAction extends BaseAction {

	public Window mWindow;

	public WindowState mState;

	public WindowAction() {
	}

	public WindowAction(Window pWindow, WindowState pState) {
		super();
		this.mWindow = pWindow;
		this.mState = pState;
	}

}
