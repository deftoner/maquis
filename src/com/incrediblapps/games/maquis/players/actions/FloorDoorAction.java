package com.incrediblapps.games.maquis.players.actions;

import com.incrediblapps.games.maquis.elements.Door.DoorState;
import com.incrediblapps.games.maquis.elements.FloorDoor;

public class FloorDoorAction extends DoorAction {

	public FloorDoorAction(FloorDoor pDoor, DoorState pState, boolean pCheckGameOver) {
		this.mDoor = pDoor;
		this.mState = pState;
		this.mCheckGameOver = pCheckGameOver;
	}

}
