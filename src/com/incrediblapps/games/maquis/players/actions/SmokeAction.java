package com.incrediblapps.games.maquis.players.actions;

public class SmokeAction extends BaseAction {

	public boolean mLeft;

	public int mTime;

	public SmokeAction(boolean pLeft, int pTime) {
		this.mLeft = pLeft;
		this.mTime = pTime;
	}
}
