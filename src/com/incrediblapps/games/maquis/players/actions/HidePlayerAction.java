package com.incrediblapps.games.maquis.players.actions;

import com.incrediblapps.games.maquis.players.main.MainCharacter;

public class HidePlayerAction extends BaseAction {

	public MainCharacter mPlayer;

	public HidePlayerAction(MainCharacter mPlayer, boolean mHidden) {
		super();
		this.mPlayer = mPlayer;
		this.mPlayer.mIsHidden = mHidden;
	}

}
