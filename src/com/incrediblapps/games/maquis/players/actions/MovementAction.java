package com.incrediblapps.games.maquis.players.actions;

import com.incrediblapps.games.maquis.elements.ObliqueDoor;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;

public class MovementAction extends BaseAction {
	public float mTargetX;

	public float mTargetY;

	public PlayerState mState;

	public boolean mDoubleClicked = false;

	public ObliqueDoor mDoorClicked;

	public MovementAction(float mTargetX, float mTargetY, PlayerState mState) {
		this.mTargetX = mTargetX;
		this.mTargetY = mTargetY;
		this.mState = mState;
	}

	public MovementAction(float mTargetX, float mTargetY, PlayerState mState, boolean pDoubleClicked) {
		this.mTargetX = mTargetX;
		this.mTargetY = mTargetY;
		this.mState = mState;
		this.mDoubleClicked = pDoubleClicked;
	}

	public MovementAction(float mTargetX, float mTargetY, PlayerState mState, boolean pOblique, ObliqueDoor pDoor) {
		this.mTargetX = mTargetX;
		this.mTargetY = mTargetY;
		this.mState = mState;
		this.mDoubleClicked = pOblique;
		this.mDoorClicked = pDoor;
	}

}
