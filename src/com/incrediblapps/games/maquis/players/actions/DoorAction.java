package com.incrediblapps.games.maquis.players.actions;

import com.incrediblapps.games.maquis.elements.Door;
import com.incrediblapps.games.maquis.elements.Door.DoorState;

public class DoorAction extends BaseAction {

	public Door mDoor;

	public DoorState mState;

	public DoorAction() {
	}

	public DoorAction(Door mDoor, DoorState mState) {
		super();
		this.mDoor = mDoor;
		this.mState = mState;
	}

}
