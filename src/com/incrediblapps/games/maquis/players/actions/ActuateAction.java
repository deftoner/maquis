package com.incrediblapps.games.maquis.players.actions;

import com.incrediblapps.games.maquis.elements.ActionableElement;

public class ActuateAction extends BaseAction {

	public ActionableElement mElement;

	public ActuateAction(ActionableElement pActionableElement) {
		this.mElement = pActionableElement;
	}

}
