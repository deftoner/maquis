package com.incrediblapps.games.maquis.players.actions;

public class ChangeFloorAction extends BaseAction {

	public int mTargetFloor;

	public int mTargetRoom;

	public ChangeFloorAction(int pTargetFloor, int pTargetRoom) {
		this.mTargetFloor = pTargetFloor;
		this.mTargetRoom = pTargetRoom;
	}

}
