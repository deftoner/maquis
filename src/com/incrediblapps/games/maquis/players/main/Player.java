package com.incrediblapps.games.maquis.players.main;

import java.util.LinkedList;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.AnimatedSprite;

import com.incrediblapps.games.maquis.players.actions.BaseAction;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

public abstract class Player implements IUpdateHandler {

	public enum PlayerState {
		RUNNING_RIGHT, RUNNING_LEFT, WALKING_RIGHT, WALKING_LEFT, GO_TO_DOOR, TELEPORTATION, CONFUSED_RIGHT, CONFUSED_LEFT, THROUGH_DOOR_RIGHT,
		THROUGH_DOOR_LEFT, CROSS_WINDOW_LEFT, CROSS_WINDOW_RIGHT, RUNNING_TO_DOOR_RIGHT, RUNNING_TO_DOOR_LEFT, SMOKE_LEFT, SMOKE_RIGHT
	}

	public Player(SceneLevel pSceneLevel, float pPlayerX, float pPlayerY, int pFloor, int pRoom) {
		this.mSceneLevel = pSceneLevel;
		this.mPlayerX = pPlayerX;
		this.mPlayerY = pPlayerY;
		this.mFloor = pFloor;
		this.mRoom = pRoom;
		this.mStartFloor = pFloor;
		this.mStartRoom = pRoom;
		this.mStartPointX = pPlayerX;
		this.mStartPointY = pPlayerY;

		this.mMovementsQueue = new LinkedList<BaseAction>();
	}

	public float mPlayerX;

	public float mPlayerY;

	public SceneLevel mSceneLevel;

	public AnimatedSprite mPlayerAnimation;

	public LinkedList<BaseAction> mMovementsQueue;

	protected long waitTime;

	protected boolean gotTTime = false;

	public Rectangle mShapeCollision;

	public int mFloor;

	public int mRoom;

	public float mStartPointX;

	public float mStartPointY;

	public int mStartRoom;

	public int mStartFloor;

}
