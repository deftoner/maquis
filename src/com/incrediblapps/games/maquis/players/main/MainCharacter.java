package com.incrediblapps.games.maquis.players.main;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.AnimatedSprite.IAnimationListener;
import org.andengine.util.modifier.IModifier;

import android.os.SystemClock;
import android.util.Log;

import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.SFXManager;
import com.incrediblapps.games.maquis.SceneManager;
import com.incrediblapps.games.maquis.elements.ActionableElement;
import com.incrediblapps.games.maquis.elements.Door.DoorState;
import com.incrediblapps.games.maquis.elements.Window.WindowState;
import com.incrediblapps.games.maquis.intefaces.Hideable;
import com.incrediblapps.games.maquis.layers.GameOverLayer;
import com.incrediblapps.games.maquis.players.actions.ActuateAction;
import com.incrediblapps.games.maquis.players.actions.ChangeFloorAction;
import com.incrediblapps.games.maquis.players.actions.DoorAction;
import com.incrediblapps.games.maquis.players.actions.HidePlayerAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.actions.WindowAction;
import com.incrediblapps.games.maquis.players.enemies.Enemy;
import com.incrediblapps.games.maquis.scenes.SceneLevel;
import com.incrediblapps.games.maquis.utils.constants.Constants;

public class MainCharacter extends Player {

	private static final String TAG = "MainCharacter";

	private static final int GOTODOOR_TILE_END = 18;

	private static final int GOTODOOR_TILE_START = 14;

	private static final int LEFT_RUN_TILE_END = 6;

	private static final int RIGHT_RUN_TILE_END = 13;

	private static final int RIGHT_RUN_TILE_START = 8;

	private static final int LEFT_RUN_TILE_START = 1;

	public static final int LEFT_TILE = 0;

	public static final int RIGHT_TILE = 7;

	private static final int CONFUSED_RIGHT_TILE_START = 3;

	private static final int CONFUSED_RIGHT_TILE_END = 5;

	private static final int CONFUSED_LEFT_TILE_START = 0;

	private static final int CONFUSED_LEFT_TILE_END = 2;

	private static final int CLIMBING_LEFT_TILE_START = 0;

	private static final int CLIMBING_LEFT_TILE_END = 5;

	private static final int CLIMBING_RIGHT_TILE_START = 6;

	private static final int CLIMBING_RIGHT_TILE_END = 10;

	private static final int BACK_TILE = 19;

	private AnimatedSprite mPlayerConfused;

	public AnimatedSprite mPlayerClimbing;

	public boolean mHasChanged;

	public boolean mIsHidden = false;

	public Hideable mWhereIsHidden;

	public boolean mIsLocked = false;

	public Entity mChaseEntity;

	public MainCharacter(SceneLevel pLevel, float pX, float pY, int pFloor, int pRoom) {

		super(pLevel, pX, pY, pFloor, pRoom);

		final int anchorCenterWidth = (int) (ResourceManager.mPlayerTextureRegion.getWidth() * 0.5f);
		final int anchorCenterHeight = (int) (ResourceManager.mPlayerTextureRegion.getHeight() * 0.5f);

		// Create player sprite
		mPlayerAnimation = new AnimatedSprite(anchorCenterWidth, anchorCenterHeight, ResourceManager.mPlayerTextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager());

		mPlayerX -= anchorCenterWidth - 1;
		mPlayerAnimation.setX(mPlayerX);
		mPlayerAnimation.setY(mPlayerY);
		mPlayerAnimation.setZIndex(SceneLevel.LAYER_PLAYER);

		mStartPointX = mPlayerX;

		pLevel.mBackgroundSprite.attachChild(mPlayerAnimation);

		mPlayerConfused = new AnimatedSprite(anchorCenterWidth, anchorCenterHeight, ResourceManager.mPlayerConfusedTextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager());

		mPlayerConfused.setVisible(false);
		mPlayerConfused.setZIndex(SceneLevel.LAYER_PLAYER);

		pLevel.mBackgroundSprite.attachChild(mPlayerConfused);

		mPlayerClimbing = new AnimatedSprite(anchorCenterWidth, anchorCenterHeight, ResourceManager.mPlayerClimbingTextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager());

		mPlayerClimbing.setVisible(false);
		mPlayerClimbing.setZIndex(SceneLevel.LAYER_PLAYER);

		pLevel.mBackgroundSprite.attachChild(mPlayerClimbing);

		mShapeCollision = new Rectangle(anchorCenterWidth, pY, Constants.Player.SHAPE_WIDTH, Constants.Player.SHAPE_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		// Set alpha=1 for debug collisions
		mShapeCollision.setColor(1, 1, 1, 0);
		pLevel.mBackgroundSprite.attachChild(mShapeCollision);

		mChaseEntity = new Rectangle(0, 0, 10, 10, ResourceManager.engine.getVertexBufferObjectManager());
		mChaseEntity.setVisible(false);
		mSceneLevel.mBackgroundSprite.attachChild(mChaseEntity);
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {

		if (!mSceneLevel.mGameOver) {

			if (!mMovementsQueue.isEmpty()) {

				if (mMovementsQueue.element() instanceof MovementAction) {

					MovementAction mov = (MovementAction) mMovementsQueue.element();

					switch (mov.mState) {
						case RUNNING_LEFT:
							if (!mPlayerAnimation.isAnimationRunning() || mHasChanged) {
								animateRunLeft();
								mHasChanged = false;
							}
							if (mPlayerX == mov.mTargetX) {
								mPlayerAnimation.stopAnimation(LEFT_TILE);
								mMovementsQueue.pop();
							} else {
								mPlayerX = mPlayerX - Constants.Player.RUN_VELOCITY;
								movePlayerX();
							}
							break;

						case RUNNING_RIGHT:
							if (!mPlayerAnimation.isAnimationRunning() || mHasChanged) {
								animateRunRight();
								mHasChanged = false;
							}
							if (mPlayerX == mov.mTargetX) {
								mPlayerAnimation.stopAnimation(RIGHT_TILE);
								mMovementsQueue.pop();
							} else {
								mPlayerX = mPlayerX + Constants.Player.RUN_VELOCITY;
								movePlayerX();
							}
							break;

						case GO_TO_DOOR:
							if (!mPlayerAnimation.isAnimationRunning()) {
								animateGoToDoor();
							}
							if (mPlayerY == mov.mTargetY) {
								mPlayerAnimation.stopAnimation(RIGHT_TILE);
								movePlayerY();
								mMovementsQueue.pop();
							} else {
								mPlayerY = mPlayerY + Constants.Player.GOTODOOR_VELOCITY;
								movePlayerY();
							}
							break;

						case THROUGH_DOOR_LEFT:
							if (!mPlayerAnimation.isAnimationRunning() || mHasChanged) {
								animateRunLeft();
								mHasChanged = false;
							}
							if (mPlayerX == mov.mTargetX) {
								mPlayerAnimation.stopAnimation(LEFT_TILE);
								mMovementsQueue.pop();
								checkGameOver();
							} else {
								mPlayerX = mPlayerX - Constants.Player.RUN_VELOCITY;
								movePlayerX();
							}
							break;

						case THROUGH_DOOR_RIGHT:
							if (!mPlayerAnimation.isAnimationRunning() || mHasChanged) {
								animateRunRight();
								mHasChanged = false;
							}
							if (mPlayerX == mov.mTargetX) {
								mPlayerAnimation.stopAnimation(RIGHT_TILE);
								mMovementsQueue.pop();
								checkGameOver();
							} else {
								mPlayerX = mPlayerX + Constants.Player.RUN_VELOCITY;
								movePlayerX();
							}
							break;

						case TELEPORTATION:
							if (!gotTTime) {
								waitTime = SystemClock.uptimeMillis();
								setLeft();

								if (mov.mTargetY < ResourceManager.engine.getCamera().getYMin() || mov.mTargetY > ResourceManager.engine.getCamera().getYMax()) {
									mChaseEntity.setPosition(ResourceManager.engine.getCamera().getCenterX(), ResourceManager.engine.getCamera().getCenterY());
									ResourceManager.engine.getCamera().setChaseEntity(mChaseEntity);

									MoveYModifier moveYModifier = new MoveYModifier(0.5f, ResourceManager.engine.getCamera().getCenterY(), mov.mTargetY,
											new IEntityModifierListener() {

												@Override
												public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
												}

												@Override
												public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
													ResourceManager.engine.getCamera().setChaseEntity(null);
												}
											});

									mChaseEntity.registerEntityModifier(moveYModifier);
								}
								mPlayerY = mov.mTargetY;
								movePlayerY();
								gotTTime = true;
							}

							if ((SystemClock.uptimeMillis() - waitTime) >= 300) {
								mMovementsQueue.pop();
								gotTTime = false;
							}
							break;

						case CONFUSED_RIGHT:
							if (!mPlayerConfused.isAnimationRunning() || mHasChanged) {
								animateConfusedRight();
								mHasChanged = false;
							}
							mMovementsQueue.pop();
							break;

						case CONFUSED_LEFT:
							if (!mPlayerConfused.isAnimationRunning() || mHasChanged) {
								animateConfusedLeft();
								mHasChanged = false;
							}
							mMovementsQueue.pop();
							break;

						default:
							break;
					}
				} else if (mMovementsQueue.element() instanceof DoorAction) {
					DoorAction doorAction = (DoorAction) mMovementsQueue.element();
					if (doorAction.mState == DoorState.OPEN_DOOR) {
						SFXManager.playDoorOpen(1f, 1f);
						doorAction.mDoor.setCurrentTileIndex(doorAction.mDoor.openTile);
						mMovementsQueue.pop();
					} else if (doorAction.mState == DoorState.CLOSED_DOOR) {
						SFXManager.playDoorClose(1f, 1f);
						doorAction.mDoor.setCurrentTileIndex(doorAction.mDoor.closedTile);
						doorAction.mDoor.setIgnoreUpdate(false);
						mMovementsQueue.pop();
						if (doorAction.mCheckGameOver) {
							checkGameOver();
						}
					}
				} else if (mMovementsQueue.element() instanceof HidePlayerAction) {
					HidePlayerAction player = (HidePlayerAction) mMovementsQueue.element();
					player.mPlayer.mPlayerAnimation.setVisible(!player.mPlayer.mIsHidden);
					mMovementsQueue.pop();
					checkGameOver();
				} else if (mMovementsQueue.element() instanceof ChangeFloorAction) {
					mFloor = ((ChangeFloorAction) mMovementsQueue.element()).mTargetFloor;
					mRoom = ((ChangeFloorAction) mMovementsQueue.element()).mTargetRoom;
					Log.d(TAG, "player floor=" + mFloor + " room=" + mRoom);
					mMovementsQueue.pop();
					// checkGameOver();
				} else if (mMovementsQueue.element() instanceof ActuateAction) {
					ActionableElement element = ((ActuateAction) mMovementsQueue.element()).mElement;
					animateBack(element);
					mMovementsQueue.pop();
				} else if (mMovementsQueue.element() instanceof WindowAction) {
					WindowAction windowAction = (WindowAction) mMovementsQueue.element();
					if (windowAction.mState == WindowState.LEFT_WINDOW) {
						if (!mPlayerClimbing.isAnimationRunning()) {
							animateClimbingLeft();
							// checkGameOver();
						}
					} else if (windowAction.mState == WindowState.RIGHT_WINDOW) {
						if (!mPlayerClimbing.isAnimationRunning()) {
							animateClimbingRight();

						}
					} else if (windowAction.mState == WindowState.CLOSED_WINDOW) {
						windowAction.mWindow.setIgnoreUpdate(false);
						mMovementsQueue.pop();
						checkGameOver();
					}

				}
			}
		} else {
			// GAME OVER
		}
	}

	public void movePlayerX() {
		mPlayerAnimation.setX(mPlayerX);
		mShapeCollision.setX(mPlayerX);
	}

	public void moveClimbingPlayerX() {
		mPlayerClimbing.setX(mPlayerX);
		mPlayerAnimation.setX(mPlayerX);
		mShapeCollision.setX(mPlayerX);
	}

	public void movePlayerY() {
		mPlayerAnimation.setY(mPlayerY);
		mShapeCollision.setY(mPlayerY);
	}

	@Override
	public void reset() {
		mMovementsQueue.clear();
		mIsHidden = false;
		mWhereIsHidden = null;
		mIsLocked = false;
	}

	public void restart() {
		mPlayerAnimation.stopAnimation();
		mPlayerAnimation.setVisible(true);
		mPlayerConfused.setVisible(false);
		mPlayerClimbing.setVisible(false);
		mMovementsQueue.clear();
		mIsHidden = false;
		mWhereIsHidden = null;
		mIsLocked = false;
		mPlayerX = mStartPointX;
		movePlayerX();
		mPlayerY = mStartPointY;
		movePlayerY();
		setLeft();
		mFloor = mStartFloor;
		mRoom = mStartRoom;
	}

	private void animateRunRight() {
		mPlayerAnimation.setVisible(true);
		mPlayerConfused.setVisible(false);
		mPlayerAnimation.animate(new long[] { Constants.Player.PLAYER_FRAMES, Constants.Player.PLAYER_FRAMES, Constants.Player.PLAYER_FRAMES,
				Constants.Player.PLAYER_FRAMES, Constants.Player.PLAYER_FRAMES, Constants.Player.PLAYER_FRAMES },
				RIGHT_RUN_TILE_START,
				RIGHT_RUN_TILE_END,
				true);
	}

	private void animateRunLeft() {
		mPlayerAnimation.setVisible(true);
		mPlayerConfused.setVisible(false);
		mPlayerAnimation.animate(new long[] { Constants.Player.PLAYER_FRAMES, Constants.Player.PLAYER_FRAMES, Constants.Player.PLAYER_FRAMES,
				Constants.Player.PLAYER_FRAMES, Constants.Player.PLAYER_FRAMES, Constants.Player.PLAYER_FRAMES }, LEFT_RUN_TILE_START, LEFT_RUN_TILE_END, true);
	}

	private void animateGoToDoor() {
		mPlayerAnimation.animate(new long[] { Constants.Player.PLAYER_GOTODOOR_FRAMES, Constants.Player.PLAYER_GOTODOOR_FRAMES,
				Constants.Player.PLAYER_GOTODOOR_FRAMES, Constants.Player.PLAYER_GOTODOOR_FRAMES, Constants.Player.PLAYER_GOTODOOR_FRAMES },
				GOTODOOR_TILE_START,
				GOTODOOR_TILE_END,
				true);
	}

	private void animateConfusedRight() {
		mPlayerAnimation.setVisible(false);
		mPlayerConfused.setX(mPlayerX);
		mPlayerConfused.setY(mPlayerY);
		mPlayerConfused.setVisible(true);
		mPlayerConfused.animate(new long[] { Constants.Player.PLAYER_CONFUSED_FRAMES, Constants.Player.PLAYER_CONFUSED_FRAMES,
				Constants.Player.PLAYER_CONFUSED_FRAMES }, CONFUSED_RIGHT_TILE_START, CONFUSED_RIGHT_TILE_END, false, new IAnimationListener() {

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
			}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				mPlayerAnimation.setVisible(true);
				mPlayerConfused.setVisible(false);
				setRight();
			}
		});
	}

	private void animateConfusedLeft() {
		mPlayerAnimation.setVisible(false);
		mPlayerConfused.setX(mPlayerX);
		mPlayerConfused.setY(mPlayerY);
		mPlayerConfused.setVisible(true);
		mPlayerConfused.animate(new long[] { Constants.Player.PLAYER_CONFUSED_FRAMES, Constants.Player.PLAYER_CONFUSED_FRAMES,
				Constants.Player.PLAYER_CONFUSED_FRAMES }, CONFUSED_LEFT_TILE_START, CONFUSED_LEFT_TILE_END, false, new IAnimationListener() {

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
			}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				mPlayerAnimation.setVisible(true);
				mPlayerConfused.setVisible(false);
				setLeft();
			}
		});
	}

	public void animateClimbingRight() {
		mPlayerAnimation.setVisible(false);
		mPlayerClimbing.setX(mPlayerX);
		mPlayerClimbing.setY(mPlayerY);
		mPlayerClimbing.setVisible(true);
		mPlayerClimbing.animate(new long[] { Constants.Player.PLAYER_CLIMBING_FRAMES, Constants.Player.PLAYER_CLIMBING_FRAMES,
				Constants.Player.PLAYER_CLIMBING_FRAMES, Constants.Player.PLAYER_CLIMBING_FRAMES, Constants.Player.PLAYER_CLIMBING_FRAMES },
				CLIMBING_RIGHT_TILE_START,
				CLIMBING_RIGHT_TILE_END,
				false,
				new IAnimationListener() {

					@Override
					public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
					}

					@Override
					public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {
					}

					@Override
					public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
						switch (pNewFrameIndex) {
							case 0:
								mPlayerX = mPlayerX + 6;
								break;
							case 1:
								mPlayerX = mPlayerX + 2;
								break;
							case 2:
								mPlayerX = mPlayerX + 4;
								break;
							case 3:
								mPlayerX = mPlayerX + 6;
								break;
							case 4:
								mPlayerX = mPlayerX + 40;
								break;

							default:
								break;
						}
						moveClimbingPlayerX();
					}

					@Override
					public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
						mPlayerAnimation.setVisible(true);
						mPlayerClimbing.setVisible(false);
						setRight();
						mIsLocked = false;
						mMovementsQueue.pop();
					}
				});
	}

	public void animateClimbingLeft() {
		mPlayerAnimation.setVisible(false);
		mPlayerClimbing.setX(mPlayerX);
		mPlayerClimbing.setY(mPlayerY);
		mPlayerClimbing.setVisible(true);
		mPlayerClimbing.animate(new long[] { Constants.Player.PLAYER_CLIMBING_FRAMES, Constants.Player.PLAYER_CLIMBING_FRAMES,
				Constants.Player.PLAYER_CLIMBING_FRAMES, Constants.Player.PLAYER_CLIMBING_FRAMES, Constants.Player.PLAYER_CLIMBING_FRAMES,
				Constants.Player.PLAYER_CLIMBING_FRAMES }, CLIMBING_LEFT_TILE_START, CLIMBING_LEFT_TILE_END, false, new IAnimationListener() {

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
				switch (pNewFrameIndex) {
					case 0:
						mPlayerX = mPlayerX - 14;
						break;
					case 1:
						mPlayerX = mPlayerX - 2;
						break;
					case 2:
						mPlayerX = mPlayerX - 4;
						break;
					case 3:
						mPlayerX = mPlayerX - 6;
						break;
					case 4:
						mPlayerX = mPlayerX - 34;
						break;
					case 5:
						mPlayerX = mPlayerX - 6;
						break;

					default:
						break;
				}
				moveClimbingPlayerX();

			}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				mMovementsQueue.pop();
				mIsLocked = true;
			}
		});
	}

	public void setLeft() {
		mPlayerAnimation.setCurrentTileIndex(LEFT_TILE);
	}

	public void setRight() {
		mPlayerAnimation.setCurrentTileIndex(RIGHT_TILE);
	}

	public void animateBack(final ActionableElement element) {
		int heading = mPlayerAnimation.getCurrentTileIndex();
		mPlayerAnimation.animate(new long[] { Constants.Player.BACK_FRAMES, Constants.Player.BACK_FRAMES, Constants.Player.BACK_FRAMES }, new int[] { heading,
				BACK_TILE, heading }, false, new IAnimationListener() {

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
				if (pNewFrameIndex == 1) {
					element.mActuated = true;
					element.setCurrentTileIndex(element.mActuatedTile);
					element.mHudObject.setCurrentTileIndex(Constants.HUD.GOT_TILE);
				}
			}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
			}
		});
	}

	private boolean checkGameOver() {
		if (!this.mIsHidden) {
			for (Enemy enemy : mSceneLevel.mEnemies) {
				if (enemy.mFloor == this.mFloor && enemy.mRoom == this.mRoom) {
					Log.d(TAG, " -----------------GAME OVER----------------- ");
					mSceneLevel.mGameOver = true;
					if (this.mPlayerAnimation.isAnimationRunning()) {
						this.mPlayerAnimation.stopAnimation();
					}
					if (enemy.mPlayerX > this.mPlayerX) {
						enemy.animateHaltLeft();
						this.setRight();
					} else {
						enemy.animateHaltRight();
						this.setLeft();
					}
					SceneManager.getInstance().showLayer(GameOverLayer.getInstance(mSceneLevel), false, false, true);
					return true;
				}
			}
			return false;
		} else {
			return false;
		}
	}

}
