package com.incrediblapps.games.maquis.players.enemies;

import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.scenes.SceneLevel;
import com.incrediblapps.games.maquis.utils.constants.Constants;

public class Soldier1 extends Enemy {

	public Soldier1(SceneLevel pLevel, float pX, float pY, int pFloor, int pRoom) {
		super(pLevel, pX, pY, pFloor, pRoom, ResourceManager.mSoldier1TextureRegion);

		this.right_tile = 0;
		this.left_tile = 8;
		this.right_run_tile_start = 1;
		this.right_run_tile_end = 6;
		this.left_run_tile_start = 9;
		this.left_run_tile_end = 14;
		this.smoke_right_start = 0;
		this.smoke_right_end = 0;
		this.smoke_left_start = 0;
		this.smoke_left_end = 0;
		this.halt_right = 7;
		this.halt_left = 15;
		this.runVelocity = Constants.Enemies.Soldier1.RUN_VELOCITY;
		this.walkFrames = Constants.Enemies.Soldier1.WALK_FRAMES;
	}

}
