package com.incrediblapps.games.maquis.players.enemies;

import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.scenes.SceneLevel;
import com.incrediblapps.games.maquis.utils.constants.Constants;

public class Maqui1 extends Enemy {

	public Maqui1(SceneLevel pLevel, float pX, float pY, int pFloor, int pRoom) {
		super(pLevel, pX, pY, pFloor, pRoom, ResourceManager.mMaquiTextureRegion);

		this.right_tile = 0;
		this.left_tile = 12;
		this.right_run_tile_start = 1;
		this.right_run_tile_end = 6;
		this.left_run_tile_start = 13;
		this.left_run_tile_end = 18;
		this.smoke_right_start = 8;
		this.smoke_right_end = 10;
		this.smoke_left_start = 20;
		this.smoke_left_end = 22;
		this.halt_right = 11;
		this.halt_left = 23;
		this.runVelocity = Constants.Enemies.Maqui.RUN_VELOCITY;
		this.walkFrames = Constants.Enemies.Maqui.WALK_FRAMES;
	}

}
