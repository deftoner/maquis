package com.incrediblapps.games.maquis.players.enemies;

import java.security.spec.MGF1ParameterSpec;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.AnimatedSprite.IAnimationListener;
import org.andengine.opengl.texture.region.ITiledTextureRegion;

import android.os.SystemClock;
import android.util.Log;

import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.SceneManager;
import com.incrediblapps.games.maquis.elements.Door.DoorState;
import com.incrediblapps.games.maquis.layers.GameOverLayer;
import com.incrediblapps.games.maquis.players.actions.DoorAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.actions.SmokeAction;
import com.incrediblapps.games.maquis.players.actions.WaitAction;
import com.incrediblapps.games.maquis.players.main.Player;
import com.incrediblapps.games.maquis.scenes.SceneLevel;
import com.incrediblapps.games.maquis.utils.constants.Constants;

public class Enemy extends Player {

	private static final String TAG = "Enemy";

	protected int right_tile;

	protected int left_tile;

	protected int right_run_tile_start;

	protected int right_run_tile_end;

	protected int left_run_tile_start;

	protected int left_run_tile_end;

	protected int smoke_right_start;

	protected int smoke_right_end;

	protected int smoke_left_start;

	protected int smoke_left_end;

	protected int halt_right;

	protected int halt_left;

	protected float runVelocity = 1f;

	protected long walkFrames = 200;

	public int mMovementIndex = 0;

	public Enemy(SceneLevel pLevel, float pX, float pY, int pFloor, int pRoom, ITiledTextureRegion pTiledTexturedRegion) {

		super(pLevel, pX, pY, pFloor, pRoom);

		this.mPlayerAnimation = new AnimatedSprite(0, 0, pTiledTexturedRegion, ResourceManager.engine.getVertexBufferObjectManager());

		this.mPlayerAnimation.setX(mPlayerX);
		this.mPlayerAnimation.setY(mPlayerY);
		this.mPlayerAnimation.setZIndex(SceneLevel.LAYER_PLAYER);

		this.mSceneLevel.mBackgroundSprite.attachChild(mPlayerAnimation);
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {

		if (!mSceneLevel.mGameOver) {
			if (!mMovementsQueue.isEmpty()) {
				if (mMovementsQueue.get(mMovementIndex) instanceof MovementAction) {

					MovementAction mov = (MovementAction) mMovementsQueue.get(mMovementIndex);

					switch (mov.mState) {
						case RUNNING_LEFT:
							if (!mPlayerAnimation.isAnimationRunning()) {
								animateRunLeft();
							}
							if (mPlayerX == mov.mTargetX) {
								mPlayerAnimation.stopAnimation(left_tile);
								increaseIndex();
							} else {
								mPlayerX = mPlayerX - runVelocity;
								movePlayerX();
							}
							break;

						case RUNNING_RIGHT:
							if (!mPlayerAnimation.isAnimationRunning()) {
								animateRunRight();
							}
							if (mPlayerX == mov.mTargetX) {
								mPlayerAnimation.stopAnimation(right_tile);
								increaseIndex();
							} else {
								mPlayerX = mPlayerX + runVelocity;
								movePlayerX();
							}
							break;

						case RUNNING_TO_DOOR_LEFT:
							if (!mPlayerAnimation.isAnimationRunning()) {
								animateRunLeft();
							}
							if (mPlayerX == mov.mTargetX) {
								increaseIndex();
							} else {
								mPlayerX = mPlayerX - runVelocity;
								movePlayerX();
							}
							break;

						case RUNNING_TO_DOOR_RIGHT:
							if (!mPlayerAnimation.isAnimationRunning()) {
								animateRunRight();
							}
							if (mPlayerX == mov.mTargetX) {
								increaseIndex();
							} else {
								mPlayerX = mPlayerX + runVelocity;
								movePlayerX();
							}
							break;
						default:
							break;
					}
				} else if (mMovementsQueue.get(mMovementIndex) instanceof DoorAction) {
					DoorAction doorAction = (DoorAction) mMovementsQueue.get(mMovementIndex);
					if (doorAction.mState == DoorState.OPEN_DOOR) {
						doorAction.mDoor.setCurrentTileIndex(doorAction.mDoor.openTile);
						increaseIndex();
					} else if (doorAction.mState == DoorState.CLOSED_DOOR) {
						doorAction.mDoor.setCurrentTileIndex(doorAction.mDoor.closedTile);
						increaseIndex();
						int currentTile = mPlayerAnimation.getCurrentTileIndex();
						if (currentTile >= right_run_tile_start && currentTile <= right_run_tile_end) {
							mRoom++;
						} else {
							mRoom--;
						}
						checkGameOver();
					}
				} else if (mMovementsQueue.get(mMovementIndex) instanceof WaitAction) {
					WaitAction wait = (WaitAction) mMovementsQueue.get(mMovementIndex);
					if (!gotTTime) {
						waitTime = SystemClock.uptimeMillis();
						gotTTime = true;
					}

					if ((SystemClock.uptimeMillis() - waitTime) >= wait.mTime) {
						increaseIndex();
						gotTTime = false;
					}
				} else if (mMovementsQueue.get(mMovementIndex) instanceof SmokeAction) {
					SmokeAction smoke = (SmokeAction) mMovementsQueue.get(mMovementIndex);
					if (!gotTTime) {
						waitTime = SystemClock.uptimeMillis();
						gotTTime = true;
						if (smoke.mLeft) {
							animateSmokeLeft();
						} else {
							animateSmokeRight();
						}
					}

					if ((SystemClock.uptimeMillis() - waitTime) >= smoke.mTime) {
						increaseIndex();
						gotTTime = false;
						mPlayerAnimation.stopAnimation();
					}
				}
			}
		}
	}

	private void animateSmokeRight() {
		mPlayerAnimation.animate(new long[] { 400, 400 }, smoke_right_start, smoke_right_start + 1, false, new IAnimationListener() {

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
			}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				mPlayerAnimation.animate(new long[] { 1000, 1000 }, new int[] { smoke_right_start + 1, smoke_right_end }, true);

			}
		});
	}

	private void animateSmokeLeft() {
		mPlayerAnimation.animate(new long[] { 400, 400 }, smoke_left_start, smoke_left_start + 1, false, new IAnimationListener() {

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {
			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
			}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				mPlayerAnimation.animate(new long[] { 1000, 1000 }, new int[] { smoke_left_start + 1, smoke_left_end }, true);

			}
		});
	}

	private void increaseIndex() {
		mMovementIndex++;
		if (mMovementIndex == mMovementsQueue.size()) {
			mMovementIndex = 0;
		}
	}

	public void movePlayerX() {
		mPlayerAnimation.setX(mPlayerX);
	}

	public void movePlayerY() {
		mPlayerAnimation.setY(mPlayerY);
	}

	@Override
	public void reset() {
	}

	public void restart() {		
		mPlayerX = mStartPointX;
		movePlayerX();
		mPlayerY = mStartPointY;
		movePlayerY();
		mMovementIndex = 0;
		mFloor = mStartFloor;
		mRoom = mStartRoom;
	}

	private void animateRunRight() {
		mPlayerAnimation.animate(new long[] { walkFrames, walkFrames, walkFrames, walkFrames, walkFrames, walkFrames },
				right_run_tile_start,
				right_run_tile_end,
				true);
	}

	private void animateRunLeft() {
		mPlayerAnimation.animate(new long[] { walkFrames, walkFrames, walkFrames, walkFrames, walkFrames, walkFrames },
				left_run_tile_start,
				left_run_tile_end,
				true);
	}

	public void animateHaltLeft() {
		mPlayerAnimation.stopAnimation(halt_left);
	}

	public void animateHaltRight() {
		mPlayerAnimation.stopAnimation(halt_right);
	}

	public void setLeft() {
		mPlayerAnimation.setCurrentTileIndex(left_tile);
	}

	public void setRight() {
		mPlayerAnimation.setCurrentTileIndex(right_tile);
	}

	private boolean checkGameOver() {
		if (!mSceneLevel.mPlayer.mIsHidden) {
			if (this.mFloor == mSceneLevel.mPlayer.mFloor && this.mRoom == mSceneLevel.mPlayer.mRoom) {
				Log.d(TAG, " -----------------GAME OVER----------------- ");
				mSceneLevel.mGameOver = true;
				if (mSceneLevel.mPlayer.mPlayerAnimation.isAnimationRunning()) {
					mSceneLevel.mPlayer.mPlayerAnimation.stopAnimation();
				}
				if (mSceneLevel.mPlayer.mPlayerX < this.mPlayerX) {
					this.animateHaltLeft();
					mSceneLevel.mPlayer.setRight();
				} else {
					this.animateHaltRight();
					mSceneLevel.mPlayer.setLeft();
				}
				SceneManager.getInstance().showLayer(GameOverLayer.getInstance(mSceneLevel), false, false, true);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
