package com.incrediblapps.games.maquis.menus.levelselector;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.util.Log;

import com.incrediblapps.games.maquis.SceneManager;
import com.incrediblapps.games.maquis.scenes.ManagedGameScene;
import com.incrediblapps.games.maquis.scenes.SceneLevel1;
import com.incrediblapps.games.maquis.scenes.SceneLevel6;

public class LevelSelectorButton extends AnimatedSprite {

	/*
	 * The LevelTile should keep track of level number and lock status. Feel
	 * free to add additional data within level tiles
	 */

	private static final String TAG = "LevelSelectorButton";

	public final int mLevelIndex;

	public final int mChapterIndex;

	private final boolean mIsLocked;

	// private final Font mFont;

	// private Text mTileText;

	/*
	 * Each level tile will be sized according to the constant TILE_DIMENSION
	 * within the LevelSelector class
	 */
	public LevelSelectorButton(final int pLevelIndex, final int pChapterIndex, final boolean pIsLocked, final float pX, final float pY,
			final ITiledTextureRegion pTiledTextureRegion, final VertexBufferObjectManager pVertexBufferObjectManager) {

		// (float pX, float pY, boolean pIsLocked, int pLevelNumber,
		// ITextureRegion pTextureRegion, Font pFont) {

		super(pX, pY, pTiledTextureRegion, pVertexBufferObjectManager);

		/* Initialize the necessary variables for the LevelTile */

		// this.mFont = ResourceManager.mFontWhite;
		this.mIsLocked = pIsLocked;
		this.mLevelIndex = pLevelIndex;
		this.mChapterIndex = pChapterIndex;
	}

	/*
	 * Method used to obtain whether or not this level tile represents a level
	 * which is currently locked
	 */
	public boolean isLocked() {
		return this.mIsLocked;
	}

	/* Method used to obtain this specific level tiles level number */
	public int getLevelIndex() {
		return this.mLevelIndex;
	}

	/*
	 * Attach the LevelTile's text to itself based on whether it's locked or
	 * not. If not, then the level number will be displayed on the level tile.
	 */
	// public void attachText() {
	//
	// String tileTextString = null;
	//
	// /* If the tile's text is currently null... */
	// if (this.mTileText == null) {
	//
	// /*
	// * Determine the tile's string based on whether it's locked or not
	// */
	// if (this.mIsLocked) {
	// tileTextString = "Locked";
	// } else {
	// tileTextString = String.valueOf(this.mLevelIndex);
	// }
	//
	// /*
	// * Setup the text position to be placed in the center of the tile
	// */
	// final float textPositionX = this.mHeight * 0.5f;
	// final float textPositionY = textPositionX;
	//
	// /* Create the tile's text in the center of the tile */
	// this.mTileText = new Text(textPositionX, textPositionY, this.mFont,
	// tileTextString, tileTextString.length(),
	// ResourceManager.engine.getVertexBufferObjectManager());
	//
	// /* Attach the Text to the LevelTile */
	// this.attachChild(mTileText);
	// }
	// }

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {

		/* If a level tile is initially pressed down on */
		if (pSceneTouchEvent.isActionDown()) {
			/* If this level tile is locked... */
			if (this.mIsLocked) {
				/* Level Locked event... */

			} else {
				/*
				 * Tile unlocked event... This event would likely prompt level
				 * loading but without getting too complicated we will simply
				 * set the Scene's background color to green
				 */

				ManagedGameScene mLevel = null;

				Log.d(TAG, "Level selected: " + mLevelIndex);

				switch (mLevelIndex) {
					case 1:
						mLevel = new SceneLevel1();
						break;

					case 2:
						//mLevel = new SceneLevel2();
						break;

					case 6:
						mLevel = new SceneLevel6();
						break;

					default:
						break;
				}

				if (mLevel != null) {
					SceneManager.getInstance().showScene(mLevel);
				}

				/**
				 * Example level loading: LevelSelector.this.hide();
				 * SceneManager.loadLevel(this.mLevelNumber);
				 */
			}
			return true;
		}

		return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
	}
}
