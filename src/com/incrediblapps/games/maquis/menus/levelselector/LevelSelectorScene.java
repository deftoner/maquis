package com.incrediblapps.games.maquis.menus.levelselector;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.PinchZoomDetector;
import org.andengine.input.touch.detector.PinchZoomDetector.IPinchZoomDetectorListener;

import android.util.Log;
import android.view.GestureDetector;

import com.incrediblapps.games.maquis.MaquisMainActivity;
import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.scenes.ManagedScene;
import com.incrediblapps.games.maquis.utils.constants.Constants;

public class LevelSelectorScene extends ManagedScene implements IPinchZoomDetectorListener, IOnSceneTouchListener {

	private static final LevelSelectorScene INSTANCE = new LevelSelectorScene();

	private static final String TAG = "LevelSelectorScene";

	private static final float ZOOM_FACTOR_MAX = 1.2f;

	private static final float ZOOM_FACTOR_MIN = 0.8f;

	// ====================================================
	// INSTANCE GETTERS
	// ====================================================
	public static LevelSelectorScene getInstance() {
		return INSTANCE;
	}

	// ====================================================
	// VARIABLES
	// ====================================================

	private AnimatedSprite mMap;

	// Zoom detector
	protected PinchZoomDetector mPinchZoomDetector;

	protected static GestureDetector mGestureDetector;

	public TouchEvent mGestureTouchEvent;

	protected float mInitialTouchZoomFactor;

	// Initial scene touch coordinates on ACTION_DOWN
	protected float mInitialTouchX;

	protected float mInitialTouchY;

	protected SmoothCamera mCamera;

	private int mMaxChapter;

	/* Variable containing the current max level unlocked */
	private int mMaxLevel;

	// ====================================================
	// METHODS
	// ====================================================

	@Override
	public Scene onLoadingScreenLoadAndShown() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onLoadScene() {
		Log.d(TAG, "onLoadScene()");

		this.mMaxChapter = MaquisMainActivity.getIntFromSharedPreferences(MaquisMainActivity.SHARED_PREFS_MAX_CHAPTER);
		this.mMaxLevel = MaquisMainActivity.getIntFromSharedPreferences(MaquisMainActivity.SHARED_PREFS_MAX_LEVEL);

		this.setOnSceneTouchListener(this);

		// this.setBackgroundEnabled(true);
		// setBackground(new Background(0.5f, 0.5f, 0.5f, 0.5f));

		mCamera = (SmoothCamera) ResourceManager.engine.getCamera();
		mCamera.setBounds(0, 0, Constants.LevelSelector.WIDTH, Constants.LevelSelector.HEIGHT);

		// Register this activity as our zoom detector listener and enable it
		mPinchZoomDetector = new PinchZoomDetector(this);
		mPinchZoomDetector.setEnabled(true);

		final int halfTextureWidth = (int) (ResourceManager.mMapLevelSelectorTextureRegion.getWidth() * 0.5f);
		final int halfTextureHeight = (int) (ResourceManager.mMapLevelSelectorTextureRegion.getHeight() * 0.5f);

		this.mMap = new AnimatedSprite(halfTextureWidth, halfTextureHeight, ResourceManager.mMapLevelSelectorTextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager());
		this.attachChild(mMap);
		mMap.animate(250);

		createZone1();
	}

	private void createZone1() {

		/* Create a level1 tile */
		LevelSelectorButton levelTile1 = new LevelSelectorButton(1, 1, false, Constants.LevelSelector.FLAG_LEVEL1_POSITION_X,
				Constants.LevelSelector.FLAG_LEVEL1_POSITION_Y, ResourceManager.mLevelButtonTextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager());
		levelTile1.setAnchorCenter(0, 0);
		levelTile1.setPosition(Constants.LevelSelector.FLAG_LEVEL1_POSITION_X, Constants.LevelSelector.FLAG_LEVEL1_POSITION_Y);
		levelTile1.animate(250);
		this.registerTouchArea(levelTile1);
		this.attachChild(levelTile1);

		/* Create a level6 tile */
		LevelSelectorButton levelTile2 = new LevelSelectorButton(6, 1, false, Constants.LevelSelector.FLAG_LEVEL6_POSITION_X,
				Constants.LevelSelector.FLAG_LEVEL6_POSITION_Y, ResourceManager.mLevelButtonTextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager());
		levelTile2.setAnchorCenter(0, 0);
		levelTile2.setPosition(Constants.LevelSelector.FLAG_LEVEL6_POSITION_X, Constants.LevelSelector.FLAG_LEVEL6_POSITION_Y);
		levelTile2.animate(250);
		this.registerTouchArea(levelTile2);
		this.attachChild(levelTile2);
	}

	@Override
	public void onShowScene() {
		Log.d(TAG, "onShowScene()");

		// mCamera.setCenter(mCamera.getWidth() / 2, mCamera.getHeight() / 2);
		mCamera.setCenterDirect(Constants.LevelSelector.FLAG_LEVEL1_POSITION_X, Constants.LevelSelector.FLAG_LEVEL1_POSITION_Y);
		mCamera.setZoomFactorDirect(0.95f);
	}

	@Override
	public void onHideScene() {		
	}

	@Override
	public void onUnloadScene() {
		Log.d(TAG, "onUnloadScene()");
	}

	@Override
	public boolean onSceneTouchEvent(final Scene pScene, final TouchEvent pSceneTouchEvent) {

		mPinchZoomDetector.onTouchEvent(pSceneTouchEvent);

		if (pSceneTouchEvent.isActionDown()) {
			mInitialTouchX = pSceneTouchEvent.getX();
			mInitialTouchY = pSceneTouchEvent.getY();
		}

		if (pSceneTouchEvent.isActionMove()) {
			final float touchOffsetX = mInitialTouchX - pSceneTouchEvent.getX();
			final float touchOffsetY = mInitialTouchY - pSceneTouchEvent.getY();

			mCamera.setCenter(mCamera.getCenterX() + touchOffsetX, mCamera.getCenterY() + touchOffsetY);

			return true;
		}
		return true;
	}

	@Override
	public void onPinchZoomStarted(PinchZoomDetector pPinchZoomDetector, TouchEvent pSceneTouchEvent) {
		// Obtain the initial zoom factor on pinch detection
		mInitialTouchZoomFactor = mCamera.getZoomFactor();
	}

	@Override
	public void onPinchZoom(PinchZoomDetector pPinchZoomDetector, TouchEvent pTouchEvent, float pZoomFactor) {
		// Calculate the zoom offset
		final float newZoomFactor = mInitialTouchZoomFactor * pZoomFactor;

		// Apply the zoom offset to the camera, allowing zooming of between
		// (default) 1x and 2x
		if (newZoomFactor < ZOOM_FACTOR_MAX && newZoomFactor > ZOOM_FACTOR_MIN)
			mCamera.setZoomFactorDirect(newZoomFactor);
	}

	@Override
	public void onPinchZoomFinished(PinchZoomDetector pPinchZoomDetector, TouchEvent pTouchEvent, float pZoomFactor) {
		// Calculate the zoom offset
		final float newZoomFactor = mInitialTouchZoomFactor * pZoomFactor;

		// Apply the zoom offset to the camera, allowing zooming of between
		// (default) 1x and 2x
		if (newZoomFactor < ZOOM_FACTOR_MAX && newZoomFactor > ZOOM_FACTOR_MIN)
			mCamera.setZoomFactorDirect(newZoomFactor);
	}

}