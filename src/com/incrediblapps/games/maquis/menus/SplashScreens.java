package com.incrediblapps.games.maquis.menus;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.FadeInModifier;
import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.ScaleAtModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.IModifier.IModifierListener;

import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.SceneManager;

/**
 * The SplashScreen class uses entity modifiers and resolution-independent
 * positioning to show the splash screens of the game. Each logo is clickable
 * and starts an Intent related to the logo.
 * 
 * This class is unique because it is seen only at the beginning of the game.
 * After it is hidden, it's never used again. It also has to load as quickly as
 * possible, and unload without a noticeable lag from garbage collection.
 * 
 *** @author Brian Broyles - IFL Game Studio
 **/
public class SplashScreens extends ManagedSplashScreen {

	// ====================================================
	// CONSTANTS
	// ====================================================
	private static final float mEachAnimationDuration = 0.25f;

	private static final float mEachAnimationPauseDuration = 2.2f;

	private static final float mEachScaleToSize = 0.7f * ResourceManager.cameraScaleFactorY;

	private static Sprite mStudioLogoSprite;

	private static Sprite mMaquisLogoSprite;

	public SplashScreens() {
		super();
	}

	public SplashScreens(float pLoadingScreenMinimumSecondsShown) {
		super(pLoadingScreenMinimumSecondsShown);
	}

	private static SequenceEntityModifier mStudioSequenceEntityModifier;

	private static SequenceEntityModifier mMaquisSequenceEntityModifier;

	// ====================================================
	// METHODS
	// ====================================================
	@Override
	public void onLoadScene() {

		ResourceManager.loadSplashResources();

		mStudioSequenceEntityModifier = new SequenceEntityModifier(new ParallelEntityModifier(new ScaleAtModifier(mEachAnimationDuration, 25f,
				mEachScaleToSize, 0.5f, 0.5f), new FadeInModifier(mEachAnimationDuration)), new DelayModifier(mEachAnimationPauseDuration),
				new ParallelEntityModifier(new ScaleAtModifier(mEachAnimationDuration, mEachScaleToSize, 0f, 0.5f, 0.5f), new FadeOutModifier(
						mEachAnimationDuration)));

		mMaquisSequenceEntityModifier = new SequenceEntityModifier(new ParallelEntityModifier(new ScaleAtModifier(mEachAnimationDuration, 25f,
				mEachScaleToSize, 0.5f, 0.5f), new FadeInModifier(mEachAnimationDuration)), new DelayModifier(mEachAnimationPauseDuration),
				new ParallelEntityModifier(new ScaleAtModifier(mEachAnimationDuration, mEachScaleToSize, 0f, 0.5f, 0.5f), new FadeOutModifier(
						mEachAnimationDuration)));

		mStudioLogoSprite = new Sprite(ResourceManager.cameraWidthHalf, 0f, ResourceManager.mStudioLogoTextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager());

		mMaquisLogoSprite = new Sprite(ResourceManager.cameraWidthHalf, 0f, ResourceManager.mMaquisLogoTextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager());

		ResourceManager.getInstance().getEngine().getCamera().setCenter(ResourceManager.cameraWidthHalf, ResourceManager.cameraHeightHalf);

		this.setBackgroundEnabled(true);
		this.setBackground(new Background(0.1f, 0.1f, 0.1f));

		mStudioLogoSprite.setAlpha(0.001f);
		this.attachChild(mStudioLogoSprite);

		mMaquisLogoSprite.setScale(0.01f);
		mMaquisLogoSprite.setAlpha(0.001f);
		this.attachChild(mMaquisLogoSprite);

		mStudioSequenceEntityModifier.addModifierListener(new IModifierListener<IEntity>() {
			@Override
			public void onModifierFinished(final IModifier<IEntity> pModifier, final IEntity pItem) {
				SplashScreens.mMaquisLogoSprite.registerEntityModifier(SplashScreens.mMaquisSequenceEntityModifier);
			}

			@Override
			public void onModifierStarted(final IModifier<IEntity> pModifier, final IEntity pItem) {
			}
		});

		mMaquisSequenceEntityModifier.addModifierListener(new IModifierListener<IEntity>() {
			@Override
			public void onModifierFinished(final IModifier<IEntity> pModifier, final IEntity pItem) {	
				MainMenu.getInstance().hasLoadingScreen = false;
				SceneManager.getInstance().showMainMenu();				
			}

			@Override
			public void onModifierStarted(final IModifier<IEntity> pModifier, final IEntity pItem) {
			}
		});

		this.registerUpdateHandler(new IUpdateHandler() {
			int counter = 0;

			@Override
			public void onUpdate(final float pSecondsElapsed) {
				this.counter++;
				if (this.counter > 2) {
					SplashScreens.mStudioLogoSprite.registerEntityModifier(SplashScreens.mStudioSequenceEntityModifier);
					thisManagedSplashScene.unregisterUpdateHandler(this);
				}
			}

			@Override
			public void reset() {
			}
		});
	}

	@Override
	public void unloadSplashTextures() {
		ResourceManager.unloadSplashResources();
	}

}