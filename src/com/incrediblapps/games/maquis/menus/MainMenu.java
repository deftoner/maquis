package com.incrediblapps.games.maquis.menus;

import org.andengine.entity.Entity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;

import android.util.Log;

import com.incrediblapps.games.R;
import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.SceneManager;
import com.incrediblapps.games.maquis.menus.levelselector.LevelSelectorScene;
import com.incrediblapps.games.maquis.utils.MaquisSmoothCamera;

/**
 * 
 * 
 *** @author Javier S�nchez Gonz�lez
 **/
public class MainMenu extends ManagedMenuScene {

	// ====================================================
	// CONSTANTS
	// ====================================================

	private static final String TAG = "MainMenu";

	private static final MainMenu INSTANCE = new MainMenu();

	// ====================================================
	// INSTANCE GETTER
	// ====================================================
	public static MainMenu getInstance() {
		return INSTANCE;
	}

	// ====================================================
	// VARIABLES
	// ====================================================

	private Entity mHomeMenuScreen;

	private Sprite mMainMenuBGSprite;

	// ====================================================
	// CONSTRUCTOR
	// ====================================================
	public MainMenu() {
		super(0f);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
	}

	@Override
	public void onHideScene() {
	}

	@Override
	public Scene onLoadingScreenLoadAndShown() {
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {
	}

	@Override
	public void onLoadScene() {

		Log.d(TAG, "onLoadScene");

		// Load the game resources
		ResourceManager.loadMenuResources();
		ResourceManager.loadGameResources();

		this.mMainMenuBGSprite = new Sprite(0f, 0f, ResourceManager.mMenuBackgroundTextureRegion, ResourceManager.getActivity().getVertexBufferObjectManager());
		// this.mMainMenuBGSprite.setScale(ResourceManager.cameraHeight /
		// ResourceManager.mMenuBackgroundTextureRegion.getHeight());
		this.mMainMenuBGSprite.setPosition((this.mMainMenuBGSprite.getWidth() * this.mMainMenuBGSprite.getScaleX()) / 2f,
				(this.mMainMenuBGSprite.getHeight() * this.mMainMenuBGSprite.getScaleY()) / 2f);
		this.mMainMenuBGSprite.setZIndex(-999);
		this.attachChild(this.mMainMenuBGSprite);

		this.mHomeMenuScreen = new Entity(0f, ResourceManager.cameraHeight) {
			boolean hasloaded = false;

			@Override
			protected void onManagedUpdate(final float pSecondsElapsed) {
				super.onManagedUpdate(pSecondsElapsed);
				if (!this.hasloaded) {
					this.hasloaded = true;
					this.registerEntityModifier(new MoveModifier(0.25f, 0f, ResourceManager.cameraHeight, 0f, 0f));
				}
			}
		};

		Text playB = new Text(ResourceManager.cameraWidthHalf, ResourceManager.cameraHeightHalf, ResourceManager.mFontPixelBlack,
				ResourceManager.context.getString(R.string.play), ResourceManager.getActivity().getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					if (pTouchAreaLocalX > this.getWidth() || pTouchAreaLocalX < 0f || pTouchAreaLocalY > this.getHeight() || pTouchAreaLocalY < 0f) {
						Log.d(TAG, "onAreaTouched button play offside");
					} else {
						Log.d(TAG, "onAreaTouched button play inside");
						SceneManager.getInstance().showScene(LevelSelectorScene.getInstance());
					}

				}
				return true;
			}
		};
		playB.setScale(1.5f);
		this.mHomeMenuScreen.attachChild(playB);
		this.registerTouchArea(playB);
		this.attachChild(this.mHomeMenuScreen);
	}

	@Override
	public void onShowScene() {
		// this.RefreshLevelStars();
		MaquisSmoothCamera.setupForMenus();
		if (!this.mMainMenuBGSprite.hasParent()) {
			this.attachChild(this.mMainMenuBGSprite);
			this.sortChildren();
		}
	}

	@Override
	public void onUnloadScene() {
	}

	public void RefreshLevelStars() {
		// this.mLevelSelector.refreshAllButtonStars();
	}
}