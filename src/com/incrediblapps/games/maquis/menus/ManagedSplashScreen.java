package com.incrediblapps.games.maquis.menus;

import org.andengine.entity.scene.Scene;

import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.scenes.ManagedScene;
import com.incrediblapps.games.maquis.utils.MaquisSmoothCamera;

/**
 * Based on the ManagedMenuScene class.
 * 
 *** @author Brian Broyles - IFL Game Studio
 **/
public abstract class ManagedSplashScreen extends ManagedScene {

	public ManagedSplashScreen thisManagedSplashScene = this;

	public ManagedSplashScreen() {
		this(0f);
	};

	public ManagedSplashScreen(float pLoadingScreenMinimumSecondsShown) {
		super(pLoadingScreenMinimumSecondsShown);
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
		MaquisSmoothCamera.setupForMenus();
		this.setPosition(0, ResourceManager.cameraHeightHalf);
		this.dispose();
	}

	@Override
	public Scene onLoadingScreenLoadAndShown() {
		return null;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {
	}

	@Override
	public void onShowScene() {
	}

	@Override
	public void onHideScene() {
	}

	@Override
	public void onUnloadScene() {
		ResourceManager.getInstance().getEngine().runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				thisManagedSplashScene.detachChildren();
				for (int i = 0; i < thisManagedSplashScene.getChildCount(); i++)
					thisManagedSplashScene.getChildByIndex(i).dispose();
				thisManagedSplashScene.clearEntityModifiers();
				thisManagedSplashScene.clearTouchAreas();
				thisManagedSplashScene.clearUpdateHandlers();
				thisManagedSplashScene.unloadSplashTextures();
			}
		});
	}

	public abstract void unloadSplashTextures();
}