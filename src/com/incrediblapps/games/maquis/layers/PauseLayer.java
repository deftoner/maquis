package com.incrediblapps.games.maquis.layers;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;

import android.util.Log;

import com.incrediblapps.games.R;
import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.SceneManager;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

/**
 * 
 * 
 *** @author Javier S�nchez Gonz�lez
 **/
public class PauseLayer extends ManagedLayer {

	private static final float SLIDE_PIXELS_PER_SECONDS = 2000f;

	private static final int LAYER_OFFSET_HALF = 25;

	private static final int LAYER_OFFSET = 50;

	// ====================================================
	// CONSTANTS
	// ====================================================
	private static final PauseLayer INSTANCE = new PauseLayer();

	private static final String TAG = "PauseLayer";

	// ====================================================
	// INSTANCE GETTERS
	// ====================================================
	public static PauseLayer getInstance() {
		return INSTANCE;
	}

	public static PauseLayer getInstance(final SceneLevel pSceneLevel) {
		INSTANCE.setCurrentLevel(pSceneLevel);
		return INSTANCE;
	}

	// ====================================================
	// VARIABLES
	// ====================================================
	private SceneLevel mSceneLevel;

	private Rectangle fadableBGRect;

	private Text mPauseText;

	private Text mRetryText;

	private Text mContinueText;

	private Text mMenuText;

	private Entity layer;

	// ====================================================
	// UPDATE HANDLERS
	// ====================================================

	// Animates the layer to slide in from the top.
	IUpdateHandler mSlideInUpdateHandler = new IUpdateHandler() {
		@Override
		public void onUpdate(final float pSecondsElapsed) {
			if (PauseLayer.this.layer.getY() > 0) {
				PauseLayer.this.layer.setY(Math.max(PauseLayer.this.layer.getY() - (SLIDE_PIXELS_PER_SECONDS * (pSecondsElapsed)), 0));
			} else {
				ResourceManager.engine.unregisterUpdateHandler(this);
			}
		}

		@Override
		public void reset() {
		}
	};

	// Animates the layer to slide out through the top
	IUpdateHandler mSlideOutUpdateHandler = new IUpdateHandler() {
		@Override
		public void onUpdate(final float pSecondsElapsed) {
			if (PauseLayer.this.layer.getY() < ((ResourceManager.cameraHeight))) {
				PauseLayer.this.layer.setY(Math.min(PauseLayer.this.layer.getY() + (pSecondsElapsed * SLIDE_PIXELS_PER_SECONDS), (ResourceManager.cameraHeight)));
			} else {
				ResourceManager.engine.unregisterUpdateHandler(this);
				SceneManager.getInstance().hideLayer();
				SceneManager.getInstance().showLevelSelector();
			}
		}

		@Override
		public void reset() {
		}
	};

	// Animates the layer to slide out through the top
	IUpdateHandler mContinueUpdateHandler = new IUpdateHandler() {
		@Override
		public void onUpdate(final float pSecondsElapsed) {
			if (PauseLayer.this.layer.getY() < ((ResourceManager.cameraHeight))) {
				PauseLayer.this.layer.setY(Math.min(PauseLayer.this.layer.getY() + (pSecondsElapsed * SLIDE_PIXELS_PER_SECONDS), (ResourceManager.cameraHeight)));
			} else {
				ResourceManager.engine.unregisterUpdateHandler(this);
				SceneManager.getInstance().hideLayer();
			}
		}

		@Override
		public void reset() {
		}
	};

	// ====================================================
	// METHODS
	// ====================================================
	@Override
	public void onHideLayer() {
		// Register the slide-out animation with the engine.
		ResourceManager.engine.registerUpdateHandler(this.mContinueUpdateHandler);
	}

	@Override
	public void onLoadLayer() {

		Log.d(TAG, "onLoadLayer()");

		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);

		float layerWidth = ResourceManager.cameraWidth - LAYER_OFFSET;
		float layerHeight = ResourceManager.cameraHeight - LAYER_OFFSET;

		fadableBGRect = new Rectangle(0f, 0f, layerWidth, layerHeight, ResourceManager.getActivity().getVertexBufferObjectManager());
		fadableBGRect.setColor(0f, 0f, 0f, 0.5f);
		this.attachChild(fadableBGRect);

		layer = new Entity(0f, 0f, layerWidth, layerHeight);
		layer.setAnchorCenterY(0);
		this.attachChild(layer);

		mPauseText = new Text(0f, 0f, ResourceManager.mFontPixelWhite, ResourceManager.context.getString(R.string.pause), ResourceManager.getActivity()
				.getVertexBufferObjectManager());
		mPauseText.setScale(1.5f);

		mMenuText = new Text(0f, 0f, ResourceManager.mFontPixelWhite, ResourceManager.context.getString(R.string.menu_button), ResourceManager.getActivity()
				.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					if (pTouchAreaLocalX > this.getWidth() || pTouchAreaLocalX < 0f || pTouchAreaLocalY > this.getHeight() || pTouchAreaLocalY < 0f) {
					} else {
						ResourceManager.engine.registerUpdateHandler(PauseLayer.INSTANCE.mSlideOutUpdateHandler);
					}
				}
				return true;
			}
		};
		mMenuText.setPosition(0, -ResourceManager.cameraHeightHalf + mMenuText.getHeight() + LAYER_OFFSET_HALF);
		this.registerTouchArea(mMenuText);

		mRetryText = new Text(0, 0, ResourceManager.mFontPixelWhite, ResourceManager.context.getString(R.string.retry_button), ResourceManager.getActivity()
				.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					if (pTouchAreaLocalX > this.getWidth() || pTouchAreaLocalX < 0f || pTouchAreaLocalY > this.getHeight() || pTouchAreaLocalY < 0f) {
					} else {
						SceneManager.getInstance().hideLayer();
						mSceneLevel.restartLevel();
					}
				}
				return true;
			}
		};
		mRetryText.setPosition(-mRetryText.getWidth() - mMenuText.getWidth() * 0.5f, -ResourceManager.cameraHeightHalf + mRetryText.getHeight()
				+ LAYER_OFFSET_HALF);
		this.registerTouchArea(mRetryText);

		mContinueText = new Text(0f, 0f, ResourceManager.mFontPixelWhite, ResourceManager.context.getString(R.string.continue_button),
				ResourceManager.getActivity().getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					if (pTouchAreaLocalX > this.getWidth() || pTouchAreaLocalX < 0f || pTouchAreaLocalY > this.getHeight() || pTouchAreaLocalY < 0f) {
					} else {
						ResourceManager.engine.registerUpdateHandler(PauseLayer.INSTANCE.mContinueUpdateHandler);
					}
				}
				return true;
			}
		};
		mContinueText.setPosition(mContinueText.getWidth() + mMenuText.getWidth() * 0.5f, -ResourceManager.cameraHeightHalf + mContinueText.getHeight()
				+ LAYER_OFFSET_HALF);
		this.registerTouchArea(mContinueText);

		layer.attachChild(mPauseText);
		layer.attachChild(mRetryText);
		layer.attachChild(mContinueText);
		layer.attachChild(mMenuText);

		this.setPosition(ResourceManager.cameraWidthHalf, ResourceManager.cameraHeightHalf);
	}

	@Override
	public void onShowLayer() {
		Log.d(TAG, "onShowLayer()");
		layer.setPosition(ResourceManager.cameraWidthHalf, ResourceManager.cameraHeightHalf);
		fadableBGRect.setVisible(true);
		ResourceManager.engine.registerUpdateHandler(this.mSlideInUpdateHandler);
	}

	@Override
	public void onUnloadLayer() {
		Log.d(TAG, "onUnloadLayer()");
	}

	public void setCurrentLevel(final SceneLevel pSceneLevel) {
		this.mSceneLevel = pSceneLevel;
	}
}