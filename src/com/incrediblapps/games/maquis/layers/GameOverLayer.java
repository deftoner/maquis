package com.incrediblapps.games.maquis.layers;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;

import android.util.Log;

import com.incrediblapps.games.R;
import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.SceneManager;
import com.incrediblapps.games.maquis.scenes.SceneLevel;

/**
 * 
 * 
 *** @author Javier S�nchez Gonz�lez
 **/
public class GameOverLayer extends ManagedLayer {

	private static final int LAYER_OFFSET_HALF = 25;

	private static final int LAYER_OFFSET = 50;

	private static final float ALPHA_RECTANGLE_INCREMENT = 0.01f;

	private static final float ALPHA_BUTTONS_INCREMENT = 0.02f;

	// ====================================================
	// CONSTANTS
	// ====================================================
	private static final GameOverLayer INSTANCE = new GameOverLayer();

	private static final String TAG = "GameOverLayer";

	// ====================================================
	// INSTANCE GETTERS
	// ====================================================
	public static GameOverLayer getInstance() {
		return INSTANCE;
	}

	public static GameOverLayer getInstance(final SceneLevel pSceneLevel) {
		INSTANCE.setCurrentLevel(pSceneLevel);
		return INSTANCE;
	}

	// ====================================================
	// VARIABLES
	// ====================================================
	private SceneLevel mSceneLevel;

	private Rectangle fadableBGRect;

	public Text mGameoverText;

	public Text mRetryText;

	public Text mBackText;

	public Entity layer;

	private float alphaRectangle = 0;

	private float alphaButtons = 0;

	// ====================================================
	// UPDATE HANDLERS
	// ====================================================

	// // Animates the layer to fade in
	IUpdateHandler mFadeInUpdateHandler = new IUpdateHandler() {
		@Override
		public void onUpdate(final float pSecondsElapsed) {
			// mGameoverText.setVisible(true);
			if (GameOverLayer.this.incrementAlpha()) {
				ResourceManager.engine.unregisterUpdateHandler(this);
			}
		}

		@Override
		public void reset() {
		}
	};

	// Animates the layer to fade out and show levelSelector
	IUpdateHandler mFadeOutUpdateHandler = new IUpdateHandler() {
		@Override
		public void onUpdate(final float pSecondsElapsed) {
			if (GameOverLayer.this.decrementAlpha()) {
				ResourceManager.engine.unregisterUpdateHandler(this);
				SceneManager.getInstance().hideLayer();
				SceneManager.getInstance().showLevelSelector();
			}
		}

		@Override
		public void reset() {
		}
	};

	// Animates the layer to fade out and restart the level
	IUpdateHandler mRetrytUpdateHandler = new IUpdateHandler() {
		@Override
		public void onUpdate(final float pSecondsElapsed) {
			if (GameOverLayer.this.decrementAlpha()) {
				ResourceManager.engine.unregisterUpdateHandler(this);
				SceneManager.getInstance().hideLayer();
				mSceneLevel.restartLevel();
			}
		}

		@Override
		public void reset() {
		}
	};

	// ====================================================
	// METHODS
	// ====================================================
	@Override
	public void onHideLayer() {
		// Register the slide-out animation with the engine.
		ResourceManager.engine.registerUpdateHandler(this.mFadeOutUpdateHandler);
	}

	@Override
	public void onLoadLayer() {

		Log.d(TAG, "onLoadLayer()");
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);

		float layerWidth = ResourceManager.cameraWidth - LAYER_OFFSET;
		float layerHeight = ResourceManager.cameraHeight - LAYER_OFFSET;

		layer = new Entity(ResourceManager.cameraWidthHalf - LAYER_OFFSET_HALF, ResourceManager.cameraHeightHalf - LAYER_OFFSET_HALF, layerWidth, layerHeight);
		this.attachChild(layer);

		fadableBGRect = new Rectangle(0, 0, layerWidth, layerHeight, ResourceManager.getActivity().getVertexBufferObjectManager());
		fadableBGRect.setColor(0f, 0f, 0f);
		layer.attachChild(fadableBGRect);

		mGameoverText = new Text(0f, 0f, ResourceManager.mFontPixelWhite, ResourceManager.context.getString(R.string.gameover), ResourceManager.getActivity()
				.getVertexBufferObjectManager());
		mGameoverText.setScale(1.5f);

		mRetryText = new Text(0, 0, ResourceManager.mFontPixelWhite, ResourceManager.context.getString(R.string.retry_button), ResourceManager.getActivity()
				.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					if (pTouchAreaLocalX > this.getWidth() || pTouchAreaLocalX < 0f || pTouchAreaLocalY > this.getHeight() || pTouchAreaLocalY < 0f) {
					} else {
						ResourceManager.engine.registerUpdateHandler(mRetrytUpdateHandler);
					}
				}
				return true;
			}
		};
		mRetryText.setPosition(-mRetryText.getWidth(), -ResourceManager.cameraHeightHalf + mRetryText.getHeight() + LAYER_OFFSET_HALF);
		this.registerTouchArea(mRetryText);

		mBackText = new Text(0f, 0f, ResourceManager.mFontPixelWhite, ResourceManager.context.getString(R.string.back_button), ResourceManager.getActivity()
				.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					if (pTouchAreaLocalX > this.getWidth() || pTouchAreaLocalX < 0f || pTouchAreaLocalY > this.getHeight() || pTouchAreaLocalY < 0f) {
					} else {
						ResourceManager.engine.registerUpdateHandler(mFadeOutUpdateHandler);
					}
				}
				return true;
			}
		};
		mBackText.setPosition(mBackText.getWidth(), -ResourceManager.cameraHeightHalf + mBackText.getHeight() + LAYER_OFFSET_HALF);
		this.registerTouchArea(mBackText);

		layer.attachChild(mGameoverText);
		layer.attachChild(mRetryText);
		layer.attachChild(mBackText);

		fadableBGRect.setAlpha(0);
		mGameoverText.setAlpha(0);
		mBackText.setAlpha(0);
		mRetryText.setAlpha(0);

		this.setPosition(ResourceManager.cameraWidthHalf, ResourceManager.cameraHeightHalf);
	}

	@Override
	public void onShowLayer() {
		Log.d(TAG, "onShowLayer()");
		ResourceManager.engine.registerUpdateHandler(this.mFadeInUpdateHandler);
	}

	@Override
	public void onUnloadLayer() {
		Log.d(TAG, "onUnloadLayer()");
	}

	public void setCurrentLevel(final SceneLevel pSceneLevel) {
		this.mSceneLevel = pSceneLevel;
	}

	private boolean incrementAlpha() {
		alphaButtons += ALPHA_BUTTONS_INCREMENT;
		alphaRectangle += ALPHA_RECTANGLE_INCREMENT;

		if (alphaButtons < 1) {
			fadableBGRect.setAlpha(alphaRectangle);
			mGameoverText.setAlpha(alphaButtons);
			mRetryText.setAlpha(alphaButtons);
			mBackText.setAlpha(alphaButtons);
			return false;
		} else {
			return true;
		}
	}

	private boolean decrementAlpha() {
		alphaButtons -= ALPHA_BUTTONS_INCREMENT;
		alphaRectangle -= ALPHA_RECTANGLE_INCREMENT;

		if (alphaButtons > 0) {
			fadableBGRect.setAlpha(alphaRectangle);
			mGameoverText.setAlpha(alphaButtons);
			mRetryText.setAlpha(alphaButtons);
			mBackText.setAlpha(alphaButtons);
			return false;
		} else {
			return true;
		}
	}

}