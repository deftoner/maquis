package com.incrediblapps.games.maquis.scenes;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.PinchZoomDetector;
import org.andengine.util.modifier.IModifier;

import android.util.Log;

import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.elements.ActionableElement;
import com.incrediblapps.games.maquis.elements.Closet;
import com.incrediblapps.games.maquis.elements.Door;
import com.incrediblapps.games.maquis.elements.Door.DoorState;
import com.incrediblapps.games.maquis.elements.FloorDoor;
import com.incrediblapps.games.maquis.elements.HideDoor;
import com.incrediblapps.games.maquis.elements.ObliqueDoor;
import com.incrediblapps.games.maquis.elements.UselessAnimatedElement;
import com.incrediblapps.games.maquis.elements.UselessElement;
import com.incrediblapps.games.maquis.elements.Wall;
import com.incrediblapps.games.maquis.elements.Window;
import com.incrediblapps.games.maquis.players.actions.DoorAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.actions.SmokeAction;
import com.incrediblapps.games.maquis.players.actions.WaitAction;
import com.incrediblapps.games.maquis.players.enemies.Enemy;
import com.incrediblapps.games.maquis.players.enemies.Maqui1;
import com.incrediblapps.games.maquis.players.main.MainCharacter;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;
import com.incrediblapps.games.maquis.utils.constants.Constants;
import com.incrediblapps.games.maquis.utils.constants.ConstantsLevel1;

public class SceneLevel1 extends SceneLevel {

	private static final String TAG = "SceneLevel1";

	public static final float ZOOM_FACTOR_MAX = 1.5f;

	public static final float ZOOM_FACTOR_MIN = 0.75f;

	private static final int ENEMY_NUMBER = 1;

	private AnimatedSprite papersHUD;

	private Sprite skipButtonHUD;

	public ObliqueDoor mDoor1010;

	private MoveXModifier moveXModifier;

	private ActionableElement mTablePapers;

	public SceneLevel1() {
		super(1f);
		mCamera.setBounds(0, 0, ConstantsLevel1.WIDTH, ConstantsLevel1.HEIGHT);
		mEnemies = new Enemy[ENEMY_NUMBER];
	}

	@Override
	public void onLoadLevel() {
		Log.d(TAG, "onLoadScene level");

		ResourceManager.loadLevel1Resources();

		createBackground();
		createHUD();
		createMainPlayer();

		// Right House
		createDoor1100();
		createDoor1101();
		createDoor1102();
		createDoor1110();
		createFloorDoor2110_2100();
		createHideDoor3100();
		createCloset110();
		createColumn();
		createShelves1100();
		createPicture1100();
		createTablePeople();
		createWall11001();

		// Left House
		createDoor1000();
		createDoor1001();
		createDoor1002();
		createDoor1010();
		createFloorDoor2010_2000();
		createCloset000();
		createShelves2000();
		createLibrary();
		createPicture2000();
		createFlagCat();
		createFlagRep();
		createClock();
		createCar();
		createTable();
		createWall10000();
		createWindow();

		// Enemies
		createEnemy1();

		mBackgroundSprite.sortChildren();
	}

	@Override
	public void restartLevel() {
		Log.d(TAG, "restarting level");

		mPlayer.restart();
		mGameOver = false;

		mEnemies[0].restart();
		mEnemies[0].setRight();

		for (Door d : mDoorList) {
			d.setCurrentTileIndex(d.closedTile);
			d.setIgnoreUpdate(false);
		}

		mTablePapers.restart();

		onShowScene();
	}

	private void createBackground() {
		int halfTextureWidth = (int) (ResourceManager.mBackgroundTextureRegionLvl1_1.getWidth() * 0.5f);
		int halfTextureWidth2 = (int) (ResourceManager.mBackgroundTextureRegionLvl1_2.getWidth() * 0.5f);
		int halfTextureHeight = (int) (ResourceManager.mBackgroundTextureRegionLvl1_1.getHeight() * 0.5f);

		Sprite mBackgroundSprite_1 = new Sprite(halfTextureWidth, halfTextureHeight, ResourceManager.mBackgroundTextureRegionLvl1_1,
				ResourceManager.engine.getVertexBufferObjectManager());

		Sprite mBackgroundSprite_2 = new Sprite(mBackgroundSprite_1.getX() + halfTextureWidth + halfTextureWidth2, halfTextureHeight,
				ResourceManager.mBackgroundTextureRegionLvl1_2, ResourceManager.engine.getVertexBufferObjectManager());

		mBackgroundSprite.attachChild(mBackgroundSprite_1);
		mBackgroundSprite.attachChild(mBackgroundSprite_2);
	}

	@Override
	public void createHUD() {
		super.createHUD();

		papersHUD = new AnimatedSprite(0, 0, ResourceManager.mHUDPapersTextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		papersHUD.setAnchorCenter(0, 0);
		papersHUD.setX(-1);
		papersHUD.setY(-1);
		papersHUD.setScale(0.75f);

		mGameLayerHUD.attachChild(papersHUD);

		skipButtonHUD = new Sprite(0, 0, ResourceManager.mSkipButtonRegion, ResourceManager.engine.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					if (pTouchAreaLocalX > this.getWidth() || pTouchAreaLocalX < 0f || pTouchAreaLocalY > this.getHeight() || pTouchAreaLocalY < 0f) {
					} else {
						mCamera.setZoomFactor(1f);
						mCamera.setCenter(ConstantsLevel1.PLAYER_START_POSITION_X, ConstantsLevel1.PLAYER_START_POSITION_Y);
						mCamera.setChaseEntity(null);
						mChaseEntity.clearEntityModifiers();
						stopIntroduction();
					}
				}
				return true;
			}
		};

		skipButtonHUD.setAnchorCenter(0, 0);
		skipButtonHUD.setX(ResourceManager.cameraWidth - skipButtonHUD.getWidth());
		skipButtonHUD.setY(ResourceManager.cameraHeight - skipButtonHUD.getHeight());

		mIntroductionLayerHUD.attachChild(skipButtonHUD);
		// mCamera.getHUD().registerTouchArea(skipButtonHUD);
	}

	public void createMainPlayer() {
		mPlayer = new MainCharacter(this, ConstantsLevel1.PLAYER_START_POSITION_X, ConstantsLevel1.PLAYER_START_POSITION_Y, 1, 6);
		mPlayer.setLeft();
		mBackgroundSprite.registerUpdateHandler(mPlayer);
	}

	private void createEnemy1() {
		Enemy enemy = new Maqui1(this, ConstantsLevel1.ENEMY1_START_POSITION_X, ConstantsLevel1.ENEMY1_START_POSITION_Y, 2, 3);
		mBackgroundSprite.registerUpdateHandler(enemy);
		mEnemies[0] = enemy;

		// Movements
		enemy.setRight();
		enemy.mMovementsQueue.add(new SmokeAction(false, 4000));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel1.ENEMY1_POINT1_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(mDoor1010, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel1.ENEMY1_POINT2_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(mDoor1010, DoorState.CLOSED_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel1.ENEMY1_POINT3_POSITION_X, 0, PlayerState.RUNNING_LEFT));
		enemy.mMovementsQueue.add(new WaitAction(3000));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel1.ENEMY1_POINT2_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(mDoor1010, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel1.ENEMY1_POINT1_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(mDoor1010, DoorState.CLOSED_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel1.ENEMY1_START_POSITION_X, 0, PlayerState.RUNNING_RIGHT));
	}

	private void createWindow() {
		Window mWindow = new Window(this, ConstantsLevel1.WINDOW_X, ConstantsLevel1.WINDOW_Y, ConstantsLevel1.WINDOW_WIDTH, ConstantsLevel1.WINDOW_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager(), 2, 1);
		this.registerTouchArea(mWindow);
		mBackgroundSprite.attachChild(mWindow);
	}

	private void createWall10000() {
		Wall mWall10000 = new Wall(mPlayer, ConstantsLevel1.WALL10000_X, ConstantsLevel1.WALL10000_Y, Constants.Shared.WALL_WIDTH,
				Constants.Shared.WALL_HEIGHT, ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(mWall10000);
	}

	private void createWall11001() {
		Wall mWall11001 = new Wall(mPlayer, ConstantsLevel1.WALL11001_X, ConstantsLevel1.WALL11001_Y, Constants.Shared.WALL_WIDTH,
				Constants.Shared.WALL_HEIGHT, ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(mWall11001);
	}

	private void createTable() {
		mTablePapers = new ActionableElement(this, 0, 0, ResourceManager.mTableTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), 2, 2,
				papersHUD);

		mTablePapers.setAnchorCenter(0, 0);
		mTablePapers.setX(ConstantsLevel1.TABLE_POSITION_X);
		mTablePapers.setY(ConstantsLevel1.TABLE_POSITION_Y);
		mTablePapers.setCurrentTileIndex(mTablePapers.mNoActuatedTile);

		this.registerTouchArea(mTablePapers);
		mBackgroundSprite.attachChild(mTablePapers);
	}

	private void createFlagRep() {
		UselessElement flag = new UselessElement(this, 0, 0, ResourceManager.mFlagRepTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), 1, 2);

		flag.setAnchorCenter(0, 0);
		flag.setX(ConstantsLevel1.FLAG_REP_POSITION_X);
		flag.setY(ConstantsLevel1.FLAG_REP_POSITION_Y);

		this.registerTouchArea(flag);
		mBackgroundSprite.attachChild(flag);
	}

	private void createFlagCat() {
		UselessElement flag = new UselessElement(this, 0, 0, ResourceManager.mFlagCatTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), 2, 2);

		flag.setAnchorCenter(0, 0);
		flag.setX(ConstantsLevel1.FLAG_CAT_POSITION_X);
		flag.setY(ConstantsLevel1.FLAG_CAT_POSITION_Y);

		this.registerTouchArea(flag);
		mBackgroundSprite.attachChild(flag);
	}

	private void createPicture2000() {
		UselessElement picture = new UselessElement(this, 0, 0, ResourceManager.mPicture12000TextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager(), 2, 2);

		picture.setAnchorCenter(0, 0);
		picture.setX(ConstantsLevel1.PICTURE2000_POSITION_X);
		picture.setY(ConstantsLevel1.PICTURE2000_POSITION_Y);

		this.registerTouchArea(picture);
		mBackgroundSprite.attachChild(picture);
	}

	private void createLibrary() {
		UselessElement picture = new UselessElement(this, 0, 0, ResourceManager.mLibraryTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(),
				1, 2);

		picture.setAnchorCenter(0, 0);
		picture.setX(ConstantsLevel1.LIBRARY_POSITION_X);
		picture.setY(ConstantsLevel1.LIBRARY_POSITION_Y);

		this.registerTouchArea(picture);
		mBackgroundSprite.attachChild(picture);
	}

	private void createClock() {
		UselessAnimatedElement clock = new UselessAnimatedElement(this, 0, 0, ResourceManager.mClockTextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager(), 2, 2);

		clock.setAnchorCenter(0, 0);
		clock.setX(ConstantsLevel1.CLOCK_POSITION_X);
		clock.setY(ConstantsLevel1.CLOCK_POSITION_Y);

		clock.animate(ConstantsLevel1.CLOCK_FRAMES, true);

		this.registerTouchArea(clock);
		mBackgroundSprite.attachChild(clock);
	}

	// private void createMetalicCloset() {
	// UselessElement closet = new UselessElement(this, 0, 0,
	// ResourceManager.mMetalicClosetTextureRegion,
	// ResourceManager.engine.getVertexBufferObjectManager(), 2, 2);
	//
	// closet.setAnchorCenter(0, 0);
	// closet.setX(ConstantsLevel1.METALIC_CLOSET_POSITION_X);
	// closet.setY(ConstantsLevel1.METALIC_CLOSET_POSITION_Y);
	//
	// this.registerTouchArea(closet);
	// mBackgroundSprite.attachChild(closet);
	// }

	private void createCar() {
		UselessElement car = new UselessElement(this, 0, 0, ResourceManager.mCarTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), 1, 2);

		car.setAnchorCenter(0, 0);
		car.setX(ConstantsLevel1.CAR_POSITION_X);
		car.setY(ConstantsLevel1.CAR_POSITION_Y);

		this.registerTouchArea(car);
		mBackgroundSprite.attachChild(car);
	}

	private void createShelves2000() {
		UselessElement shelves = new UselessElement(this, 0, 0, ResourceManager.mShelves2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(),
				1, 5);

		shelves.setAnchorCenter(0, 0);
		shelves.setX(ConstantsLevel1.SHELVES2000_POSITION_X);
		shelves.setY(ConstantsLevel1.SHELVES2000_POSITION_Y);

		this.registerTouchArea(shelves);
		mBackgroundSprite.attachChild(shelves);
	}

	private void createCloset000() {
		Closet closet = new Closet(this, 0, 0, ResourceManager.mClosetTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1, 2);

		closet.setAnchorCenter(0, 0);
		closet.setX(ConstantsLevel1.CLOSET000_POSITION_X);
		closet.setY(ConstantsLevel1.CLOSET000_POSITION_Y);

		mDoorList.add(closet);

		this.registerTouchArea(closet);
		mBackgroundSprite.attachChild(closet);
	}

	private void createFloorDoor2010_2000() {
		FloorDoor door1 = new FloorDoor(this, 0, 0, ResourceManager.mDoor2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 2, 2);

		door1.setAnchorCenter(0, 0);
		door1.setX(ConstantsLevel1.FLOORDOOR2010_POSITION_X);
		door1.setY(ConstantsLevel1.FLOORDOOR2010_POSITION_Y);
		door1.setCurrentTileIndex(1);

		mDoorList.add(door1);

		this.registerTouchArea(door1);
		mBackgroundSprite.attachChild(door1);

		FloorDoor door2 = new FloorDoor(this, 0, 0, ResourceManager.mDoor2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1, 2);

		door2.setAnchorCenter(0, 0);
		door2.setX(ConstantsLevel1.FLOORDOOR2000_POSITION_X);
		door2.setY(ConstantsLevel1.FLOORDOOR2000_POSITION_Y);
		door2.setCurrentTileIndex(1);

		mDoorList.add(door2);

		this.registerTouchArea(door2);
		mBackgroundSprite.attachChild(door2);

		door1.mConnectedDoor = door2;
		door2.mConnectedDoor = door1;
	}

	private void createDoor1000() {
		Sprite door = new Sprite(0, 0, ResourceManager.mDoor11000TextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel1.DOOR1000_POSITION_X);
		door.setY(ConstantsLevel1.DOOR1000_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mBackgroundSprite.attachChild(door);
	}

	private void createPicture1100() {
		UselessElement picture = new UselessElement(this, 0, 0, ResourceManager.mPicture11100TextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager(), 2, 4);

		picture.setAnchorCenter(0, 0);
		picture.setX(ConstantsLevel1.PICTURE1100_POSITION_X);
		picture.setY(ConstantsLevel1.PICTURE1100_POSITION_Y);

		this.registerTouchArea(picture);
		mBackgroundSprite.attachChild(picture);
	}

	private void createShelves1100() {
		UselessElement shelves = new UselessElement(this, 0, 0, ResourceManager.mShelves1TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(),
				1, 5);

		shelves.setAnchorCenter(0, 0);
		shelves.setX(ConstantsLevel1.SHELVES1100_POSITION_X);
		shelves.setY(ConstantsLevel1.SHELVES1100_POSITION_Y);

		this.registerTouchArea(shelves);
		mBackgroundSprite.attachChild(shelves);
	}

	private void createTablePeople() {
		AnimatedSprite table = new AnimatedSprite(0, 0, ResourceManager.mTablePeopleTextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		table.setAnchorCenter(0, 0);
		table.setX(ConstantsLevel1.TABLEPEOPLE_POSITION_X);
		table.setY(ConstantsLevel1.TABLEPEOPLE_POSITION_Y);
		table.animate(ConstantsLevel1.TABLEPEOPLE_FRAMES_ARRAY, 0, 25, true);

		mBackgroundSprite.attachChild(table);
	}

	private void createColumn() {
		Sprite column = new Sprite(0, 0, ResourceManager.mColumnTextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		column.setAnchorCenter(0, 0);
		column.setX(ConstantsLevel1.COLUMN_POSITION_X);
		column.setY(ConstantsLevel1.COLUMN_POSITION_Y);
		column.setZIndex(LAYER_DOORS);
		mBackgroundSprite.attachChild(column);
	}

	private void createCloset110() {
		Closet closet = new Closet(this, 0, 0, ResourceManager.mClosetTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 2, 4);

		closet.setAnchorCenter(0, 0);
		closet.setX(ConstantsLevel1.CLOSET110_POSITION_X);
		closet.setY(ConstantsLevel1.CLOSET110_POSITION_Y);

		mDoorList.add(closet);

		this.registerTouchArea(closet);
		mBackgroundSprite.attachChild(closet);
	}

	private void createDoor1001() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor11001TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel1.DOOR1001_POSITION_X);
		door.setY(ConstantsLevel1.DOOR1001_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createDoor1002() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor11002TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel1.DOOR1002_POSITION_X);
		door.setY(ConstantsLevel1.DOOR1002_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createDoor1010() {
		mDoor1010 = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor11010TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 2);

		mDoor1010.setAnchorCenter(0, 0);
		mDoor1010.setX(ConstantsLevel1.DOOR1010_POSITION_X);
		mDoor1010.setY(ConstantsLevel1.DOOR1010_POSITION_Y);
		mDoor1010.setZIndex(LAYER_DOORS);

		mDoorList.add(mDoor1010);

		this.registerTouchArea(mDoor1010);
		mBackgroundSprite.attachChild(mDoor1010);
	}

	private void createDoor1100() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor11100TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel1.DOOR1100_POSITION_X);
		door.setY(ConstantsLevel1.DOOR1100_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createDoor1101() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor11101TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel1.DOOR1101_POSITION_X);
		door.setY(ConstantsLevel1.DOOR1101_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createDoor1102() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor11102TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel1.DOOR1102_POSITION_X);
		door.setY(ConstantsLevel1.DOOR1102_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createDoor1110() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor11110TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 2);

		door.openable = false;
		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel1.DOOR1110_POSITION_X);
		door.setY(ConstantsLevel1.DOOR1110_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createFloorDoor2110_2100() {
		FloorDoor door1 = new FloorDoor(this, 0, 0, ResourceManager.mDoor2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1, 5);

		door1.setAnchorCenter(0, 0);
		door1.setX(ConstantsLevel1.FLOORDOOR2110_POSITION_X);
		door1.setY(ConstantsLevel1.FLOORDOOR2110_POSITION_Y);
		door1.setCurrentTileIndex(1);

		mDoorList.add(door1);

		this.registerTouchArea(door1);
		mBackgroundSprite.attachChild(door1);

		FloorDoor door2 = new FloorDoor(this, 0, 0, ResourceManager.mDoor2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 2, 4);

		door2.setAnchorCenter(0, 0);
		door2.setX(ConstantsLevel1.FLOORDOOR2100_POSITION_X);
		door2.setY(ConstantsLevel1.FLOORDOOR2100_POSITION_Y);
		door2.setCurrentTileIndex(1);

		mDoorList.add(door2);

		this.registerTouchArea(door2);
		mBackgroundSprite.attachChild(door2);

		door1.mConnectedDoor = door2;
		door2.mConnectedDoor = door1;
	}

	private void createHideDoor3100() {
		HideDoor door = new HideDoor(this, 0, 0, ResourceManager.mDoor3TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1, 5);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel1.HIDEDOOR3100_POSITION_X);
		door.setY(ConstantsLevel1.HIDEDOOR3100_POSITION_Y);
		door.setCurrentTileIndex(1);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	@Override
	public void onShowScene() {
		createIntroduction();
	}

	private void createIntroduction() {
		float startPointX = ConstantsLevel1.WIDTH - mCamera.getWidth() * 0.5f;
		float startPointY = (mCamera.getHeight() * 0.5f) / ZOOM_FACTOR_MIN;
		float endPointX = (mCamera.getWidth() * 0.5f) / ZOOM_FACTOR_MIN;

		// Start the Level with the camera in some point, depending on the level
		mCamera.setCenterDirect(startPointX, startPointY);
		mCamera.setZoomFactorDirect(ZOOM_FACTOR_MIN);

		mChaseEntity = new Rectangle(startPointX, startPointY, 50, 50, ResourceManager.engine.getVertexBufferObjectManager());
		mChaseEntity.setVisible(false);
		mBackgroundSprite.attachChild(mChaseEntity);
		mCamera.setChaseEntity(mChaseEntity);

		mIntroductionLayerHUD.setVisible(true);
		mCamera.getHUD().registerTouchArea(skipButtonHUD);

		IEntityModifierListener moveListener1 = new IEntityModifierListener() {

			IEntityModifierListener moveListener2 = new IEntityModifierListener() {

				IEntityModifierListener moveListener3 = new IEntityModifierListener() {

					@Override
					public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
					}

					@Override
					public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
						stopIntroduction();
					}
				};

				@Override
				public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
				}

				@Override
				public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
					mCamera.setZoomFactor(ZOOM_FACTOR_MIN);
					TimerHandler time = new TimerHandler(2f, new ITimerCallback() {
						@Override
						public void onTimePassed(TimerHandler pTimerHandler) {
							MoveXModifier moveXModifier = new MoveXModifier(5f, mChaseEntity.getX(), ConstantsLevel1.PLAYER_START_POSITION_X, moveListener3);
							mChaseEntity.registerEntityModifier(moveXModifier);
						}
					});
					ResourceManager.engine.getScene().registerUpdateHandler(time);
				}
			};

			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
				mPlayer.mIsLocked = true;
				Log.d(TAG, "modifier started");
			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
				mCamera.setZoomFactor(1.2f);
				TimerHandler time = new TimerHandler(3f, new ITimerCallback() {
					@Override
					public void onTimePassed(TimerHandler pTimerHandler) {
						MoveModifier moveModifier = new MoveModifier(5f, mChaseEntity.getX(), mChaseEntity.getY(), 100, 100, moveListener2);
						mChaseEntity.registerEntityModifier(moveModifier);
					}
				});
				ResourceManager.engine.getScene().registerUpdateHandler(time);
			}
		};

		moveXModifier = new MoveXModifier(5f, startPointX, endPointX, moveListener1);
		mChaseEntity.registerEntityModifier(moveXModifier);
	}

	private void stopIntroduction() {
		mPlayer.mIsLocked = false;
		mCamera.setChaseEntity(null);
		mCamera.getHUD().unregisterTouchArea(skipButtonHUD);
		mIntroductionLayerHUD.setVisible(false);
		mGameLayerHUD.setVisible(true);
	}

	@Override
	public void onHideScene() {
		Log.d(TAG, "onHideScene level");
		// UnLoad resources for level1
		this.mCamera.setChaseEntity(null);
		final HUD oldHUD = this.mCamera.getHUD();
		if (oldHUD != null) {
			oldHUD.detachSelf();
			oldHUD.dispose();
			this.mCamera.setHUD(null);
		}
	}

	@Override
	public void onPinchZoomStarted(PinchZoomDetector pPinchZoomDetector, TouchEvent pSceneTouchEvent) {
		// Obtain the initial zoom factor on pinch detection
		mInitialTouchZoomFactor = mCamera.getZoomFactor();
	}

	@Override
	public void onPinchZoom(PinchZoomDetector pPinchZoomDetector, TouchEvent pTouchEvent, float pZoomFactor) {
		// Calculate the zoom offset
		final float newZoomFactor = mInitialTouchZoomFactor * pZoomFactor;

		// Apply the zoom offset to the camera, allowing zooming of between
		// (default) 1x and 2x
		if (newZoomFactor < ZOOM_FACTOR_MAX && newZoomFactor > ZOOM_FACTOR_MIN)
			mCamera.setZoomFactorDirect(newZoomFactor);
	}

	@Override
	public void onPinchZoomFinished(PinchZoomDetector pPinchZoomDetector, TouchEvent pTouchEvent, float pZoomFactor) {
		// Calculate the zoom offset
		final float newZoomFactor = mInitialTouchZoomFactor * pZoomFactor;

		// Apply the zoom offset to the camera, allowing zooming of between
		// (default) 1x and 2x
		if (newZoomFactor < ZOOM_FACTOR_MAX && newZoomFactor > ZOOM_FACTOR_MIN)
			mCamera.setZoomFactorDirect(newZoomFactor);
	}

}
