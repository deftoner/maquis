package com.incrediblapps.games.maquis.scenes;

import java.util.ArrayList;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.text.Text;
import org.andengine.util.adt.color.Color;

import android.util.Log;

import com.incrediblapps.games.R;
import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.utils.LoadingRunnable;

public abstract class ManagedGameScene extends ManagedScene {

	private static final String TAG = "ManagedGameScene";

	// Create an easy to manage HUD that we can attach/detach when the game
	// scene is shown or hidden.
	// public HUD mGameHud = new HUD();

	// These objects will make up our loading scene.
	public Text mLoadingText;

	private Scene mLoadingScene;

	public ManagedGameScene thisManagedGameScene = this;

	ArrayList<LoadingRunnable> mLoadingSteps = new ArrayList<LoadingRunnable>();

	public float mLoadingStepsTotal = 0;

	public IUpdateHandler mLoadingStepsHandler = new IUpdateHandler() {
		@Override
		public void onUpdate(final float pSecondsElapsed) {
			Log.d(TAG, "onUpdate()");
			if (!ManagedGameScene.this.mLoadingSteps.isEmpty()) {
				ManagedGameScene.this.mLoadingSteps.get(0).run();
				ManagedGameScene.this.mLoadingSteps.remove(0);
				// ManagedGameScene.this.mLoadingRect.setWidth(ResourceManager.getInstance().cameraWidth
				// * (1f - (ManagedGameScene.this.mLoadingSteps.size() /
				// ManagedGameScene.this.mLoadingStepsTotal)));
				if (ManagedGameScene.this.mLoadingSteps.isEmpty()) {
					ManagedGameScene.this.isLoaded = true;
					ResourceManager.engine.unregisterUpdateHandler(this);
					return;
				}
				ManagedGameScene.this.isLoaded = false;
			} else {
				Log.d(TAG, "onUpdate() isEmpty");
				ManagedGameScene.this.isLoaded = true;
				ResourceManager.engine.unregisterUpdateHandler(this);
				return;
			}
		}

		@Override
		public void reset() {
		}
	};

	public ManagedGameScene() {
		// Let the Scene Manager know that we want to show a Loading Scene for
		// at least 2 seconds.
		this(2f);
	};

	public ManagedGameScene(float pLoadingScreenMinimumSecondsShown) {
		super(pLoadingScreenMinimumSecondsShown);
		// Setup the touch attributes for the Game Scenes.
		this.setOnSceneTouchListenerBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionDownEnabled(true);
		this.setTouchAreaBindingOnActionMoveEnabled(true);
		// Scale the Game Scenes according to the Camera's scale factor.
		// this.setScale(ResourceManager.getInstance().cameraScaleFactorX,
		// ResourceManager.getInstance().cameraScaleFactorY);
		// this.setPosition(0, 0);
		// GameHud.setScaleCenter(0f, 0f);
		// GameHud.setScale(ResourceManager.getInstance().cameraScaleFactorX,
		// ResourceManager.getInstance().cameraScaleFactorY);

		// ResourceManager.engine.getCamera().setCenter(1000, 1110);

		Log.d(TAG, "ManagedGameScene()");

	}

	@Override
	public Scene onLoadingScreenLoadAndShown() {

		Log.d(TAG, "onLoadingScreenLoadAndShown(), camera center x=" + ((SmoothCamera) ResourceManager.engine.getCamera()).getZoomFactor() + " y="
				+ ResourceManager.engine.getCamera().getCenterY());

		// Setup and return the loading screen.
		mLoadingScene = new Scene();

		// Rectangle r = new Rectangle(400, 240, 800, 480,
		// ResourceManager.engine.getVertexBufferObjectManager());
		// r.setColor(Color.ABGR_PACKED_BLUE_CLEAR);
		//
		// mLoadingScene.attachChild(r);

		mLoadingText = new Text(0, 0, ResourceManager.mFontPixelWhite, ResourceManager.context.getString(R.string.loading_screen), ResourceManager.engine.getVertexBufferObjectManager());
		mLoadingText.setPosition(mLoadingText.getWidth() / 2f, ResourceManager.cameraHeight - mLoadingText.getHeight() / 2f);
		mLoadingScene.attachChild(mLoadingText);
		return mLoadingScene;
	}

	@Override
	public void onLoadingScreenUnloadAndHidden() {

		Log.d(TAG, "onLoadingScreenUnloadAndHidden()");

		// detach & dispose of the the loading screen resources.
		ResourceManager.getActivity().runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				mLoadingText.detachSelf();
				mLoadingText.dispose();
				mLoadingScene.dispose();
				mLoadingText = null;
				mLoadingScene = null;
			}
		});
	}

	@Override
	public void onLoadScene() {

		Log.d(TAG, "onLoadScene()");

		// Load the resources to be used in the Game Scenes.
		// ResourceManager.loadGameResources();
		ResourceManager.engine.registerUpdateHandler(this.mLoadingStepsHandler);

		this.onLoadLevel();
	}

	public abstract void onLoadLevel();

	@Override
	public void onShowScene() {
		// We want to wait to set the HUD until the scene is shown because
		// otherwise it will appear on top of the loading screen.
		// ResourceManager.engine.getCamera().setHUD(mGameHud);
		Log.d(TAG, "onShowScene()");
	}

	@Override
	public void onHideScene() {
		Log.d(TAG, "onHideScene()");
	}

	@Override
	public void onUnloadScene() {

		Log.d(TAG, "onUnloadScene()");

		// detach and unload the scene.
		ResourceManager.engine.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				thisManagedGameScene.detachChildren();
				thisManagedGameScene.clearEntityModifiers();
				thisManagedGameScene.clearTouchAreas();
				thisManagedGameScene.clearUpdateHandlers();
			}
		});
	}

}