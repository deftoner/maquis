package com.incrediblapps.games.maquis.scenes;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.PinchZoomDetector;

import android.util.Log;

import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.elements.Closet;
import com.incrediblapps.games.maquis.elements.Door.DoorState;
import com.incrediblapps.games.maquis.elements.Door;
import com.incrediblapps.games.maquis.elements.FloorDoor;
import com.incrediblapps.games.maquis.elements.FloorDoorNoDoor;
import com.incrediblapps.games.maquis.elements.HideDoor;
import com.incrediblapps.games.maquis.elements.ObliqueDoor;
import com.incrediblapps.games.maquis.elements.UselessAnimatedElement;
import com.incrediblapps.games.maquis.elements.UselessElement;
import com.incrediblapps.games.maquis.elements.Wall;
import com.incrediblapps.games.maquis.players.actions.DoorAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.actions.WaitAction;
import com.incrediblapps.games.maquis.players.enemies.Enemy;
import com.incrediblapps.games.maquis.players.enemies.Soldier1;
import com.incrediblapps.games.maquis.players.main.MainCharacter;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;
import com.incrediblapps.games.maquis.utils.constants.Constants;
import com.incrediblapps.games.maquis.utils.constants.ConstantsLevel6;

public class SceneLevel6 extends SceneLevel {

	private static final String TAG = "SceneLevel6";

	public static final float ZOOM_FACTOR_MAX = 2f;

	public static final float ZOOM_FACTOR_MIN = 0.5f;

	// private AnimatedSprite papersHUD;

	private static final int ENEMY_NUMBER = 2;

	public ObliqueDoor door61010;

	public ObliqueDoor door61011;

	public ObliqueDoor door61050;

	public ObliqueDoor door61051;

	public ObliqueDoor door61052;

	public SceneLevel6() {
		super(1f);
		mCamera.setBounds(0, 0, ConstantsLevel6.WIDTH, ConstantsLevel6.HEIGHT);
		mEnemies = new Enemy[ENEMY_NUMBER];
	}

	@Override
	public void onLoadLevel() {
		Log.d(TAG, "onLoadScene level6");

		ResourceManager.loadLevel6Resources();

		createBackground();
		createHUD();
		createMainPlayer();

		// Floor1
		createDoor61010();
		createDoor61011();
		createWall60010();
		createWall60011();
		createHideDoor63010();
		createWC();
		createPicture6010();
		createPicture6011();
		createShelves61();
		createDoor6WC();

		// Floor2
		createWall60020();
		createWall60021();

		// Floor3
		createWall60030();
		createWall60031();
		createPillars();
		createRailings();
		createDoor61030();

		// Floor4
		createDoor61040();
		createDoor61041();
		createDoor61042();
		createDoor61043();
		createDoor61044();
		createWall60040();
		createWall60041();
		createWall60042();
		createHideDoor63040();
		createLibrary();
		createPoster6040();
		createPoster6041();
		createCouch();
		createViktoria();
		createCloset6040();

		// Floor5
		createDoor61050();
		createDoor61051();
		createDoor61052();
		createWall60050();
		createWall60051();
		createHideDoor63050();
		createCloset62050();

		// FloorDoors
		createFloorDoor62040_62050();
		createFloorDoor62041_62051();
		createFloorDoor62052_62010();
		createFloorDoorNoDoor4020_4031();
		createFloorDoorNoDoor4030_4040();

		// Miscellaneous
		createStreetLight1();
		createStreetLight3();
		createStreetLight2();

		// Enemies
		createEnemy1_100();
		createEnemy2_500();

		mBackgroundSprite.sortChildren();
	}

	@Override
	public void restartLevel() {
		Log.d(TAG, "restarting level");

		mPlayer.restart();
		mGameOver = false;

		mEnemies[0].restart();
		mEnemies[0].setRight();

		mEnemies[1].restart();
		mEnemies[1].setLeft();

		for (Door d : mDoorList) {
			d.setCurrentTileIndex(d.closedTile);
			d.setIgnoreUpdate(false);
		}

		// mTablePapers.restart();

		onShowScene();
	}

	private void createBackground() {
		int halfTextureWidth = (int) (ResourceManager.mBackgroundTextureRegionLvl6_1.getWidth() * 0.5f);
		int halfTextureWidth2 = (int) (ResourceManager.mBackgroundTextureRegionLvl6_2.getWidth() * 0.5f);
		int halfTextureHeight = (int) (ResourceManager.mBackgroundTextureRegionLvl6_1.getHeight() * 0.5f);

		Sprite mBackgroundSprite_1 = new Sprite(halfTextureWidth, halfTextureHeight, ResourceManager.mBackgroundTextureRegionLvl6_1,
				ResourceManager.engine.getVertexBufferObjectManager());

		Sprite mBackgroundSprite_2 = new Sprite(mBackgroundSprite_1.getX() + halfTextureWidth + halfTextureWidth2, halfTextureHeight,
				ResourceManager.mBackgroundTextureRegionLvl6_2, ResourceManager.engine.getVertexBufferObjectManager());

		mBackgroundSprite.attachChild(mBackgroundSprite_1);
		mBackgroundSprite.attachChild(mBackgroundSprite_2);
	}

	@Override
	public void createHUD() {
		super.createHUD();

		// papersHUD = new AnimatedSprite(0, 0,
		// ResourceManager.mHUDPapersTextureRegion,
		// ResourceManager.engine.getVertexBufferObjectManager());
		//
		// papersHUD.setAnchorCenter(0, 0);
		// papersHUD.setX(-1);
		// papersHUD.setY(-1);
		// papersHUD.setScale(0.75f);
		//
		// this.mCamera.getHUD().attachChild(papersHUD);
	}

	public void createMainPlayer() {
		mPlayer = new MainCharacter(this, ConstantsLevel6.PLAYER_START_POSITION_X, ConstantsLevel6.PLAYER_START_POSITION_Y, 2, 1);
		mPlayer.setLeft();
		mBackgroundSprite.registerUpdateHandler(mPlayer);
	}

	private void createEnemy1_100() {
		Enemy enemy = new Soldier1(this, ConstantsLevel6.ENEMY100_START_POSITION_X, ConstantsLevel6.ENEMY100_START_POSITION_Y, 1, 3);
		mBackgroundSprite.registerUpdateHandler(enemy);
		mEnemies[0] = enemy;

		// Movements
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_START_POSITION_X, 0, PlayerState.RUNNING_LEFT));
		enemy.mMovementsQueue.add(new WaitAction(3000));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_POINT1_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(door61011, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_POINT2_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(door61011, DoorState.CLOSED_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_POINT3_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(door61010, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_POINT4_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(door61010, DoorState.CLOSED_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_POINT5_POSITION_X, 0, PlayerState.RUNNING_LEFT));

		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_POINT5_POSITION_X, 0, PlayerState.RUNNING_RIGHT));
		enemy.mMovementsQueue.add(new WaitAction(2000));

		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_POINT4_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(door61010, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_POINT3_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(door61010, DoorState.CLOSED_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_POINT2_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(door61011, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_POINT1_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(door61011, DoorState.CLOSED_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY100_START_POSITION_X, 0, PlayerState.RUNNING_RIGHT));
	}

	private void createEnemy2_500() {
		Enemy enemy = new Soldier1(this, ConstantsLevel6.ENEMY500_START_POSITION_X, ConstantsLevel6.ENEMY500_START_POSITION_Y, 5, 1);
		mBackgroundSprite.registerUpdateHandler(enemy);
		mEnemies[1] = enemy;

		// Movements
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_START_POSITION_X, 0, PlayerState.RUNNING_RIGHT));
		enemy.mMovementsQueue.add(new WaitAction(3000));

		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT1_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(door61050, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT2_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(door61050, DoorState.CLOSED_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT3_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(door61051, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT4_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(door61051, DoorState.CLOSED_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT5_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(door61052, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT6_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_RIGHT));
		enemy.mMovementsQueue.add(new DoorAction(door61052, DoorState.CLOSED_DOOR));

		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT7_POSITION_X, 0, PlayerState.RUNNING_RIGHT));
		enemy.mMovementsQueue.add(new WaitAction(2000));

		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT6_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(door61052, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT5_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(door61052, DoorState.CLOSED_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT4_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(door61051, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT3_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(door61051, DoorState.CLOSED_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT2_POSITION_X, 0, PlayerState.RUNNING_TO_DOOR_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(door61050, DoorState.OPEN_DOOR));
		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_POINT1_POSITION_X, 0, PlayerState.RUNNING_LEFT));
		enemy.mMovementsQueue.add(new DoorAction(door61050, DoorState.CLOSED_DOOR));

		enemy.mMovementsQueue.add(new MovementAction(ConstantsLevel6.ENEMY500_START_POSITION_X, 0, PlayerState.RUNNING_LEFT));
	}

	private void createFloorDoorNoDoor4020_4031() {
		FloorDoorNoDoor door1 = new FloorDoorNoDoor(this, 0, 0, ConstantsLevel6.FD_NODOOR_WIDTH, ConstantsLevel6.FD_NODOOR_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager(), 2, 1);

		door1.setAnchorCenter(0, 0);
		door1.setX(ConstantsLevel6.FLOORDOOR4020_POSITION_X);
		door1.setY(ConstantsLevel6.FLOORDOOR4020_POSITION_Y);

		this.registerTouchArea(door1);
		mBackgroundSprite.attachChild(door1);

		FloorDoorNoDoor door2 = new FloorDoorNoDoor(this, 0, 0, ConstantsLevel6.FD_NODOOR_WIDTH, ConstantsLevel6.FD_NODOOR_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager(), 3, 2);

		door2.setAnchorCenter(0, 0);
		door2.setX(ConstantsLevel6.FLOORDOOR4031_POSITION_X);
		door2.setY(ConstantsLevel6.FLOORDOOR4031_POSITION_Y);

		this.registerTouchArea(door2);
		mBackgroundSprite.attachChild(door2);

		door1.mConnectedDoor = door2;
		door2.mConnectedDoor = door1;
	}

	private void createFloorDoorNoDoor4030_4040() {
		FloorDoorNoDoor door1 = new FloorDoorNoDoor(this, 0, 0, ConstantsLevel6.FD_NODOOR_WIDTH, ConstantsLevel6.FD_NODOOR_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager(), 3, 1);

		door1.setAnchorCenter(0, 0);
		door1.setX(ConstantsLevel6.FLOORDOOR4030_POSITION_X);
		door1.setY(ConstantsLevel6.FLOORDOOR4030_POSITION_Y);

		this.registerTouchArea(door1);
		mBackgroundSprite.attachChild(door1);

		FloorDoorNoDoor door2 = new FloorDoorNoDoor(this, 0, 0, ConstantsLevel6.FD_NODOOR_WIDTH, ConstantsLevel6.FD_NODOOR_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager(), 4, 4);

		door2.setAnchorCenter(0, 0);
		door2.setX(ConstantsLevel6.FLOORDOOR4040_POSITION_X);
		door2.setY(ConstantsLevel6.FLOORDOOR4040_POSITION_Y);

		this.registerTouchArea(door2);
		mBackgroundSprite.attachChild(door2);

		door1.mConnectedDoor = door2;
		door2.mConnectedDoor = door1;
	}

	private void createDoor61030() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor61030TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 3);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel6.DOOR61030_POSITION_X);
		door.setY(ConstantsLevel6.DOOR61030_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createPillars() {
		Sprite pillar1 = new Sprite(0, 0, ResourceManager.mPillar1TextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		pillar1.setAnchorCenter(0, 0);
		pillar1.setX(ConstantsLevel6.PILLAR1_POSITION_X);
		pillar1.setY(ConstantsLevel6.PILLAR1_POSITION_Y);
		pillar1.setZIndex(LAYER_DOORS);
		mBackgroundSprite.attachChild(pillar1);

		Sprite pillar2 = new Sprite(0, 0, ResourceManager.mPillar1TextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		pillar2.setAnchorCenter(0, 0);
		pillar2.setX(ConstantsLevel6.PILLAR2_POSITION_X);
		pillar2.setY(ConstantsLevel6.PILLAR2_POSITION_Y);
		pillar2.setZIndex(LAYER_DOORS);
		mBackgroundSprite.attachChild(pillar2);

		Sprite pillar3 = new Sprite(0, 0, ResourceManager.mPillar2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		pillar3.setAnchorCenter(0, 0);
		pillar3.setX(ConstantsLevel6.PILLAR3_POSITION_X);
		pillar3.setY(ConstantsLevel6.PILLAR3_POSITION_Y);
		mBackgroundSprite.attachChild(pillar3);

		Sprite pillar4 = new Sprite(0, 0, ResourceManager.mPillar2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		pillar4.setAnchorCenter(0, 0);
		pillar4.setX(ConstantsLevel6.PILLAR4_POSITION_X);
		pillar4.setY(ConstantsLevel6.PILLAR4_POSITION_Y);
		mBackgroundSprite.attachChild(pillar4);
	}

	private void createRailings() {
		Sprite railing1 = new Sprite(0, 0, ResourceManager.mRailing630TextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		railing1.setAnchorCenter(0, 0);
		railing1.setX(ConstantsLevel6.RAILING1_POSITION_X);
		railing1.setY(ConstantsLevel6.RAILING1_POSITION_Y);
		railing1.setZIndex(LAYER_DOORS);
		mBackgroundSprite.attachChild(railing1);

		Sprite railing2 = new Sprite(0, 0, ResourceManager.mRailing631TextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		railing2.setAnchorCenter(0, 0);
		railing2.setX(ConstantsLevel6.RAILING2_POSITION_X);
		railing2.setY(ConstantsLevel6.RAILING2_POSITION_Y);
		railing2.setZIndex(LAYER_DOORS);
		mBackgroundSprite.attachChild(railing2);
	}

	private void createWall60020() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60020_X, ConstantsLevel6.WALL60020_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
	}

	private void createWall60021() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60021_X, ConstantsLevel6.WALL60021_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
	}

	private void createWall60030() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60030_X, ConstantsLevel6.WALL60030_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
	}

	private void createWall60031() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60031_X, ConstantsLevel6.WALL60031_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
	}

	private void createStreetLight1() {
		UselessAnimatedElement streetlight = new UselessAnimatedElement(this, 0, 0, ResourceManager.mStreetLight61TextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager(), 0, 0);

		streetlight.setAnchorCenter(0, 0);
		streetlight.setX(ConstantsLevel6.STREETLIGHT1_POSITION_X);
		streetlight.setY(ConstantsLevel6.STREETLIGHT1_POSITION_Y);

		streetlight.animate(ConstantsLevel6.STREETLIGHT_FRAMES, true);

		this.registerTouchArea(streetlight);
		mBackgroundSprite.attachChild(streetlight);
	}

	private void createStreetLight2() {
		UselessAnimatedElement streetlight = new UselessAnimatedElement(this, 0, 0, ResourceManager.mStreetLight62TextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager(), 0, 0);

		streetlight.setAnchorCenter(0, 0);
		streetlight.setX(ConstantsLevel6.STREETLIGHT2_POSITION_X);
		streetlight.setY(ConstantsLevel6.STREETLIGHT2_POSITION_Y);

		streetlight.animate(ConstantsLevel6.STREETLIGHT_FRAMES, true);

		this.registerTouchArea(streetlight);
		mBackgroundSprite.attachChild(streetlight);
	}

	private void createStreetLight3() {
		UselessAnimatedElement streetlight = new UselessAnimatedElement(this, 0, 0, ResourceManager.mStreetLight63TextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager(), 0, 0);

		streetlight.setAnchorCenter(0, 0);
		streetlight.setX(ConstantsLevel6.STREETLIGHT3_POSITION_X);
		streetlight.setY(ConstantsLevel6.STREETLIGHT3_POSITION_Y);

		streetlight.animate(ConstantsLevel6.STREETLIGHT_FRAMES, true);

		this.registerTouchArea(streetlight);
		mBackgroundSprite.attachChild(streetlight);
	}

	private void createDoor61040() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor610401TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false,
				4);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel6.DOOR61040_POSITION_X);
		door.setY(ConstantsLevel6.DOOR61040_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createDoor61041() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor610401TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false,
				4);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel6.DOOR61041_POSITION_X);
		door.setY(ConstantsLevel6.DOOR61041_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createDoor61042() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor61042TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 4);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel6.DOOR61042_POSITION_X);
		door.setY(ConstantsLevel6.DOOR61042_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createDoor61043() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor610434TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false,
				4);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel6.DOOR61043_POSITION_X);
		door.setY(ConstantsLevel6.DOOR61043_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createDoor61044() {
		ObliqueDoor door = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor610434TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false,
				4);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel6.DOOR61044_POSITION_X);
		door.setY(ConstantsLevel6.DOOR61044_POSITION_Y);
		door.setZIndex(LAYER_DOORS);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createWall60040() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60040_X, ConstantsLevel6.WALL60040_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
	}

	private void createWall60041() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60041_X, ConstantsLevel6.WALL60041_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
	}

	private void createWall60042() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60042_X, ConstantsLevel6.WALL60042_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
	}

	private void createHideDoor63040() {
		HideDoor door = new HideDoor(this, 0, 0, ResourceManager.mDoor3TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 4, 2);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel6.HIDEDOOR63040_POSITION_X);
		door.setY(ConstantsLevel6.HIDEDOOR63040_POSITION_Y);
		door.setCurrentTileIndex(1);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createHideDoor63050() {
		HideDoor door = new HideDoor(this, 0, 0, ResourceManager.mDoor3TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 5, 2);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel6.HIDEDOOR63050_POSITION_X);
		door.setY(ConstantsLevel6.HIDEDOOR63050_POSITION_Y);
		door.setCurrentTileIndex(1);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createHideDoor63010() {
		HideDoor door = new HideDoor(this, 0, 0, ResourceManager.mDoor3TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1, 2);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel6.HIDEDOOR63010_POSITION_X);
		door.setY(ConstantsLevel6.HIDEDOOR63010_POSITION_Y);
		door.setCurrentTileIndex(1);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createFloorDoor62040_62050() {
		FloorDoor door1 = new FloorDoor(this, 0, 0, ResourceManager.mDoor2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 4, 3);

		door1.setAnchorCenter(0, 0);
		door1.setX(ConstantsLevel6.FLOORDOOR62040_POSITION_X);
		door1.setY(ConstantsLevel6.FLOORDOOR62040_POSITION_Y);
		door1.setCurrentTileIndex(1);

		mDoorList.add(door1);

		this.registerTouchArea(door1);
		mBackgroundSprite.attachChild(door1);

		FloorDoor door2 = new FloorDoor(this, 0, 0, ResourceManager.mDoor2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 5, 2);

		door2.setAnchorCenter(0, 0);
		door2.setX(ConstantsLevel6.FLOORDOOR62050_POSITION_X);
		door2.setY(ConstantsLevel6.FLOORDOOR62050_POSITION_Y);
		door2.setCurrentTileIndex(1);

		mDoorList.add(door2);

		this.registerTouchArea(door2);
		mBackgroundSprite.attachChild(door2);

		door1.mConnectedDoor = door2;
		door2.mConnectedDoor = door1;
	}

	private void createFloorDoor62041_62051() {
		FloorDoor door1 = new FloorDoor(this, 0, 0, ResourceManager.mDoor2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 4, 5);

		door1.setAnchorCenter(0, 0);
		door1.setX(ConstantsLevel6.FLOORDOOR62041_POSITION_X);
		door1.setY(ConstantsLevel6.FLOORDOOR62041_POSITION_Y);
		door1.setCurrentTileIndex(1);

		mDoorList.add(door1);

		this.registerTouchArea(door1);
		mBackgroundSprite.attachChild(door1);

		FloorDoor door2 = new FloorDoor(this, 0, 0, ResourceManager.mDoor2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 5, 3);

		door2.setAnchorCenter(0, 0);
		door2.setX(ConstantsLevel6.FLOORDOOR62051_POSITION_X);
		door2.setY(ConstantsLevel6.FLOORDOOR62051_POSITION_Y);
		door2.setCurrentTileIndex(1);

		mDoorList.add(door2);

		this.registerTouchArea(door2);
		mBackgroundSprite.attachChild(door2);

		door1.mConnectedDoor = door2;
		door2.mConnectedDoor = door1;
	}

	private void createFloorDoor62052_62010() {
		FloorDoor door1 = new FloorDoor(this, 0, 0, ResourceManager.mDoor2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 5, 4);

		door1.setAnchorCenter(0, 0);
		door1.setX(ConstantsLevel6.FLOORDOOR62052_POSITION_X);
		door1.setY(ConstantsLevel6.FLOORDOOR62052_POSITION_Y);
		door1.setCurrentTileIndex(1);

		mDoorList.add(door1);

		this.registerTouchArea(door1);
		mBackgroundSprite.attachChild(door1);

		FloorDoor door2 = new FloorDoor(this, 0, 0, ResourceManager.mDoor2TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1, 3);

		door2.setAnchorCenter(0, 0);
		door2.setX(ConstantsLevel6.FLOORDOOR62010_POSITION_X);
		door2.setY(ConstantsLevel6.FLOORDOOR62010_POSITION_Y);
		door2.setCurrentTileIndex(1);

		mDoorList.add(door2);

		this.registerTouchArea(door2);
		mBackgroundSprite.attachChild(door2);

		door1.mConnectedDoor = door2;
		door2.mConnectedDoor = door1;
	}

	private void createDoor61010() {
		door61010 = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor61010TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1);

		door61010.setAnchorCenter(0, 0);
		door61010.setX(ConstantsLevel6.DOOR61010_POSITION_X);
		door61010.setY(ConstantsLevel6.DOOR61010_POSITION_Y);
		door61010.setZIndex(LAYER_DOORS);

		mDoorList.add(door61010);

		this.registerTouchArea(door61010);
		mBackgroundSprite.attachChild(door61010);
	}

	private void createDoor61011() {
		door61011 = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor61011TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1);

		door61011.setAnchorCenter(0, 0);
		door61011.setX(ConstantsLevel6.DOOR61011_POSITION_X);
		door61011.setY(ConstantsLevel6.DOOR61011_POSITION_Y);
		door61011.setZIndex(LAYER_DOORS);

		mDoorList.add(door61011);

		this.registerTouchArea(door61011);
		mBackgroundSprite.attachChild(door61011);
	}

	private void createWall60010() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60010_X, ConstantsLevel6.WALL60010_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
		// wall.setDebug(true);
	}

	private void createWall60011() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60011_X, ConstantsLevel6.WALL60011_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
		// wall.setDebug(true);
	}

	private void createDoor61050() {
		door61050 = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor6105012TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 5);

		door61050.setAnchorCenter(0, 0);
		door61050.setX(ConstantsLevel6.DOOR61050_POSITION_X);
		door61050.setY(ConstantsLevel6.DOOR61050_POSITION_Y);
		door61050.setZIndex(LAYER_DOORS);

		mDoorList.add(door61050);

		this.registerTouchArea(door61050);
		mBackgroundSprite.attachChild(door61050);
	}

	private void createDoor61051() {
		door61051 = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor6105012TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 5);

		door61051.setAnchorCenter(0, 0);
		door61051.setX(ConstantsLevel6.DOOR61051_POSITION_X);
		door61051.setY(ConstantsLevel6.DOOR61051_POSITION_Y);
		door61051.setZIndex(LAYER_DOORS);

		mDoorList.add(door61051);

		this.registerTouchArea(door61051);
		mBackgroundSprite.attachChild(door61051);
	}

	private void createDoor61052() {
		door61052 = new ObliqueDoor(this, 0, 0, ResourceManager.mDoor6105012TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 5);

		door61052.setAnchorCenter(0, 0);
		door61052.setX(ConstantsLevel6.DOOR61052_POSITION_X);
		door61052.setY(ConstantsLevel6.DOOR61052_POSITION_Y);
		door61052.setZIndex(LAYER_DOORS);

		mDoorList.add(door61052);

		this.registerTouchArea(door61052);
		mBackgroundSprite.attachChild(door61052);
	}

	private void createWall60050() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60050_X, ConstantsLevel6.WALL60050_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
		// wall.setDebug(true);
	}

	private void createWall60051() {
		Wall wall = new Wall(mPlayer, ConstantsLevel6.WALL60051_X, ConstantsLevel6.WALL60051_Y, Constants.Shared.WALL_WIDTH, Constants.Shared.WALL_HEIGHT,
				ResourceManager.engine.getVertexBufferObjectManager());
		mBackgroundSprite.attachChild(wall);
		// wall.setDebug(true);
	}

	private void createCloset62050() {
		UselessElement closet = new UselessElement(this, 0, 0, ResourceManager.mCloset62050TextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager(), 5, 2);

		closet.setAnchorCenter(0, 0);
		closet.setX(ConstantsLevel6.CLOSET62050_POSITION_X);
		closet.setY(ConstantsLevel6.CLOSET62050_POSITION_Y);

		this.registerTouchArea(closet);
		mBackgroundSprite.attachChild(closet);
	}

	private void createLibrary() {
		UselessElement library = new UselessElement(this, 0, 0, ResourceManager.mLibraryTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(),
				4, 7);

		library.setAnchorCenter(0, 0);
		library.setX(ConstantsLevel6.LIBRARY_POSITION_X);
		library.setY(ConstantsLevel6.LIBRARY_POSITION_Y);

		this.registerTouchArea(library);
		mBackgroundSprite.attachChild(library);
	}

	private void createPoster6040() {
		UselessElement poster = new UselessElement(this, 0, 0, ResourceManager.mPoster6040TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(),
				4, 1);

		poster.setAnchorCenter(0, 0);
		poster.setX(ConstantsLevel6.POSTER6040_POSITION_X);
		poster.setY(ConstantsLevel6.POSTER6040_POSITION_Y);

		this.registerTouchArea(poster);
		mBackgroundSprite.attachChild(poster);
	}

	private void createPoster6041() {
		UselessElement poster = new UselessElement(this, 0, 0, ResourceManager.mPoster6041TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(),
				5, 2);

		poster.setAnchorCenter(0, 0);
		poster.setX(ConstantsLevel6.POSTER6041_POSITION_X);
		poster.setY(ConstantsLevel6.POSTER6041_POSITION_Y);

		this.registerTouchArea(poster);
		mBackgroundSprite.attachChild(poster);
	}

	private void createCouch() {
		UselessElement couch = new UselessElement(this, 0, 0, ResourceManager.mCouchTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), 4, 7);

		couch.setAnchorCenter(0, 0);
		couch.setX(ConstantsLevel6.COUCH_POSITION_X);
		couch.setY(ConstantsLevel6.COUCH_POSITION_Y);

		this.registerTouchArea(couch);
		mBackgroundSprite.attachChild(couch);
	}

	private void createViktoria() {
		UselessElement viktoria = new UselessElement(this, 0, 0, ResourceManager.mViktoriaTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(),
				4, 5);

		viktoria.setAnchorCenter(0, 0);
		viktoria.setX(ConstantsLevel6.VIKTORIA_POSITION_X);
		viktoria.setY(ConstantsLevel6.VIKTORIA_POSITION_Y);

		this.registerTouchArea(viktoria);
		mBackgroundSprite.attachChild(viktoria);
	}

	private void createWC() {
		UselessElement wc1 = new UselessElement(this, 0, 0, ResourceManager.mWCTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), 1, 1);

		wc1.setAnchorCenter(0, 0);
		wc1.setX(ConstantsLevel6.WC1_POSITION_X);
		wc1.setY(ConstantsLevel6.WC1_POSITION_Y);

		this.registerTouchArea(wc1);
		mBackgroundSprite.attachChild(wc1);

		UselessElement wc2 = new UselessElement(this, 0, 0, ResourceManager.mWCTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), 1, 1);

		wc2.setAnchorCenter(0, 0);
		wc2.setX(ConstantsLevel6.WC2_POSITION_X);
		wc2.setY(ConstantsLevel6.WC2_POSITION_Y);

		this.registerTouchArea(wc2);
		mBackgroundSprite.attachChild(wc2);

		UselessElement wc3 = new UselessElement(this, 0, 0, ResourceManager.mWCTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), 1, 1);

		wc3.setAnchorCenter(0, 0);
		wc3.setX(ConstantsLevel6.WC3_POSITION_X);
		wc3.setY(ConstantsLevel6.WC3_POSITION_Y);

		this.registerTouchArea(wc3);
		mBackgroundSprite.attachChild(wc3);
	}

	private void createPicture6010() {
		UselessElement picture = new UselessElement(this, 0, 0, ResourceManager.mPicture6010TextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager(), 1, 2);

		picture.setAnchorCenter(0, 0);
		picture.setX(ConstantsLevel6.PICTURE6010_POSITION_X);
		picture.setY(ConstantsLevel6.PICTURE6010_POSITION_Y);

		this.registerTouchArea(picture);
		mBackgroundSprite.attachChild(picture);
	}

	private void createPicture6011() {
		UselessElement picture = new UselessElement(this, 0, 0, ResourceManager.mPicture6011TextureRegion,
				ResourceManager.engine.getVertexBufferObjectManager(), 1, 2);

		picture.setAnchorCenter(0, 0);
		picture.setX(ConstantsLevel6.PICTURE6011_POSITION_X);
		picture.setY(ConstantsLevel6.PICTURE6011_POSITION_Y);

		this.registerTouchArea(picture);
		mBackgroundSprite.attachChild(picture);
	}

	private void createShelves61() {
		UselessElement picture = new UselessElement(this, 0, 0, ResourceManager.mShelves61TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(),
				1, 3);

		picture.setAnchorCenter(0, 0);
		picture.setX(ConstantsLevel6.SHELVES61_POSITION_X);
		picture.setY(ConstantsLevel6.SHELVES61_POSITION_Y);

		this.registerTouchArea(picture);
		mBackgroundSprite.attachChild(picture);
	}

	private void createDoor6WC() {
		Sprite wcWall = new Sprite(0, 0, ResourceManager.mWCWallTextureRegion, ResourceManager.engine.getVertexBufferObjectManager());

		wcWall.setAnchorCenter(0, 0);
		wcWall.setX(ConstantsLevel6.WC_WALL_POSITION_X);
		wcWall.setY(ConstantsLevel6.WC_WALL_POSITION_Y);
		mBackgroundSprite.attachChild(wcWall);

		// OpenableDoor door = new OpenableDoor(this, 0, 0,
		// ResourceManager.mDoor6WCTextureRegion,
		// ResourceManager.engine.getVertexBufferObjectManager(), false, 1);
		//
		// door.setAnchorCenter(0, 0);
		// door.setX(ConstantsLevel6.DOOR6WC_POSITION_X);
		// door.setY(ConstantsLevel6.DOOR6WC_POSITION_Y);
		//
		// this.registerTouchArea(door);
		// mBackgroundSprite.attachChild(door);

		HideDoor door = new HideDoor(this, 0, 0, ResourceManager.mDoor6WCTextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 1, 1, 14,
				25);

		door.setAnchorCenter(0, 0);
		door.setX(ConstantsLevel6.DOOR6WC_POSITION_X);
		door.setY(ConstantsLevel6.DOOR6WC_POSITION_Y);
		door.setCurrentTileIndex(1);

		mDoorList.add(door);

		this.registerTouchArea(door);
		mBackgroundSprite.attachChild(door);
	}

	private void createCloset6040() {
		Closet closet = new Closet(this, 0, 0, ResourceManager.mCloset6040TextureRegion, ResourceManager.engine.getVertexBufferObjectManager(), false, 4, 6);

		closet.setAnchorCenter(0, 0);
		closet.setX(ConstantsLevel6.CLOSET6040_POSITION_X);
		closet.setY(ConstantsLevel6.CLOSET6040_POSITION_Y);

		mDoorList.add(closet);

		this.registerTouchArea(closet);
		mBackgroundSprite.attachChild(closet);
	}

	@Override
	public void onShowScene() {
		// Star the Level with the camera in some point, depending on the level
		mCamera.setCenterDirect(ConstantsLevel6.PLAYER_START_POSITION_X, ConstantsLevel6.PLAYER_START_POSITION_Y);
		mCamera.setZoomFactorDirect(0.75f);
		mCamera.getHUD().setVisible(true);
		mGameLayerHUD.setVisible(true);
	}

	@Override
	public void onHideScene() {
		Log.d(TAG, "onHideScene level");
		// UnLoad resources for level1
		this.mCamera.setChaseEntity(null);
		final HUD oldHUD = this.mCamera.getHUD();
		if (oldHUD != null) {
			oldHUD.detachSelf();
			oldHUD.dispose();
			this.mCamera.setHUD(null);
		}
	}

	@Override
	public void onPinchZoomStarted(PinchZoomDetector pPinchZoomDetector, TouchEvent pSceneTouchEvent) {
		// Obtain the initial zoom factor on pinch detection
		mInitialTouchZoomFactor = mCamera.getZoomFactor();
	}

	@Override
	public void onPinchZoom(PinchZoomDetector pPinchZoomDetector, TouchEvent pTouchEvent, float pZoomFactor) {
		// Calculate the zoom offset
		final float newZoomFactor = mInitialTouchZoomFactor * pZoomFactor;

		// Apply the zoom offset to the camera, allowing zooming of between
		// (default) 1x and 2x
		if (newZoomFactor < ZOOM_FACTOR_MAX && newZoomFactor > ZOOM_FACTOR_MIN)
			mCamera.setZoomFactorDirect(newZoomFactor);
	}

	@Override
	public void onPinchZoomFinished(PinchZoomDetector pPinchZoomDetector, TouchEvent pTouchEvent, float pZoomFactor) {
		// Calculate the zoom offset
		final float newZoomFactor = mInitialTouchZoomFactor * pZoomFactor;

		// Apply the zoom offset to the camera, allowing zooming of between
		// (default) 1x and 2x
		if (newZoomFactor < ZOOM_FACTOR_MAX && newZoomFactor > ZOOM_FACTOR_MIN)
			mCamera.setZoomFactorDirect(newZoomFactor);
	}

}
