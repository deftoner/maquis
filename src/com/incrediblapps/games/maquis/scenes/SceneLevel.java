package com.incrediblapps.games.maquis.scenes;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.text.Text;
import org.andengine.entity.util.FPSCounter;
import org.andengine.input.touch.TouchEvent;
import org.andengine.input.touch.detector.PinchZoomDetector;
import org.andengine.input.touch.detector.PinchZoomDetector.IPinchZoomDetectorListener;

import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

import com.incrediblapps.games.maquis.ResourceManager;
import com.incrediblapps.games.maquis.elements.Door;
import com.incrediblapps.games.maquis.elements.Door.DoorState;
import com.incrediblapps.games.maquis.players.actions.HideDoorAction;
import com.incrediblapps.games.maquis.players.actions.HidePlayerAction;
import com.incrediblapps.games.maquis.players.actions.MovementAction;
import com.incrediblapps.games.maquis.players.enemies.Enemy;
import com.incrediblapps.games.maquis.players.main.MainCharacter;
import com.incrediblapps.games.maquis.players.main.Player.PlayerState;

public abstract class SceneLevel extends ManagedGameScene implements IPinchZoomDetectorListener, IOnSceneTouchListener {

	private static final String TAG = "SceneLevel";

	public static final String FPS_TEXT_DEBUG = "FPS:";

	public static final int LAYER_PLAYER = 1;

	public static final int LAYER_DOORS = 2;

	// Zoom detector
	protected PinchZoomDetector mPinchZoomDetector;

	protected static GestureDetector mGestureDetector;

	public TouchEvent mGestureTouchEvent;

	protected float mInitialTouchZoomFactor;

	// Initial scene touch coordinates on ACTION_DOWN
	protected float mInitialTouchX;

	protected float mInitialTouchY;

	protected SmoothCamera mCamera;

	// Scene Elements

	public Entity mBackgroundSprite;

	public MainCharacter mPlayer;

	protected final FPSCounter fpsCounter = new FPSCounter();

	private Text mFpsCounterText;

	public Enemy[] mEnemies;

	public boolean mGameOver = false;

	public Rectangle mChaseEntity;

	public Entity mGameLayerHUD;

	public Entity mIntroductionLayerHUD;

	protected List<Door> mDoorList;

	public SceneLevel(float pSeconds) {
		// Call super(float) with the number of minimum seconds to show the
		// LoadingScreen (0 to disable it)
		super(pSeconds);

		// Register this activity as our touch listener
		this.setOnSceneTouchListener(this);

		ResourceManager.activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				SceneLevel.mGestureDetector = new GestureDetector(ResourceManager.activity, new InnerOnGestureDetectorListener());
			}
		});

		mCamera = (SmoothCamera) ResourceManager.engine.getCamera();
		mCamera.setZoomFactorDirect(1f);

		// Register this activity as our zoom detector listener and enable it
		mPinchZoomDetector = new PinchZoomDetector(this);
		mPinchZoomDetector.setEnabled(true);

		// Register the FPSCounter for debug
		ResourceManager.engine.registerUpdateHandler(fpsCounter);

		mDoorList = new ArrayList<Door>();

		mBackgroundSprite = new Entity();
		this.attachChild(mBackgroundSprite);
	}

	@Override
	public boolean onSceneTouchEvent(final Scene pScene, final TouchEvent pSceneTouchEvent) {

		mGestureTouchEvent = pSceneTouchEvent;
		mPinchZoomDetector.onTouchEvent(pSceneTouchEvent);

		boolean handled = SceneLevel.mGestureDetector.onTouchEvent(pSceneTouchEvent.getMotionEvent());

		if (pSceneTouchEvent.isActionDown()) {
			// Obtain the initial touch coordinates when the scene is first
			// pressed
			mInitialTouchX = pSceneTouchEvent.getX();
			mInitialTouchY = pSceneTouchEvent.getY();

			// Log.d(TAG, "TouchEvent DOWN - x=" + mInitialTouchX + " y=" +
			// mInitialTouchY);
		}

		if (pSceneTouchEvent.isActionMove()) {
			// Calculate the offset touch coordinates
			final float touchOffsetX = mInitialTouchX - pSceneTouchEvent.getX();
			final float touchOffsetY = mInitialTouchY - pSceneTouchEvent.getY();

			// Apply the offset touch coordinates to the current camera
			// coordinates
			mCamera.setCenter(mCamera.getCenterX() + touchOffsetX, mCamera.getCenterY() + touchOffsetY);

			// Log.d(TAG, "TouchEvent MOVE - offX=" + touchOffsetX + " offY=" +
			// touchOffsetY);

			return true;

		}

		Log.d(TAG, "gesture " + handled);

		return handled;
	}

	public abstract void onLoadLevel();

	public abstract void createMainPlayer();

	protected class InnerOnGestureDetectorListener extends SimpleOnGestureListener {
		@Override
		public boolean onSingleTapConfirmed(final MotionEvent e) {
			return SceneLevel.this.onSingleTap();
		}

		// @Override
		// public boolean onDoubleTap(final MotionEvent e) {
		// return SceneLevel.this.onDoubleTap();
		// }
	}

	public boolean onSingleTap() {
		if (!mPlayer.mIsLocked) {
			float targetX = (float) Math.floor(mGestureTouchEvent.getX());
			if (targetX % 2 == 1) {
				targetX++;
			}
			float targetY = (float) Math.floor(mGestureTouchEvent.getY());

			if (!mPlayer.mMovementsQueue.isEmpty()) {
				if (mPlayer.mMovementsQueue.element() instanceof MovementAction
						&& (((MovementAction) mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_LEFT || ((MovementAction) mPlayer.mMovementsQueue.element()).mState == PlayerState.RUNNING_RIGHT)) {
					mPlayer.mHasChanged = true;
					mPlayer.mMovementsQueue.clear();
				} else {
					return true;
				}
			}

			if (mPlayer.mIsHidden) {
				mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mPlayer.mWhereIsHidden, DoorState.OPEN_DOOR));
				mPlayer.mMovementsQueue.add(new HidePlayerAction(mPlayer, false));
				mPlayer.mMovementsQueue.add(new MovementAction(targetX, mPlayer.mPlayerY - ((Door) mPlayer.mWhereIsHidden).gotodoor, PlayerState.TELEPORTATION));
				mPlayer.mMovementsQueue.add(new HideDoorAction((Door) mPlayer.mWhereIsHidden, DoorState.CLOSED_DOOR));
				mPlayer.mWhereIsHidden = null;
			}

			if (mPlayer.mPlayerX < targetX) {
				mPlayer.mMovementsQueue.add(new MovementAction(targetX, targetY, PlayerState.RUNNING_RIGHT));
			} else {
				mPlayer.mMovementsQueue.add(new MovementAction(targetX, targetY, PlayerState.RUNNING_LEFT));
			}
		}

		return true;
	}

	// public abstract boolean onDoubleTap();

	public void onHideScene() {
		super.onHideScene();
	}

	public abstract void restartLevel();

	public void createHUD() {
		this.mCamera.setHUD(new HUD());
		// this.mCamera.getHUD().setVisible(true);

		mGameLayerHUD = new Entity();
		mGameLayerHUD.setVisible(false);
		this.mCamera.getHUD().attachChild(mGameLayerHUD);
		mIntroductionLayerHUD = new Entity();
		mIntroductionLayerHUD.setVisible(false);
		this.mCamera.getHUD().attachChild(mIntroductionLayerHUD);

		mFpsCounterText = new Text(this.mCamera.getWidth() / 2f, 0f, ResourceManager.mFontPixelWhite, FPS_TEXT_DEBUG + "0", 15,
				ResourceManager.engine.getVertexBufferObjectManager());

		mFpsCounterText.setPosition(this.mCamera.getWidth() / 2f, this.mCamera.getHeight() - (mFpsCounterText.getHeight() / 2f + 4));
		// mFpsCounterText.setPosition(this.mCamera.getWidth() / 2f,
		// this.mCamera.getHeight() - (mFpsCounterText.getHeight() / 2f));

		// mFpsCounterText.setScale(0.75f);
		// mFpsCounterText.setAlpha(0.85f);

		mGameLayerHUD.attachChild(mFpsCounterText);

		this.registerUpdateHandler(new TimerHandler(1 / 2.0f, true, new ITimerCallback() {
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) {
				mFpsCounterText.setText(FPS_TEXT_DEBUG + fpsCounter.getFPS());
				fpsCounter.reset();
				pTimerHandler.reset();
			}
		}));

		// final GrowButton PauseButton = new
		// GrowButton(ResourceManager.gamePauseButtonTR.getWidth() / 2f,
		// GameLevel.this.mCamera.getHeight()
		// - (ResourceManager.gamePauseButtonTR.getHeight() / 2f),
		// ResourceManager.gamePauseButtonTR) {
		// @Override
		// public void onClick() {
		// SceneManager.getInstance().showLayer(LevelPauseLayer.getInstance(GameLevel.this),
		// false, true, true);
		// }
		// };
		// final GrowToggleButton MusicToggleButton = new
		// GrowToggleButton(PauseButton.getX() + 75f, PauseButton.getY(),
		// ResourceManager.MusicToggleTTR, !SFXManager.isMusicMuted()) {
		// @Override
		// public boolean checkState() {
		// return !SFXManager.isMusicMuted();
		// }
		//
		// @Override
		// public void onClick() {
		// SFXManager.toggleMusicMuted();
		// }
		// };
		// final GrowToggleButton SoundToggleButton = new
		// GrowToggleButton(MusicToggleButton.getX() + 75f,
		// MusicToggleButton.getY(),
		// ResourceManager.SoundToggleTTR, !SFXManager.isSoundMuted()) {
		// @Override
		// public boolean checkState() {
		// return !SFXManager.isSoundMuted();
		// }
		//
		// @Override
		// public void onClick() {
		// SFXManager.toggleSoundMuted();
		// }
		// };

		// GameLevel.this.mCamera.getHUD().attachChild(PauseButton);
		// GameLevel.this.mCamera.getHUD().attachChild(MusicToggleButton);
		// GameLevel.this.mCamera.getHUD().attachChild(SoundToggleButton);
		// GameLevel.this.mCamera.getHUD().registerTouchArea(PauseButton);
		// GameLevel.this.mCamera.getHUD().registerTouchArea(MusicToggleButton);
		// GameLevel.this.mCamera.getHUD().registerTouchArea(SoundToggleButton);
		// final Text LevelIndexText = new
		// Text(GameLevel.this.mCamera.getWidth() / 2f,
		// GameLevel.this.mCamera.getHeight() / 2f,
		// ResourceManager.fontDefaultMagneTank48, mLEVEL_NUMBER_PRETEXT +
		// GameLevel.this.mLevelDef.mLevelIndex,
		// ResourceManager.getActivity().getVertexBufferObjectManager());
		// LevelIndexText.setAlpha(0.85f);
		// LevelIndexText.registerEntityModifier(new SequenceEntityModifier(new
		// DelayModifier(1.5f), new MoveModifier(2f,
		// GameLevel.this.mCamera.getWidth() / 2f,
		// GameLevel.this.mCamera.getHeight() / 2f,
		// GameLevel.this.mCamera.getWidth()
		// - (LevelIndexText.getWidth() * 0.6f),
		// GameLevel.this.mCamera.getHeight() - (LevelIndexText.getHeight() *
		// 0.6f),
		// EaseElasticOut.getInstance())));
		// GameLevel.this.mCamera.getHUD().attachChild(LevelIndexText);
		// GameLevel.this.mCamera.getHUD().setTouchAreaBindingOnActionDownEnabled(true);
		// GameLevel.this.mCamera.getHUD().setTouchAreaBindingOnActionMoveEnabled(true);
	}

}
