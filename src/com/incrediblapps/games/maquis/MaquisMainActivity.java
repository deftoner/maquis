package com.incrediblapps.games.maquis;

import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

import android.os.Handler;
import android.util.Log;

import com.incrediblapps.games.maquis.layers.PauseLayer;
import com.incrediblapps.games.maquis.menus.SplashScreens;
import com.incrediblapps.games.maquis.menus.levelselector.LevelSelectorScene;
import com.incrediblapps.games.maquis.scenes.SceneLevel;
import com.incrediblapps.games.maquis.utils.MaquisSmoothCamera;
import com.incrediblapps.games.maquis.utils.constants.Constants;

public class MaquisMainActivity extends BaseGameActivity {

	private static final String TAG = "MaquisMainActivity";

	private MaquisSmoothCamera mCamera;

	public static Handler mHandler;

	@Override
	public EngineOptions onCreateEngineOptions() {

		// Create the zoom camera
		mCamera = new MaquisSmoothCamera(0, 0, Constants.Resolution.DEFAULT_WIDTH, Constants.Resolution.DEFAULT_HEIGHT, Constants.Camera.MAX_VELOCITY_X,
				Constants.Camera.MAX_VELOCITY_Y, Constants.Camera.MAX_FACTORZOOM_CHANGE);

		// Enable our level bounds so that we can't scroll too far
		mCamera.setBounds(0, 0, Constants.Resolution.DEFAULT_WIDTH, Constants.Resolution.DEFAULT_HEIGHT);
		mCamera.setBoundsEnabled(true);

		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), mCamera);

		mHandler = new Handler();

		// Enable sounds.
		engineOptions.getAudioOptions().setNeedsSound(true);
		// Enable music.
		engineOptions.getAudioOptions().setNeedsMusic(true);
		// Set the Wake Lock options to prevent the engine from dumping textures
		// when focus changes.
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);

		return engineOptions;
	}

	@Override
	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) {

		// TODO: Fix Scale values passed to the setup method
		// / Setup the ResourceManager.
		ResourceManager.setup(this, mEngine, this.getApplicationContext(), Constants.Resolution.DEFAULT_WIDTH, Constants.Resolution.DEFAULT_HEIGHT, 1, 1);

		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) {
		// ResourceManager.loadGameResources();

		// Load the resources necessary for the Main Menu
		// ResourceManager.loadSplashResources();
		// Tell the SceneManager to show the splash screens.
		SceneManager.getInstance().showScene(new SplashScreens());

		// SceneManager.getInstance().showMainMenu();
		// Set to zero to don't show the loading page each time we come back
		// from a level scene
		// MainMenu.getInstance().hasLoadingScreen = false;

		pOnCreateSceneCallback.onCreateSceneFinished(mEngine.getScene());

		// NullpointerException
		//SFXManager.playMusic();
		
		
		// If the music is muted in the settings, mute it in the game.
		if (getIntFromSharedPreferences(SHARED_PREFS_MUSIC_MUTED) > 0) {
			SFXManager.setMusicMuted(true);
		}
		// If the sound effects are muted in the settings, mute them in the
		// game.
		if (getIntFromSharedPreferences(SHARED_PREFS_SOUNDS_MUTED) > 0) {
			SFXManager.setSoundMuted(true);
		}
	}

	@Override
	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) {
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

	// The following list of Strings are keys within the Shared Preferences.
	// The name of the shared preferences used by this game.
	public static final String SHARED_PREFS_MAIN = "MaquisSettings";

	// The muted state of the music. True = muted.
	public static final String SHARED_PREFS_MAX_CHAPTER = "max.chapter";

	// The muted state of the music. True = muted.
	public static final String SHARED_PREFS_MAX_LEVEL = "max.level";

	// The quality (Boolean) setting. True = High Quality.
	// public static final String SHARED_PREFS_HIGH_QUALITY_GRAPHICS =
	// "quality";
	// // How many stars (Integer) the player got in each level.
	// public static final String SHARED_PREFS_LEVEL_STARS = "level.stars.";
	// // The highscore (Integer) that the player has gotten for each level.
	// public static final String SHARED_PREFS_LEVEL_HIGHSCORE =
	// "level.highscore.";
	// // The highest level (Integer) reached by the player.
	// public static final String SHARED_PREFS_LEVEL_MAX_REACHED =
	// "levels.reached.";
	// // The number (Integer) of times that the application has been started.
	// public static final String SHARED_PREFS_ACTIVITY_START_COUNT = "count";
	// // The player has rated the game. True = player has agreed to rate it.
	// public static final String SHARED_PREFS_RATING_SUCCESS = "rating";
	// // The muted state of the music. True = muted.
	public static final String SHARED_PREFS_MUSIC_MUTED = "mute.music";

	// The muted state of the sound effects. True = muted.
	public static final String SHARED_PREFS_SOUNDS_MUTED = "mute.sounds";

	// The number of times that the application has started, stored as a local
	// value
	// public int numTimesActivityOpened;

	// Methods to write/read Integers in the Shared Preferences.
	public static int writeIntToSharedPreferences(final String pStr, final int pValue) {
		// The apply() method requires API level 9 in the manifest.
		ResourceManager.getActivity().getSharedPreferences(SHARED_PREFS_MAIN, 0).edit().putInt(pStr, pValue).apply();
		return pValue;
	}

	public static int getIntFromSharedPreferences(final String pStr) {
		return ResourceManager.getActivity().getSharedPreferences(SHARED_PREFS_MAIN, 0).getInt(pStr, 0);
	}

	// Methods to write/read Booleans in the Shared Preferences
	public static void writeBooleanToSharedPreferences(final String pStr, final boolean pValue) {
		// The apply() method requires API level 9 in the manifest.
		ResourceManager.getActivity().getSharedPreferences(SHARED_PREFS_MAIN, 0).edit().putBoolean(pStr, pValue).apply();
	}

	public static boolean getBooleanFromSharedPreferences(final String pStr) {
		return ResourceManager.getActivity().getSharedPreferences(SHARED_PREFS_MAIN, 0).getBoolean(pStr, false);
	}

	// // A convenience method for accessing how many stars the player achieved
	// on a certain level.
	// public static int getLevelStars(final int pLevelNumber) {
	// return getIntFromSharedPreferences(SHARED_PREFS_LEVEL_STARS +
	// String.valueOf(pLevelNumber));
	// }

	@Override
	public void onBackPressed() {
		// If the resource manager has been setup...
		if (ResourceManager.engine != null) {
			// if a layer is shown...
			if (SceneManager.getInstance().mIsLayerShown) {
				// hide the layer.
				if (SceneManager.getInstance().mCurrentLayer.getClass().equals(PauseLayer.class)) {
					SceneManager.getInstance().mCurrentLayer.onHideLayer();
				} else {
					SceneManager.getInstance().mCurrentLayer.onHideLayer();
				}
			}
			// or if a game level is shown...
			else {
				if (SceneManager.getInstance().mCurrentScene.getClass().getGenericSuperclass().equals(SceneLevel.class)) {
					// SceneManager.getInstance().showLevelSelector();
					SceneManager.getInstance().showLayer(PauseLayer.getInstance((SceneLevel) ResourceManager.engine.getScene()), false, true, true);
				} else if (SceneManager.getInstance().mCurrentScene.getClass().equals(LevelSelectorScene.class)) {
					Log.d(TAG, "back button del level selector");
					SceneManager.getInstance().showMainMenu();
				}
				// or if the Main Menu is already shown...
				else {
					// exit the game.
					System.exit(0);
				}
			}
		}
	}
}