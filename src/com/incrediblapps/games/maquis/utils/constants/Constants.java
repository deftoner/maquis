package com.incrediblapps.games.maquis.utils.constants;

public class Constants {

	public class Resolution {
		public static final int DEFAULT_WIDTH = 800;

		public static final int DEFAULT_HEIGHT = 480;

		public static final int DEFAULT_RATIO = DEFAULT_HEIGHT / DEFAULT_WIDTH;

		public static final int MENU_BG_WIDTH = 1280;

		public static final int MENU_BG_HEIGHT = 720;
	}

	public class LevelSelector {
		public static final int WIDTH = 1144;

		public static final int HEIGHT = 946;

		public static final int ATLAS_WIDTH = 3500;

		public static final int ATLAS_HEIGHT = 3000;

		public static final int FLAG_ATLAS_WIDTH = 276;

		public static final int FLAG_ATLAS_HEIGHT = 48;

		public static final int FLAG_LEVEL1_POSITION_X = 697;

		public static final int FLAG_LEVEL1_POSITION_Y = 369;

		public static final int FLAG_LEVEL6_POSITION_X = 789;

		public static final int FLAG_LEVEL6_POSITION_Y = 521;
	}

	public class Camera {
		public static final float MAX_VELOCITY_X = 1000f;

		public static final float MAX_VELOCITY_Y = 1000f;

		public static final float MAX_FACTORZOOM_CHANGE = 0.1f;
	}

	public class Player {
		public static final int PLAYER_TILES_COLUMNS = 7;

		public static final int PLAYER_TILES_ROWS = 3;

		public static final int PLAYER_ATLAS_WIDTH = 748;

		public static final int PLAYER_ATLAS_HEIGHT = 512;

		public static final int CONFUSED_TILES_COLUMNS = 6;

		public static final int CONFUSED_TILES_ROWS = 1;

		public static final float SHAPE_WIDTH = 26;

		public static final float SHAPE_HEIGHT = 60;

		public static final int CLIMBING_TILES_COLUMNS = 11;

		public static final int CLIMBING_TILES_ROWS = 1;

		public static final float RUN_VELOCITY = 2;

		public static final float GOTODOOR_VELOCITY = 0.5f;

		public static final long BACK_FRAMES = 250;

		public static final long PLAYER_FRAMES = 100;

		public static final long PLAYER_GOTODOOR_FRAMES = 150;

		public static final long PLAYER_CONFUSED_FRAMES = 250;

		public static final long PLAYER_CLIMBING_FRAMES = 150;
	}

	public class Enemies {

		public class Maqui {
			public static final float RUN_VELOCITY = 1f;

			public static final long WALK_FRAMES = 200;

			public static final int TILES_COLUMNS = 12;

			public static final int TILES_ROWS = 2;
		}

		public class Soldier1 {

			public static final float RUN_VELOCITY = 1f;

			public static final long WALK_FRAMES = 200;

			public static final int TILES_COLUMNS = 8;

			public static final int TILES_ROWS = 2;
		}

		public static final float RUN_VELOCITY = 1f;

		public static final long WALK_FRAMES = 200;
	}

	// public class FloorDoorCons {
	// public static final int FLOORDOOR_TILES_FRAMES = 100;
	// }

	public class HUD {
		public static final int NEED_TILE = 0;

		public static final int GOT_TILE = 1;

	}

	public class Shared {

		public static final int SHARED_ATLAS_WIDTH = 512;

		public static final int SHARED_ATLAS_HEIGHT = 512;

		public static final float WALL_WIDTH = 5;

		public static final float WALL_HEIGHT = 100;

		public static final int ENEMIES_ATLAS_WIDTH = 538;

		public static final int ENEMIES_ATLAS_HEIGHT = 130;

		public static final int SPLASH_ATLAS_WIDTH = 660;

		public static final int SPLASH_ATLAS_HEIGHT = 162;
	}

}
