package com.incrediblapps.games.maquis.utils.constants;

public class ConstantsLevel6 {

	public static final int WIDTH = 1890;

	public static final int HEIGHT = 1010;

	public static final int WIDTH_1 = 1024;

	public static final int WIDTH_2 = 866;

	public static final float PLAYER_START_POSITION_X = 1620;

	public static final float PLAYER_START_POSITION_Y = 305;

	public static final float GOTODOOR_PIXELS = 21;

	public static final float FLOORDOOR4020_POSITION_X = 1106;

	public static final float FLOORDOOR4020_POSITION_Y = 299;

	public static final float FLOORDOOR4031_POSITION_X = 1107;

	public static final float FLOORDOOR4031_POSITION_Y = 468;

	public static final float FLOORDOOR4030_POSITION_X = 836;

	public static final float FLOORDOOR4030_POSITION_Y = 468;

	public static final float FLOORDOOR4040_POSITION_X = 836;

	public static final float FLOORDOOR4040_POSITION_Y = 595;

	public static final int OBJECTS_ATLAS_WIDTH = 1024;

	public static final int OBJECTS_ATLAS_HEIGHT = 1024;

	public static final float DOOR61030_POSITION_X = 945;

	public static final float DOOR61030_POSITION_Y = 361;

	public static final float PILLAR1_POSITION_X = 896;

	public static final float PILLAR1_POSITION_Y = 384;

	public static final float PILLAR2_POSITION_X = 1028;

	public static final float PILLAR2_POSITION_Y = 384;

	public static final float PILLAR3_POSITION_X = 896;

	public static final float PILLAR3_POSITION_Y = 286;

	public static final float PILLAR4_POSITION_X = 1028;

	public static final float PILLAR4_POSITION_Y = 286;

	public static final float RAILING1_POSITION_X = 794;

	public static final float RAILING1_POSITION_Y = 376;

	public static final float RAILING2_POSITION_X = 1046;

	public static final float RAILING2_POSITION_Y = 374;

	public static final float WALL60020_X = 963;

	public static final float WALL60020_Y = 270;

	public static final float WALL60021_X = 1725;

	public static final float WALL60021_Y = WALL60020_Y;

	public static final float WALL60030_X = 808;

	public static final float WALL60030_Y = 430;

	public static final float WALL60031_X = 1243;

	public static final float WALL60031_Y = WALL60030_Y;

	public static final float STREETLIGHT1_POSITION_X = 24;

	public static final float STREETLIGHT1_POSITION_Y = 302;

	public static final float STREETLIGHT2_POSITION_X = 1659;

	public static final float STREETLIGHT2_POSITION_Y = 308;

	public static final float STREETLIGHT3_POSITION_X = 1758;

	public static final float STREETLIGHT3_POSITION_Y = 388;

	public static final long STREETLIGHT_FRAMES = 300;

	public static final float DOOR61040_POSITION_X = 419;

	public static final float DOOR61040_POSITION_Y = 559;

	public static final float DOOR61041_POSITION_X = 659;

	public static final float DOOR61041_POSITION_Y = DOOR61040_POSITION_Y;

	public static final float DOOR61042_POSITION_X = 941;

	public static final float DOOR61042_POSITION_Y = DOOR61040_POSITION_Y;

	public static final float DOOR61043_POSITION_X = 1256;

	public static final float DOOR61043_POSITION_Y = DOOR61040_POSITION_Y;

	public static final float DOOR61044_POSITION_X = 1396;

	public static final float DOOR61044_POSITION_Y = DOOR61040_POSITION_Y;

	public static final float WALL60040_X = 279;

	public static final float WALL60040_Y = 567;

	public static final float WALL60041_X = 792;

	public static final float WALL60041_Y = WALL60040_Y;

	public static final float WALL60042_X = 1618;

	public static final float WALL60042_Y = WALL60040_Y;

	public static final float HIDEDOOR63040_POSITION_X = 561;

	public static final float HIDEDOOR63040_POSITION_Y = 584;

	public static final float HIDEDOOR63050_POSITION_X = 1004;

	public static final float HIDEDOOR63050_POSITION_Y = 714;

	public static final float HIDEDOOR63010_POSITION_X = 1088;

	public static final float HIDEDOOR63010_POSITION_Y = 158;

	public static final float DOOR61010_POSITION_X = 969;

	public static final float DOOR61010_POSITION_Y = 133;

	public static final float DOOR61011_POSITION_X = 1218;

	public static final float DOOR61011_POSITION_Y = DOOR61010_POSITION_Y;

	public static final float WALL60010_X = 812;

	public static final float WALL60010_Y = 140;

	public static final float WALL60011_X = 1402;

	public static final float WALL60011_Y = WALL60010_Y;

	public static final float DOOR61050_POSITION_X = 659;

	public static final float DOOR61050_POSITION_Y = 689;

	public static final float DOOR61051_POSITION_X = 941;

	public static final float DOOR61051_POSITION_Y = DOOR61050_POSITION_Y;

	public static final float DOOR61052_POSITION_X = 1256;

	public static final float DOOR61052_POSITION_Y = DOOR61050_POSITION_Y;

	public static final float WALL60050_X = 443;

	public static final float WALL60050_Y = 689;

	public static final float WALL60051_X = 1402;

	public static final float WALL60051_Y = WALL60050_Y;

	public static final float CLOSET62050_POSITION_X = 785;

	public static final float CLOSET62050_POSITION_Y = 714;

	public static final float LIBRARY_POSITION_X = 1475;

	public static final float LIBRARY_POSITION_Y = 583;

	public static final float POSTER6040_POSITION_X = 359;

	public static final float POSTER6040_POSITION_Y = 600;

	public static final float POSTER6041_POSITION_X = 491;

	public static final float POSTER6041_POSITION_Y = POSTER6040_POSITION_Y;

	public static final float COUCH_POSITION_X = 1513;

	public static final float COUCH_POSITION_Y = 583;

	public static final float VIKTORIA_POSITION_X = 1011;

	public static final float VIKTORIA_POSITION_Y = 580;

	public static final float WC1_POSITION_X = 871;

	public static final float WC1_POSITION_Y = 156;

	public static final float WC2_POSITION_X = 901;

	public static final float WC2_POSITION_Y = WC1_POSITION_Y;

	public static final float WC3_POSITION_X = 930;

	public static final float WC3_POSITION_Y = WC1_POSITION_Y;

	public static final float PICTURE6010_POSITION_X = 1031;

	public static final float PICTURE6010_POSITION_Y = 174;

	public static final float PICTURE6011_POSITION_X = 1163;

	public static final float PICTURE6011_POSITION_Y = PICTURE6010_POSITION_Y;

	public static final float SHELVES61_POSITION_X = 1278;

	public static final float SHELVES61_POSITION_Y = 158;

	public static final float DOOR6WC_POSITION_X = 817;

	public static final float DOOR6WC_POSITION_Y = 147;

	public static final float WC_WALL_POSITION_X = 859;

	public static final float WC_WALL_POSITION_Y = 156;

	public static final float CLOSET6040_POSITION_X = 1314;

	public static final float CLOSET6040_POSITION_Y = 580;

	public static final float FD_NODOOR_WIDTH = 52;

	public static final float FD_NODOOR_HEIGHT = 64;

	public static final float FLOORDOOR62040_POSITION_X = 711;

	public static final float FLOORDOOR62040_POSITION_Y = 584;

	public static final float FLOORDOOR62050_POSITION_X = FLOORDOOR62040_POSITION_X;

	public static final float FLOORDOOR62050_POSITION_Y = 714;

	public static final float FLOORDOOR62041_POSITION_X = 1196;

	public static final float FLOORDOOR62041_POSITION_Y = FLOORDOOR62040_POSITION_Y;

	public static final float FLOORDOOR62051_POSITION_X = FLOORDOOR62041_POSITION_X;

	public static final float FLOORDOOR62051_POSITION_Y = FLOORDOOR62050_POSITION_Y;

	public static final float FLOORDOOR62052_POSITION_X = 1340;

	public static final float FLOORDOOR62052_POSITION_Y = FLOORDOOR62050_POSITION_Y;

	public static final float FLOORDOOR62010_POSITION_X = FLOORDOOR62052_POSITION_X;

	public static final float FLOORDOOR62010_POSITION_Y = 158;

	public static final float ENEMY100_START_POSITION_X = 1384;

	public static final float ENEMY100_START_POSITION_Y = 177;

	public static final float ENEMY100_POINT1_POSITION_X = 1260;

	public static final float ENEMY100_POINT2_POSITION_X = 1214;

	public static final float ENEMY100_POINT3_POSITION_X = 1014;

	public static final float ENEMY100_POINT4_POSITION_X = 968;

	public static final float ENEMY100_POINT5_POSITION_X = 836;

	public static final float ENEMY500_START_POSITION_X = 466;

	public static final float ENEMY500_START_POSITION_Y = 735;

	public static final float ENEMY500_POINT1_POSITION_X = 662;

	public static final float ENEMY500_POINT2_POSITION_X = 708;

	public static final float ENEMY500_POINT3_POSITION_X = 940;

	public static final float ENEMY500_POINT4_POSITION_X = 996;

	public static final float ENEMY500_POINT5_POSITION_X = 1258;

	public static final float ENEMY500_POINT6_POSITION_X = 1304;

	public static final float ENEMY500_POINT7_POSITION_X = 1380;

}
