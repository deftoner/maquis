package com.incrediblapps.games.maquis.utils.constants;

public class ConstantsLevel1 {

	public static final int WIDTH = 1966;

	public static final int HEIGHT = 632;

	public static final int WIDTH_1 = 1024;

	public static final int WIDTH_2 = 942;

	public static final int OBJECTS_ATLAS_WIDTH = 1024;

	public static final int OBJECTS_ATLAS_HEIGHT = 1024;

	public static final int TABLEPEOPLE_ATLAS_WIDTH = 1120;

	public static final int TABLEPEOPLE_ATLAS_HEIGHT = 280;

	public static final int TABLEPEOPLE_TILES_COLUMNS = 8;

	public static final int TABLEPEOPLE_TILES_ROWS = 4;

	public static final int TABLEPEOPLE_POSITION_X = 1389;

	public static final int TABLEPEOPLE_POSITION_Y = 251;

	public static final long TABLEPEOPLE_FRAMES = 150;

	public static long[] TABLEPEOPLE_FRAMES_ARRAY = { TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES,
			TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES,
			TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES,
			TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES, TABLEPEOPLE_FRAMES };

	public static final int COLUMN_POSITION_X = 1857;

	public static final int COLUMN_POSITION_Y = 124;

	public static final int DOOR1100_POSITION_X = 1316;

	public static final int DOOR1100_POSITION_Y = 117;

	public static final int DOOR1101_POSITION_X = 1444;

	public static final int DOOR1101_POSITION_Y = DOOR1100_POSITION_Y;

	public static final int DOOR1102_POSITION_X = 1748;

	public static final int DOOR1102_POSITION_Y = DOOR1100_POSITION_Y;

	public static final int DOOR1110_POSITION_X = 1559;

	public static final int DOOR1110_POSITION_Y = 237;

	public static final int DOOR5_POSITION_X = 508;

	public static final int DOOR5_POSITION_Y = DOOR1100_POSITION_Y;

	public static final int FLOORDOOR2110_POSITION_X = 1669;

	public static final int FLOORDOOR2110_POSITION_Y = 144;

	public static final int FLOORDOOR2100_POSITION_X = FLOORDOOR2110_POSITION_X;

	public static final int FLOORDOOR2100_POSITION_Y = 264;

	//public static final int PLAYER_START_POSITION_X = 1950;

	 public static final int PLAYER_START_POSITION_X = 900;

	public static final int PLAYER_START_POSITION_Y = 164;

	public static final float HIDEDOOR3100_POSITION_X = 1513;

	public static final float HIDEDOOR3100_POSITION_Y = 144;

	public static final float CLOSET110_POSITION_X = 1613;

	public static final float CLOSET110_POSITION_Y = 261;

	public static final float SHELVES1100_POSITION_X = 1589;

	public static final float SHELVES1100_POSITION_Y = 179;

	public static final float PICTURE1100_POSITION_X = 1741;

	public static final float PICTURE1100_POSITION_Y = 285;

	public static final float WALL11001_X = 1858;

	public static final float WALL11001_Y = 245;

	public static final float DOOR1001_POSITION_X = 416;

	public static final float DOOR1001_POSITION_Y = 117;

	public static final float DOOR1002_POSITION_X = 778;

	public static final float DOOR1002_POSITION_Y = 119;

	public static final float DOOR1010_POSITION_X = 674;

	public static final float DOOR1010_POSITION_Y = 247;

	public static final float DOOR1000_POSITION_X = 162;

	public static final float DOOR1000_POSITION_Y = 120;

	public static final float FLOORDOOR2000_POSITION_X = 580;

	public static final float FLOORDOOR2000_POSITION_Y = 144;

	public static final float FLOORDOOR2010_POSITION_X = 580;

	public static final float FLOORDOOR2010_POSITION_Y = 274;

	public static final float CLOSET000_POSITION_X = 700;

	public static final float CLOSET000_POSITION_Y = 140;

	public static final float SHELVES2000_POSITION_X = 541;

	public static final float SHELVES2000_POSITION_Y = 189;

	public static final float FLAG_REP_POSITION_X = 646;

	public static final float FLAG_REP_POSITION_Y = 164;

	public static final float FLAG_CAT_POSITION_X = 531;

	public static final float FLAG_CAT_POSITION_Y = 318;

	public static final float PICTURE2000_POSITION_X = 478;

	public static final float PICTURE2000_POSITION_Y = 318;

	public static final float LIBRARY_POSITION_X = 484;

	public static final float LIBRARY_POSITION_Y = 142;

	public static final float CLOCK_POSITION_X = 638;

	public static final float CLOCK_POSITION_Y = 275;

	public static final float METALIC_CLOSET_POSITION_X = 396;

	public static final float METALIC_CLOSET_POSITION_Y = 187;

	public static final float CAR_POSITION_X = 226;

	public static final float CAR_POSITION_Y = 152;

	public static final float WALL10000_X = 783;

	public static final float WALL10000_Y = 250;

	public static final float TABLE_POSITION_X = 475;

	public static final float TABLE_POSITION_Y = 271;

	public static final float WINDOW_X = 420;

	public static final float WINDOW_WIDTH = 30;

	public static final float WINDOW_Y = 249;

	public static final float WINDOW_HEIGHT = 100;

	public static final float ENEMY1_START_POSITION_X = 765;

	public static final float ENEMY1_START_POSITION_Y = 295;

	public static final float ENEMY1_POINT1_POSITION_X = 725;

	public static final float ENEMY1_POINT2_POSITION_X = 660;

	public static final float ENEMY1_POINT3_POSITION_X = 470;

	public static final int ENEMIES_ATLAS_WIDTH = 792;

	public static final int ENEMIES_ATLAS_HEIGHT = 128;

	public static final long CLOCK_FRAMES = 400;

}
