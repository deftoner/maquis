package com.incrediblapps.games.maquis.utils;

import org.andengine.engine.camera.SmoothCamera;

import com.incrediblapps.games.maquis.ResourceManager;

/**
 * 
 * 
 * 
 *** @author Javier Sanchez
 **/
public class MaquisSmoothCamera extends SmoothCamera {

	public MaquisSmoothCamera(float pX, float pY, float pWidth, float pHeight, float pMaxVelocityX, float pMaxVelocityY, float pMaxZoomFactorChange) {
		super(pX, pY, pWidth, pHeight, pMaxVelocityX, pMaxVelocityY, pMaxZoomFactorChange);

	}

	public static void setupForMenus() {
		final MaquisSmoothCamera thisStaticCam = ((MaquisSmoothCamera) ResourceManager.getInstance().getEngine().getCamera());

		thisStaticCam.setChaseEntity(null);
		thisStaticCam.setZoomFactorDirect(1f);
		thisStaticCam.setCenterDirect(ResourceManager.cameraWidth * 0.5f, ResourceManager.cameraHeight * 0.5f);
		thisStaticCam.clearUpdateHandlers();
	}

}